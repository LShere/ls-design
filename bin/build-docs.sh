#! /usr/bin/env bash
# 单个docker部署用这个，多个docker部署，直接上docker-compose
# 自动化部署
# 需要先赋予权限
# chmod +x ./build.sh
# 然后执行 ./build.sh 即可
set -e

# 镜像名称
image_name=ls-design-docs-img

# 日期作为版本
version=$(date +"%Y%m%d-%H%M%S")

# 本机端口号
host_port=8080

# 容器端口
container_port=80

# 容器名称
container_name=ls-design-docs-contaier

# dockerfile路径
docker_file_path=./configs/Dockerfile.docs


echo "===开始构建镜像==="

docker build  -t ${image_name}:${version} -f ${docker_file_path} .

echo "===清理同名容器==="

if [ "$(docker ps -aq -f name=${container_name})" ]; then
  echo "===容器已存在，清理容器==="
  docker rm -f ${container_name}
fi

echo "===启动容器==="

docker run --name ${container_name} -dp ${host_port}:${container_port} ${image_name}:${version}

echo "===构建✅==="