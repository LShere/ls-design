module.exports = {
  extends: ['@lgfe/preset', '@lgfe/preset/typescript'],
  rules: {
    '@typescript-eslint/no-unused-vars': 1,
    '@typescript-eslint/no-unused-expressions': 1,
    '@typescript-eslint/explicit-member-accessibility': 0,
    '@typescript-eslint/member-ordering': 0,
    '@typescript-eslint/adjacent-overload-signatures': 0,
    '@typescript-eslint/consistent-type-assertions': 0,
    'typescript-eslint/no-explicit-any': 0,
    'guard-for-in': 0,
    'grouped-accessor-pairs': 0,
    'no-implicit-coercion': 0,
    radix: 0,
    'no-self-assign': 0,
    'max-params': 0,
    'no-param-reassign': 0,
    '@typescript-eslint/prefer-for-of': 0,
    '@typescript-eslint/no-invalid-void-type': 0,
    '@typescript-eslint/no-this-alias': 0,
  },
};
