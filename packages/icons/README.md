预设了多种图标，满足一般c测开发的需求

## ⚙️ 安装

```bash
npm install @ls-design/icons --save
```

## 🚀 使用

```tsx
import "@ls-design/icons/dist/es/arrow-right;

<ls-icon-arrow-right></ls-icon-arrow-right>

// 其他图标可查看源码
```
## 📖 属性

| 属性 | 说明 | 默认值 |
| --- | --- | --- |
| size | 图标尺寸 | 当前包裹元素字体`font-size`大小 |
| color | 图标颜色 | `currentColor` |
