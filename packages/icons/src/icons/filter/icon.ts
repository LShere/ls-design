export const Icon =
  '<path fill="none" stroke="currentColor" stroke-linejoin="round" stroke-width="1.5" d="M20 4H4l5.6 7.467a2 2 0 0 1 .4 1.2V20l4-2v-5.333a2 2 0 0 1 .4-1.2L20 4Z"/>';
