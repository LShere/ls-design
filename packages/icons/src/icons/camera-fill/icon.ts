export const Icon =
  '<path fill="currentColor" fill-rule="evenodd" d="M19 3a1 1 0 1 0 0-2h-2a1 1 0 1 0 0 2h2ZM2 5a1 1 0 0 1 1-1h18a1 1 0 0 1 1 1v12a3 3 0 0 1-3 3H5a3 3 0 0 1-3-3V5Zm10 10a3 3 0 1 0 0-6a3 3 0 0 0 0 6Z" clip-rule="evenodd"/>';
