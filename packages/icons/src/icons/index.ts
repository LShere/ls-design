// @ls-design/icons svg is taken from  https://icon-sets.iconify.design/iconamoon/

import ArrowLeft from './arrow-left';
import ArrowRight from './arrow-right';
import ArrowTop from './arrow-top';
import ArrowBottom from './arrow-bottom';
import Close from './close';
import Copy from './copy';
import Notify from './notify';
import SuccessCircle from './success-circle';
import ErrorCircle from './error-circle';
import WarningCircle from './warning-circle';
import Star from './star';
import StarFill from './star-fill';
import CaremaFill from './camera-fill';
import Search from './search';
import Success from './success';
import Plus from './plus';
import Setting from './setting';
import Menu from './menu';
import Home from './home';
import Like from './like';
import LikeFill from './like-fill';
import Bookmark from './bookmark';
import BookmarkFill from './bookmark-fill';
import Comment from './comment';
import Filter from './filter';
import Heart from './heart';
import HeartFill from './heart-fill';
import User from './user';
import Help from './help';
import Refresh from './refresh';
import Share from './share';
import ShopCard from './shop-card';
import Delete from './delete';
import Scan from './scan';
import Location from './location';
import LocationFill from './location-fill';

export default {
  ArrowLeft,
  ArrowRight,
  ArrowTop,
  ArrowBottom,
  Close,
  Copy,
  Notify,
  SuccessCircle,
  ErrorCircle,
  WarningCircle,
  Star,
  StarFill,
  CaremaFill,
  Search,
  Success,
  Plus,
  Setting,
  Menu,
  Home,
  Like,
  LikeFill,
  Bookmark,
  BookmarkFill,
  Comment,
  Filter,
  Heart,
  HeartFill,
  User,
  Help,
  Refresh,
  Share,
  ShopCard,
  Delete,
  Scan,
  Location,
  LocationFill,
};
