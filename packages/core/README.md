基于 `quarkc` 的`core`层

提供了对 `@ls-design/ui`的底层支持, 以更友好的姿态实现了Web Components


## ⚙️ 安装

```bash
npm install @ls-design/core --save
```

## 🚀 使用示例

```tsx
import { Property, CustomElement, createRef, LSElement } from '@ls-design/core';
import style from './style.css';

@CustomElement({
  tag: "ls-button",
  style,
})
class Button extends LSElement {
  @Property() // @Property提供响应式外部属性
  type = '';

  @Property({
    type: Boolean,
  })
  disabled = false;

  @State // 组件内部响应式元素
  isShow = false,

  slotRef: any = createRef(); // 创建ref

  render() {
    ...
  }
}

```

---

除了上述 **使用示例** 外的能力基本参考了`quarkc`，可直接其阅读[文档](https://quarkc.hellobike.com/#/)
