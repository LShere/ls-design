/**
 * @file 基于Map的kv存储，用于保存core封装的wc的properties和装饰器的descriptor及eventBus中的事件map
 */

export class DBMap<K1, K2, Value> {
  private map: Map<K1, Map<K2, Value>> = new Map();

  get(key1: K1): Map<K2, Value>;
  get(key1: K1, key2: K2): Value;
  get(key1: K1, key2?: K2) {
    const subMap = this.map.get(key1);
    if (subMap) {
      if (!key2) return subMap;
      return subMap.get(key2);
    }
  }

  set(key1: K1, key2: K2, value: Value) {
    let subMap = this.map.get(key1);
    if (!subMap) {
      subMap = new Map();
      this.map.set(key1, subMap);
    }
    subMap?.set(key2, value);
  }

  forEach(cb: (value: Value, key1: K1, key2: K2) => void) {
    this.map.forEach((subMap, key1) => {
      subMap.forEach((value, key2) => {
        cb(value, key1, key2);
      });
    });
  }

  delete(key: K1) {
    this.map.delete(key);
  }

  deleteAll() {
    this.map.forEach((_, key1) => {
      this.map.delete(key1);
    });
  }
}
