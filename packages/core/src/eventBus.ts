/**
 * @file eventBus用于dom事件监听
 */
import { DBMap } from './dbMap';

export type EventHandler = (evnet: Event) => void;

export class EventBus {
  private eventMap: DBMap<Element, string, Set<EventHandler>> = new DBMap();

  on(el: Element | null, eventName: string, eventHandler: EventHandler) {
    if (!el || !eventName || !eventHandler) {
      return;
    }

    let set = this.eventMap.get(el, eventName);
    if (!set) {
      set = new Set<EventHandler>();
      this.eventMap.set(el, eventName, set);
    }
    if (!set.has(eventHandler)) {
      set.add(eventHandler);
      el.addEventListener(eventName, eventHandler, true);
    }
  }

  clearAll() {
    this.eventMap.forEach((set, el, eventName) => {
      set.forEach((handler) => {
        el.removeEventListener(eventName, handler, true);
      });
    });
    this.eventMap.deleteAll();
  }
}
