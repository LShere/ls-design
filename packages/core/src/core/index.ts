/**
 * Copy From https://github.com/hellof2e/quark-core/tree/main/packages/core/src/core
 * as @ls-design/core render engine
 **/

export { Component, getDomSibling } from './common/component';
export { EMPTY_OBJ, EMPTY_ARR, IS_NON_DIMENSIONAL } from './common/constants';
export { createElement, createRef, createVNode, Fragment } from './common/create-element';
export { render } from './common/render';
export { assign, removeNode, isFunction, slice } from './common/util';

export { diffChildren } from './diff/children';
export { diff, applyRef, unmount } from './diff';
export { diffProps, setProperty } from './diff/props';
