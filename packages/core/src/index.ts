/**
 * @file 封装wc
 */

import { createElement as h, Fragment as OrigFragment, render, isFunction } from './core';
import { DBMap } from './dbMap';
import type { EventHandler } from './eventBus';
import { EventBus } from './eventBus';

export interface Ref<T = any> {
  current: T;
}

export function createRef<T = any>(): Ref<T | null> {
  return { current: null };
}

export const Fragment: any = OrigFragment;

type Falsy = false | 0 | '' | null | undefined;

/** 排除0和false */
const isEmpty = (val: unknown): val is Exclude<Falsy, false | 0> => !(val || val === false || val === 0);

type ParamsType = typeof Boolean | typeof Number | typeof String;

type ConvertFn = (val: any, type?: ParamsType) => any;

type ElementWrapper<T> = {
  constructor: T;
};

type PropertyDeclaration = {
  /**
   * 是否响应式属性，接收外部的参数变化，会自动加入observedAttributes数组中
   */
  readonly observed?: boolean | string;
  /**
   * 属性类型，会针对类型做不同的特殊处理。
   * Boolean | Number | String
   */
  readonly type?: ParamsType;
  /**
   * 从外部获取属性时的值转换方法
   */
  readonly converter?: ConvertFn;
  /**
   * 创建内部属性名和外部属性名不同时，可以通过改属性指定外部属性名称
   */
  readonly attribute?: string;
};

type WCParams =
  | string
  | {
      tag: string;
      style?: string;
    };

const defaultConverter: ConvertFn = (value, type) => {
  let newVal = value;
  switch (type) {
    case Number:
      newVal = isEmpty(value) ? value : Number(value);
      break;
    case Boolean:
      newVal = !([null, undefined, false, 'false'].indexOf(value) > -1);
      break;
  }
  return newVal;
};

const defaultPropertyDeclaration: PropertyDeclaration = {
  observed: true,
  type: String,
  converter: defaultConverter,
};

/** 组件属性DBMap */
const ElementProperties: DBMap<typeof LSElement, string, PropertyDeclaration> = new DBMap();

/** descriptorsDBMap */
const Descriptors: DBMap<typeof LSElement, string, (defaultValue?: any) => PropertyDescriptor> = new DBMap();

/** 用于收集属性到DBMap */
export const Property = (options: PropertyDeclaration = {}) => {
  return (target: unknown, name: string) => {
    return (target.constructor as typeof LSElement).createProperty(name, options);
  };
};

/** 用于定义组件state，变更会导致组件重渲染 */
export const State = (target: unknown, name: string) => {
  return (target.constructor as typeof LSElement).createState(name);
};

/** 用于继承LSElement的装饰器，内置属性监听钩子及constructor方法 */
export const CustomElement = (params: WCParams) => {
  const { tag, style = '' } = typeof params === 'string' ? { tag: params } : params;
  return (target: typeof LSElement) => {
    // 再次继承target，对LSElement二度封装，在此完成WC构造函数调用
    class OuterLSElement extends target {
      static get observedAttributes() {
        const attributes: string[] = [];
        // 获取target元素上的属性map
        const targetProperties = ElementProperties.get(target);
        if (targetProperties) {
          targetProperties.forEach((val, key) => {
            if (val.observed) {
              // 是响应式属性的就加入到attributes中，给propertiesChangeCallback传递必要参数
              attributes.push(key);
            }
          });
        }
        return attributes;
      }

      static isBooleanProperty(propertyName: string) {
        let isBoolean = false;
        const targetProperties = ElementProperties.get(target);
        if (targetProperties) {
          targetProperties.forEach((val, key) => {
            if (val.type === Boolean && propertyName === key) {
              isBoolean = true;
              return isBoolean;
            }
          });
        }

        return isBoolean;
      }

      constructor() {
        super();

        // 创建shadowRoot,可访问外界属性
        const shadowRoot = this.attachShadow({ mode: 'open' });

        if (shadowRoot) {
          if (typeof CSSStyleSheet === 'function' && shadowRoot.adoptedStyleSheets) {
            // 使用styleSheet导入外部样式表
            const sheet = new CSSStyleSheet();
            sheet.replaceSync(style);
            shadowRoot.adoptedStyleSheets = [sheet];
          } else {
            // 兜底,使用传统style标签
            const styleLabel = document.createElement('style');
            styleLabel.innerHTML = style;
            shadowRoot.appendChild(styleLabel);
          }
        }

        // 重写子类的属性操作符，因为子类的构造函数晚于基类，所以这部分定义就需要放到子类构造函数中执行，此时基类构造函数已执行完
        const targetDescriptors = Descriptors.get(Object.getPrototypeOf(this.constructor));
        if (targetDescriptors) {
          targetDescriptors.forEach((descriptorCreator, propertyName) => {
            Object.defineProperty(this, propertyName, descriptorCreator(this[propertyName]));
          });
        }
      }
    }

    if (!customElements.get(tag)) {
      // 防止重复定义WC
      customElements.define(tag, OuterLSElement);
    }
  };
};

/** 自定义wc封装 */
export class LSElement extends HTMLElement {
  /** 作为jsxFactory使用 */
  static h = h;
  static Fragment = Fragment;
  /** 外部属性装饰器，抹平不同框架使用差异 */
  protected static getPropertyDescriptor(name: string, options: PropertyDeclaration) {
    return (defaultValue?: any): PropertyDescriptor => {
      return {
        configurable: true,
        enumerable: true,
        get(this: LSElement) {
          // 通过getAttribute获取属性值
          let val = this.getAttribute(name);
          if (!isEmpty(defaultValue)) {
            // 判断val是否为空值
            // const isEmpty = () => !(val || val === false || val === 0)
            // 当类型为非Boolean时，通过isEmpty方法判断val是否为空值
            // 当类型为Boolean时，在isEmpty判断之外，额外认定空字符串不为空值
            //
            // 条件表达式推导过程
            // 由：(options.type !== Boolean && isEmpty(val)) || (options.type === Boolean && isEmpty(val) && val !== '')
            // 变形为：isEmpty(val) && (options.type !== Boolean || (options.type === Boolean && val !== ''))
            // 其中options.type === Boolean显然恒等于true：isEmpty(val) && (options.type !== Boolean || (true && val !== ''))
            // 得出：isEmpty(val) && (options.type !== Boolean || val !== '')
            if (isEmpty(val) && (options.type !== Boolean || val !== '')) {
              return defaultValue;
            }
          }

          if (isFunction(options.converter)) {
            // 有外部convert的时候直接覆盖
            val = options.converter(val, options.type) as string;
          }

          return val;
        },
        set(this: LSElement, value: string | boolean | null) {
          let val = value as string;
          if (isFunction(options.converter)) {
            // 赋值时也需要使用外部convert进行转换
            val = options.converter(value, options.type) as string;
          }
          if (!isEmpty(val)) {
            // val 可能为false 或 0
            if (typeof val === 'boolean') {
              if (val) {
                // boolean时，且 val 为 true ,赋值空字符串？暂时不知道为啥
                this.setAttribute(name, '');
              } else {
                this.removeAttribute(name);
              }
            } else {
              // 非boolean时，正常赋值
              this.setAttribute(name, val);
            }
          } else {
            // 移除属性
            this.removeAttribute(name);
          }
        },
      };
    };
  }

  /** 内部装饰器，用于更新组件内的状态 */
  protected static getStateDescriptor(name) {
    return (defaultValue?: any): PropertyDescriptor => {
      let _value = defaultValue;
      return {
        configurable: true,
        enumerable: true,
        get(this: LSElement) {
          return _value;
        },
        set(this: LSElement, value: string | boolean | null) {
          const oldValue = _value;
          _value = value;
          // set时使组件render
          this._render();
          if (isFunction(this.componentDidUpdate)) {
            // 暴露componentDidUpdate函数，作为组件更新时的回调
            this.componentDidUpdate(name, oldValue, value);
          }
        },
      };
    };
  }

  static createProperty(name: string, options: PropertyDeclaration) {
    const newOpt = { ...defaultPropertyDeclaration, ...options };
    const attributeName = options.attribute || name;
    // 设置元素属性
    ElementProperties.set(this, attributeName, newOpt);
    // 设置descriptors
    Descriptors.set(this, name, this.getPropertyDescriptor(attributeName, newOpt));
  }

  static createState(name: string) {
    // 设置内部更新状态
    Descriptors.set(this, name, this.getStateDescriptor(name));
  }

  // 控制器
  private eventController: EventBus = new EventBus();

  private rootPatch = (newRootVNode: any) => {
    if (this.shadowRoot) {
      render(newRootVNode, this.shadowRoot);
    }
  };
  private _render() {
    const newRootVNode = this.render();

    if (newRootVNode) {
      this.rootPatch(newRootVNode);
    }
  }

  private _updateProperty() {
    // CustomElement装饰器内部有继承于LSElement的子类，里面配置有该静态属性 observedAttributes
    (this.constructor as any).observedAttributes.forEach((propertyName: string) => {
      // 走一次descriptor中的set函数，完成更新
      (this as any)[propertyName] = (this as any)[propertyName];
    });
  }

  private _updateBooleanProperty(propertyName: string) {
    // 判断是否是 boolean
    if ((this.constructor as any).isBooleanProperty(propertyName)) {
      // 针对 false 场景走一次 set， true 不需要重新走 set
      if (!(this as any)[propertyName]) {
        (this as any)[propertyName] = (this as any)[propertyName];
      }
    }
  }

  $on(eventName: string, eventHandler: EventHandler, el?: Element) {
    // LSElement内部封装控制器，启用监听，内部使用了el.addEventListener
    return this.eventController.on(el || this, eventName, eventHandler);
  }

  $emit<T>(eventName: string, customEventInit?: CustomEventInit<T>) {
    // 派发自定义事件 CustomEventInit 是自定义事件options的类型
    return this.dispatchEvent(new CustomEvent(eventName, { bubbles: true, ...customEventInit }));
  }

  /**
   * 此时组件 dom 已插入到页面中，等同于 connectedCallback() { super.connectedCallback(); }
   */
  componentDidMount() {}

  /**
   * disconnectedCallback 触发时、dom 移除前执行，等同于 disconnectedCallback() { super.disconnectedCallback(); }
   */
  componentWillUnmount() {}

  /**
   * 控制当前属性变化是否导致组件渲染
   *
   * @param propName 属性名
   * @param oldValue 属性旧值
   * @param newValue 属性新值
   * @returns boolean
   */
  shouldComponentUpdate(propName: string, oldValue: string, newValue: string) {
    return oldValue !== newValue;
  }

  /**
   * 更新内部属性时会触发该钩子函数
   */
  componentDidUpdate(propName: string, oldValue: any, newValue: any) {}

  /**
   * quarkc 的 render 方法，直接拷来用
   * 功能：自动执行 this.shadowRoot.innerHTML = this.render()
   */
  render() {
    return '' as any;
  }

  /**
   * 当自定义元素首次被插入到文档DOM中时调用。在这个回调函数中，可以执行一些初始化操作，比如添加事件监听器、设置初始状态等
   */
  connectedCallback() {
    // 设置属性
    this._updateProperty();
    // 渲染
    this._render();
    if (isFunction(this.componentDidMount)) {
      // 调用componentDidMount回调
      this.componentDidMount();
    }
  }

  /**
   * 当自定义元素的一个或多个属性发生变化时调用。可以通过在 observedAttributes 静态属性中定义要观察的属性来启用这个回调函数。
   */
  attributeChangedCallback(name: string, oldValue: string, value: string) {
    // 因为 React 的属性变更并不会触发 set，此时如果 boolean 值变更，这里的 value 会是字符串，组件内部通过 get 操作可以获取到正确的类型
    const newValue = this[name] || value;
    if (isFunction(this.shouldComponentUpdate)) {
      // 调用shouldComponentUpdate来判断是否进行组件更新
      if (!this.shouldComponentUpdate(name, oldValue, newValue)) {
        return;
      }
    }
    // 其余情况，进行重渲染
    this._render();

    if (isFunction(this.componentDidUpdate)) {
      // 调用componentDidUpdate回调
      this.componentDidUpdate(name, oldValue, newValue);
    }

    // 因为 React的属性变更并不会触发set，此时如果boolean值变更，这里的value会是字符串，组件内部通过get操作可以正常判断类型，但css里面有根据boolean属性设置样式的将会出现问题
    if (value !== oldValue) {
      // boolean 重走set
      this._updateBooleanProperty(name);
    }
  }

  /**
   * 自定义元素从文档DOM中移除时调用
   */
  disconnectedCallback() {
    if (isFunction(this.componentWillUnmount)) {
      // 调用componentWillUnmount
      this.componentWillUnmount();
    }

    // 取消控制器监听,内部调用removeEventListener
    this.eventController.clearAll();
    this.rootPatch(null);
  }
}

/** https://github.com/microsoft/TypeScript/issues/52396遇到legacy装饰器不管用的情况，只需设置vscode ts版本为本地安装的，而不是最新版5.x */
class Test extends LSElement {
  @Property()
  aa: string;

  @State
  bb: string;
}
