import { defineConfig } from 'rollup';
import commonjs from '@rollup/plugin-commonjs';
import { nodeResolve } from '@rollup/plugin-node-resolve';
import { babel } from '@rollup/plugin-babel';
import typescript from '@rollup/plugin-typescript';
import terser from '@rollup/plugin-terser';
import json from '@rollup/plugin-json';

import * as fs from 'fs';
import * as path from 'path';

const pkgJson = JSON.parse(fs.readFileSync(path.resolve(__dirname, 'package.json')).toString());
const dependencies = Object.keys(pkgJson.dependencies ?? {});

const extensions = ['.js', '.ts', '.tsx'];

const plugins = [
  typescript(),
  json(),
  commonjs(),
  nodeResolve({
    extensions,
  }),
  babel({
    babelHelpers: 'runtime',
    exclude: 'node_modules/**',
    extensions,
  }),
  terser(),
];

const input = './src/index.ts';
const dir = 'dist';

export default defineConfig([
  {
    input,
    output: [
      {
        dir,
        entryFileNames: 'index.js',
        format: 'es',
      },
    ],
    plugins,
    external: dependencies,
  },
  {
    input,
    output: [
      {
        dir,
        entryFileNames: 'index.umd.js',
        format: 'umd',
        name: 'ls-design-core',
      },
    ],
    plugins,
  },
]);
