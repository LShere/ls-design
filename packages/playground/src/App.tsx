import { Fragment, useState, useRef, useEffect } from 'react';
import './App.css';
import {
  Button,
  Image,
  Cell,
  CellGroup,
  Badge,
  Circle,
  GridItem,
  Grid,
  Popup,
  ImagePreview,
  ImagePreivewRef,
  imagePreview,
  List,
  VirtualList,
  Marquee,
  Noticebar,
  Progress,
  Skeleton,
  Steps,
  StepItem,
  Sticky,
  Tag,
  Navbar,
  Tabs,
  TabContent,
  Tabbar,
  TabbarItem,
  ActionSheet,
  Collapse,
  Dialog,
  Overlay,
  Popover,
  PopoverRef,
  PullRefresh,
  ShareSheet,
  SwipeCell,
  Toast,
  Tooltip,
  Cascade,
  CascadeRef,
  Checkbox,
  CheckboxGroup,
  Input,
  InputRef,
  Picker,
  PickerRef,
  DatetimePicker,
  DatetimePickerRef,
  Radio,
  RadioGroup,
  Rate,
  Stepper,
  TextArea,
  Switch,
  Form,
  FormRef,
  FormItem,
  Upload,
  Empty,
  CountDown,
} from '@ls-design/ui-react';
import '@ls-design/icons/dist/es/arrow-right';

const baseUrls = [
  'https://m.hellobike.com/resource/helloyun/15697/iWS-0QI6QV.png',
  'https://m.hellobike.com/resource/helloyun/15697/1V_2oJv02t.png',
  'https://m.hellobike.com/resource/helloyun/15697/Q6t6B_noNetWork.png',
];

function App() {
  const [isShowPopup, setIsShowPopup] = useState(false);
  const imagePreviewRef = useRef<ImagePreivewRef>(null);
  const [isShowPreview, setIsShowPreview] = useState(false);
  return (
    <>
      <Sticky offsettop="2px">
        <div value="其他单位">粘性布局</div>
      </Sticky>
      <Button>你好</Button>
      <Image
        src="https://images.pexels.com/photos/18791598/pexels-photo-18791598.jpeg?auto=compress&cs=tinysrgb&w=1600&lazy=load"
        width={100}
        height={100}
      />
      <Cell desc="你好你好" title="sdfsfsddf" islink to="https://baidu.com" />
      <ls-icon-arrow-right size="20" color="#ccc" />
      <Badge type="round" content="11"></Badge>

      <CellGroup>
        <Cell desc="你好你好" title="sdfsfsddf" islink to="https://baidu.com" />
        <Cell desc="你好你好" title="sdfsfsddf" islink to="https://baidu.com" />
      </CellGroup>
      <Circle rate={10} startposition="right">
        右侧
      </Circle>
      <Grid square>
        <GridItem icon="https://m.hellobike.com/resource/helloyun/16682/dyElZ_img.png" text="文字" />
        <GridItem icon="https://m.hellobike.com/resource/helloyun/16682/dyElZ_img.png" text="文字" />
        <GridItem icon="https://m.hellobike.com/resource/helloyun/16682/dyElZ_img.png" text="文字" />
      </Grid>
      <button onClick={() => setIsShowPopup(true)}>打开popup</button>
      <Popup open={isShowPopup} closeable onClose={() => setIsShowPopup(false)}>
        你好
      </Popup>

      <button
        onClick={() => {
          setIsShowPreview(true);
          console.log('===', isShowPreview);
          imagePreviewRef.current.setData({
            images: baseUrls,
            close: (index: number) => {
              setIsShowPreview(false);
              console.log('===preview close cb', index);
            },
          });
        }}
      >
        打开图片预览
      </button>
      <button
        onClick={() => {
          console.log('11111', imagePreview);
          imagePreview({
            images: baseUrls,
          });
        }}
      >
        打开图片预览，标签式用法
      </button>
      <ImagePreview ref={imagePreviewRef} open={isShowPreview} />
      {/* 外层容器要比内部宽度小时，才能开始跑马灯 */}
      <div style={{ width: '100px' }}>
        <Marquee title="测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试"></Marquee>
      </div>
      <Noticebar text="aaaa" color="red" bgcolor="#ddd"></Noticebar>
      <Progress value="20" showtext />
      <br />
      <Skeleton avatar avatarshape="round">
        1111
      </Skeleton>
      <Steps>
        <StepItem status="done" title="步骤一" order="1"></StepItem>
        <StepItem status="doing" title="步骤二" order="2"></StepItem>
        <StepItem status="todo" title="步骤三" order="3"></StepItem>
      </Steps>
      <ul>
        {new Array(10).fill(1).map((item) => (
          <li key={item}>{item}</li>
        ))}
      </ul>
      <Tag type="primary">正</Tag>
      <Navbar title="页面标题"></Navbar>
      <Tabbar>
        <TabbarItem>首页</TabbarItem>
        <TabbarItem>我的</TabbarItem>
        <TabbarItem>联系</TabbarItem>
      </Tabbar>
      <Collapse title="标题">生命远不止连轴转和忙到极限，人类的体验远比这辽阔、丰富得多。</Collapse>
      <button onClick={() => Toast.warning('小心')}>toast提示</button>
      <Rate value="1" />
      <Stepper value="1" />
      <TextArea />
      <Switch checked></Switch>
      <Upload onAfterread={(e) => console.log(e)} />
      <Empty />
      <CountDown time="7200000" />
    </>
  );
}

function ListDemo() {
  const [loading1, setLoading1] = useState(false);
  const [finished1, setFinished1] = useState(false);
  const [list1, setList1] = useState([]);

  const onLoad1 = () => {
    if (finished1) {
      return;
    }
    setLoading1(true);
    setTimeout(() => {
      for (let i = 0; i < 10; i += 1) {
        setList1((list1) => [...list1, list1.length + 1]);
      }
      setLoading1(false);
      if (list1.length >= 30) {
        setFinished1(true);
      }
    }, 1000);
  };

  return (
    <List loading={loading1} finished={finished1} finishedtext="finished text" onLoad={onLoad1}>
      <div slot="content">
        {list1.map((item) => (
          <div className="list-list" key={item}>
            {item}
          </div>
        ))}
      </div>
    </List>
  );
}

function VirtualListDemo() {
  const [list, setList] = useState(new Array(100).fill(0).map((_, i) => i + 1));
  const virtualListRef = useRef(null);
  useEffect(() => {
    virtualListRef.current.setListData(list);
    virtualListRef.current.setRenderItem(renderItem);
  }, []);

  const onLoad = () => {
    const len = list.length;
    const arr = new Array(100).fill(0).map((_, i) => len + i + 1);
    const newList = [...list, ...arr];
    setList(newList);
    virtualListRef.current.setListData(newList);
  };

  const renderItem = (text) => {
    const div = `
      <div style="height: 50px; line-height: 50px; text-align: center;">${text}</div>
    `;
    return div;
  };

  return (
    <div style={{ width: 100 }}>
      <VirtualList ref={virtualListRef} itemheight={50} containerheight={500} onLoad={onLoad} />
    </div>
  );
}

function TabDemo() {
  return (
    <Tabs activekey="0">
      <TabContent label="tab1"> tab1 content </TabContent>
      <TabContent label="tab2"> tab2 content </TabContent>
      <TabContent label="tab3" disabled>
        tab3 content
      </TabContent>
      <TabContent label="tab4"> tab4 content </TabContent>
    </Tabs>
  );
}

function ActionSheetDemo() {
  const btnClick = () => {
    ActionSheet({
      title: '我是标题信息',
      cancelText: '取消',
      actions: [{ name: '选项一' }, { name: '选项二' }, { name: '选项三' }],
      select: (index, action) => {},
      cancel: () => {},
      close: () => {},
    });
  };
  return <button onClick={btnClicck}>打开actionSheet</button>;
}

function DialogDemo() {
  const [open, setOpen] = useState(false);
  const showDialog = () => {
    setOpen(true);
  };

  const handleConfirm = () => {
    setOpen(false);
  };

  const handleClose = () => setOpen(false);

  return (
    <div>
      <div onClick={showDialog}>基础弹窗</div>
      <Dialog
        open={open}
        title="基础弹窗"
        content="生命远不止连轴转和忙到极限，人类的体验远比这辽阔、丰富得多。"
        onConfirm={handleConfirm}
        onClose={handleClose}
      ></Dialog>
    </div>
  );
}

function OverlayDemo() {
  const [open, setOpen] = useState(false);

  const handleClose = () => setOpen(false);
  const handleClick = () => setOpen(true);

  return (
    <div>
      <div onClick={handleClick}>sdfsdf</div>
      <Overlay open={open} onClose={handleClose} />
    </div>
  );
}

function PopoverDemo() {
  const [open, setOpen] = useState(false);
  const openRef = useRef<PopoverRef>(null);
  const actions = [{ text: '选项一' }, { text: '选项二' }, { text: '选项三' }];

  const handleClick = () => setOpen(true);
  const handleClose = () => setOpen(false);

  useEffect(() => {
    const { current: lightCurrent } = openRef;
    lightCurrent.setActions(actions);
  }, []);

  return (
    <div className="demo">
      <Popover
        ref={openRef}
        open={open}
        onClose={handleClose}
        onSelect={({ detail }) => {
          const { action } = detail;
          console.log(action.text);
          handleClose();
        }}
      >
        <div className="quark-popover" onClick={handleClick}>
          基本使用
        </div>
      </Popover>
    </div>
  );
}

function PullRefreshDemo() {
  const [loading, setloading] = useState(false);
  const [count, setCount] = useState(0);

  const onload = () => {
    setloading(true);
    setTimeout(() => {
      console.log('刷新成功');

      setloading(false);
      setCount((count) => count + 1);
    }, 1000);
  };

  return (
    <div style={{ overflowY: 'auto', height: 200 }}>
      <PullRefresh dark loading={loading} onRefresh={onload}>
        <div slot="content" className="pull-content">
          刷新次数: {count}
        </div>
      </PullRefresh>
    </div>
  );
}

function ShareSheetDemo() {
  const showBase = () => {
    ShareSheet({
      options: [
        {
          name: '微信',
          icon: 'https://m.hellobike.com/resource/helloyun/16682/LY3mn00VTX.png',
        },
        {
          name: '微信朋友圈',
          icon: 'https://m.hellobike.com/resource/helloyun/16682/QOiMPs9BLj.png',
        },
        {
          name: 'QQ',
          icon: 'https://m.hellobike.com/resource/helloyun/16682/J4TWX9Jpca.png',
        },
        {
          name: 'QQ空间',
          icon: 'https://m.hellobike.com/resource/helloyun/16682/wG7wG2CHQx.png',
        },
        {
          name: '微博',
          icon: 'https://m.hellobike.com/resource/helloyun/16682/vt_vyR3M8I.png',
        },
        {
          name: '二维码',
          icon: 'https://m.hellobike.com/resource/helloyun/16682/hvu4xjJpNY.png',
        },
      ],
      select: (index, action) => {},
      cancel: () => {},
      close: () => {},
    });
  };

  return (
    <div>
      <div onClick={showBase} title="基本使用">
        1111
      </div>
    </div>
  );
}

function SwipeCellDemo() {
  return (
    <SwipeCell>
      <Cell title="这是标题" desc="描述文字" />
      <div slot="left">
        <Button type="primary" shape="square">
          选择
        </Button>
      </div>
      <div slot="right">
        <Button type="danger" shape="square">
          删除
        </Button>
        <Button type="primary" shape="square">
          收藏
        </Button>
      </div>
    </SwipeCell>
  );
}

function TooltipDemo() {
  const [open, setOpen] = useState(false);
  const handleClick = () => setOpen(true);
  const handleClose = () => setOpen(false);

  return (
    <div>
      <Tooltip tips="气泡文字" open={open} onClose={handleClose}>
        <div onClick={handleClick}>top 位置</div>
      </Tooltip>
    </div>
  );
}

function CascadeDemo() {
  const DATA = [
    {
      text: '浙江',
      children: [
        {
          text: '杭州',
          children: [{ text: '西湖区' }, { text: '余杭区' }],
        },
        {
          text: '温州',
          children: [{ text: '鹿城区' }, { text: '瓯海区' }],
        },
      ],
    },
    {
      text: '福建',
      children: [
        {
          text: '福州',
          children: [{ text: '鼓楼区' }, { text: '台江区' }],
        },
        {
          text: '厦门',
          children: [{ text: '思明区' }, { text: '海沧区' }],
        },
      ],
    },
    {
      text: '北京',
      children: [
        {
          text: '',
          children: [{ text: '' }],
        },
      ],
    },
  ];
  const [open, setOpen] = useState(false);
  const pickerRef = useRef<CascadeRef>(null);

  const handleClose = () => setOpen(false);

  const handleConfirm = () => {
    const { current } = pickerRef;
    const values = current
      .getValues()
      .map((column) => {
        return column.value;
      })
      .join('，');
    console.log(`当前选中：${values}`);
    setOpen(false);
  };

  useEffect(() => {
    // 模拟异步获取数据
    setTimeout(() => {
      const { current: pickerCurrent } = pickerRef;
      pickerCurrent.setColumns(DATA);
    }, 1000);
  }, []);

  const handleClick = () => setOpen(true);

  return (
    <div className="demo">
      <div onClick={handleClick}>即联选择器</div>
      <Cascade title="请选择地区" ref={pickerRef} open={open} onClose={() => setOpen(false)}>
        <div slot="header" className="head-container">
          <span className="cancel" onClick={handleClose}>
            取消
          </span>
          <span className="picker-title">请选择地区</span>
          <span className="ensure" onClick={handleConfirm}>
            确定
          </span>
        </div>
      </Cascade>
    </div>
  );
}

function CheckboxDemo() {
  const [groupValue, setGroupValue] = useState(['苹果', '橘子']);

  const onGroupChange = ({ detail }) => {
    setGroupValue(() => detail.value);
  };

  return (
    <>
      <CheckboxGroup value={groupValue.join()} onChange={onGroupChange}>
        <Checkbox name="apple">苹果</Checkbox>
        <Checkbox name="warning" disabled>
          橘子
        </Checkbox>
        <Checkbox name="banana">香蕉</Checkbox>
      </CheckboxGroup>
    </>
  );
}

function InputDemo() {
  const fieldRef = useRef<InputRef>(null);
  useEffect(() => {
    console.log('11122', fieldRef);
    const { current } = fieldRef;
    current.setRules([
      {
        validator: (val) => {
          if (!/^1[3456789]\d{9}$/.test(val)) {
            return false;
          }
          return true;
        },
        message: '请输入正确的手机号',
      },
    ]);
  }, []);
  return <Input placeholder="请输入文本" label="文本" ref={fieldRef} />;
}

function PickerDemo() {
  const [open, setOpen] = useState(false);
  const pickerRef = useRef<PickerRef>(null);

  const handleClose = () => setOpen(false);

  const handleConfirm = ({ detail }) => {
    const values = detail.value
      .map((column) => {
        return column.value;
      })
      .join('，');
    console.log(`当前选中：${values}`);
    setOpen(false);
  };

  useEffect(() => {
    // 模拟异步获取数据
    setTimeout(() => {
      const { current: pickerCurrent } = pickerRef;
      pickerCurrent.setColumns([
        {
          defaultIndex: 2,
          values: ['星期一', '星期二', '星期三', '星期四', '星期五'],
        },
        {
          defaultIndex: 0,
          values: ['上午', '下午'],
        },
      ]);
    }, 1000);
  }, []);

  const handleClick = () => setOpen(true);

  return (
    <div>
      <div onClick={handleClick}>基本使用</div>
      <Picker title="请选择时间" ref={pickerRef} open={open} onClose={handleClose} onConfirm={handleConfirm} />
    </div>
  );
}

function DatetimePickerDemo() {
  const [open, setOpen] = useState(false);
  const minDate = '2020-10-01';
  const maxDate = '2025-08-13';
  const currentDate = '2021-11-30';

  const handleClose = () => setOpen(false);

  const handleConfirm = ({ detail }) => {
    Toast.text(`${detail.value}`);
    setOpen(false);
  };

  const handleClick = () => setOpen(true);

  const handleChange = ({ detail }) => {
    Toast.text(`${detail.value}`);
  };

  return (
    <div className="demo">
      <div onClick={handleClick}>选择年月日</div>
      <DatetimePicker
        title="选择年月日"
        type="date"
        open={open}
        value={currentDate}
        mindate={minDate}
        maxdate={maxDate}
        onClose={handleClose}
        onConfirm={handleConfirm}
        onChange={handleChange}
      />
    </div>
  );
}

function RadioDemo() {
  const [value1, setValue1] = useState('apple');
  const handleChange = ({ detail }) => {
    setValue1(() => detail.value);
  };

  return (
    <>
      <RadioGroup value={value1} onChange={handleChange}>
        <Radio name="apple">苹果</Radio>
        <Radio name="banana">香蕉</Radio>
      </RadioGroup>
    </>
  );
}

function FormDemo() {
  const [form, setForm] = useState({
    name: '',
    password: '',
    other: {
      age: 18,
    },
  });
  const formRef = useRef<FormRef>(null);

  useEffect(() => {
    formRef.current.setModel(form);
    formRef.current.setRules({
      name: [{ required: true, message: '请输入姓名' }],
      password: { required: true, message: '请输入密码' },
      other: {
        age: [{ required: true, message: '请输入年龄' }],
      },
    });
  }, []);

  const submit = () => {
    formRef.current.validate((valid, res) => {
      console.log('submit', valid, res);
    });
  };

  const reset = () => {
    formRef.current.resetFields();
  };

  return (
    <Fragment>
      <Form ref={formRef}>
        <FormItem prop="name" label="姓名">
          <Input placoholder="姓名" />
        </FormItem>
        <FormItem prop="password" label="密码">
          <Input placoholder="密码" type="password" />
        </FormItem>
        <FormItem prop="other.age" label="年龄">
          <Input value={form.other.age} placoholder="年龄" />
        </FormItem>
      </Form>
      <div className="flex-box">
        <Button type="primary" size="big" onClick={submit}>
          提交
        </Button>
        <Button size="big" onClick={reset}>
          重置
        </Button>
      </div>
    </Fragment>
  );
}

export default App;
// export default ListDemo;
// export default VirtualListDemo;
// export default TabDemo;
// export default ActionSheetDemo;
// export default DialogDemo;
// export default OverlayDemo
// export default PopoverDemo;
// export default PullRefreshDemo;
// export default ShareSheetDemo;
// export default SwipeCellDemo;
// export default TooltipDemo;
// export default CascadeDemo;
// export default CheckboxDemo;
// export default InputDemo;
// export default PickerDemo;
// export default DatetimePickerDemo;
// export default RadioDemo;
// export default FormDemo;
