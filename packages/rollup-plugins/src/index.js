const queryRE = /\?.*$/s;
const hashRE = /#.*$/s;

const cleanUrl = (url) => url.replace(hashRE, '').replace(queryRE, '');

module.exports = function (options = {}) {
  if (!options.variableMap) {
    throw Error('options.variableMap is required');
  }
  if (typeof options.variableMap !== 'object') {
    throw Error('options.variableMap should be an object');
  }
  options.prefix = options.prefix || 'cssvar-';

  const reg = new RegExp(`${options.prefix}([a-zA-Z_]+)`, 'gi');
  return {
    name: 'css-variable',
    transform(code, url) {
      if (cleanUrl(url).indexOf('.css') > -1) {
        return code.replace(reg, (matchStr, key) => {
          const val = options.variableMap[key];
          if (!val) {
            return matchStr;
          }
          return val;
        });
      }
      return code;
    },
  };
};
