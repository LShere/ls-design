用于自定义`map`传入的变量替换, 服务于`@ls-design/ui`

## ⚙️ 安装

```bash
npm install @ls-design/rollup-plugin-css-variable
```

## 🚀 使用


```ts
// rollup.config.ts

import { defineConfig } from 'rollup';
import cssVariable from '@ls-design/rollup-plugin-css-variable';
import variableMap from './global-css';


export default defineConfig({
  plugins: [
    variableMap, // 对象形式 { a: '#ccc' }
    prefix: 'ls-', // 识别到前缀替换，成上面variableMap的value
  ],
  ....
})
```