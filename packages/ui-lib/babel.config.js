module.exports = {
  presets: [
    [
      '@babel/preset-env',
      {
        targets:
          'chrome >= 53, edge >= 79, firefox >= 63, opera >= 40, safari >= 10.1, ChromeAndroid >= 53,  FirefoxAndroid >= 63, ios_saf >= 10.3, Samsung >= 6.0',
      },
    ],
    ['@babel/preset-typescript'],
  ],
  plugins: [['@babel/plugin-transform-runtime'], ['@babel/plugin-proposal-decorators', { version: 'legacy' }]],
};
