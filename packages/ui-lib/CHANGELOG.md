# @ls-design/ui

## 1.0.2

### Patch Changes

- 修改图片及链接地址

## 1.0.1

### Patch Changes

- publish to npm
- Updated dependencies
  - @ls-design/core@1.0.1
  - @ls-design/icons@1.0.1
