import { defineConfig } from 'rollup';
import commonjs from '@rollup/plugin-commonjs';
import { nodeResolve } from '@rollup/plugin-node-resolve';
import { babel } from '@rollup/plugin-babel';
import typescript from '@rollup/plugin-typescript';
import terser from '@rollup/plugin-terser';
import json from '@rollup/plugin-json';
import postcss from 'rollup-plugin-postcss';
import cssVariable from '@ls-design/rollup-plugin-css-variable';
import alias from '@rollup/plugin-alias';
import pxToVp from 'postcss-px-to-viewport';
import variableMap from './global-css';

import * as fs from 'fs';
import * as path from 'path';

// const pkgJson = JSON.parse(fs.readFileSync(path.resolve(__dirname, 'package.json')).toString());
// const dependencies = Object.keys(pkgJson.dependencies ?? {});

const pkgSrcRoot = path.resolve(__dirname, './src/components');

const componentNames = fs
  .readdirSync(pkgSrcRoot, { withFileTypes: true })
  .filter((dirent) => dirent.isDirectory())
  .map((item) => ({ name: item.name, path: `${item.name}/index` }))
  // 加上汇总index
  .concat({ path: 'index', name: 'index' });

console.log('===components', componentNames);

const extensions = ['.js', '.ts', '.tsx'];

const plugins = [
  cssVariable({
    variableMap,
    prefix: 'ls-',
  }),
  alias({
    entries: [{ find: '@', replacement: path.resolve(__dirname, './src') }],
  }),
  postcss({
    plugins: [
      pxToVp({
        unitToConvert: 'px',
        viewportWidth: 375,
        unitPrecision: 5,
        propList: ['*'],
        viewportUnit: 'vw',
        fontViewportUnit: 'vw',
        selectorBlackList: [],
        minPixelValue: 1,
        mediaQuery: false,
        replace: true,
        exclude: undefined,
        include: undefined,
        landscape: false,
      }),
    ],
    inject: false,
    extensions: ['.css'],
  }),
  typescript(),
  json(),
  commonjs(),
  nodeResolve({
    extensions,
  }),
  babel({
    babelHelpers: 'runtime',
    exclude: 'node_modules/**',
    extensions,
  }),
  terser(),
];

export default defineConfig([
  {
    input: componentNames.reduce((prev, curr) => {
      prev[curr.path] = `${pkgSrcRoot}/${curr.name}`;
      return prev;
    }, {}),
    output: [
      {
        dir: 'dist/es',
        chunkFileNames: '[name].chunk.js',
        format: 'es',
      },
    ],
    plugins,
    treeshake: false,
    // external: dependencies,
  },
  {
    input: './src/components/index.ts',
    output: [
      {
        file: 'dist/umd/index.js',
        format: 'umd',
        name: 'LSDesign',
      },
    ],
    plugins,
  },
]);
