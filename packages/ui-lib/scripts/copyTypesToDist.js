const fs = require('fs');
const path = require('path');
const { execSync } = require('child_process');

const typesRoot = path.resolve(__dirname, '../types');
const typesCompRoot = path.resolve(__dirname, '../types/components');
const distEsRoot = path.resolve(__dirname, '../dist/es');

const componentNames = fs
  .readdirSync(path.join(__dirname, '../src/components'), { withFileTypes: true })
  .filter((dirent) => dirent.isDirectory())
  .map((dirent) => dirent.name);

// 复制types到dist
execSync(`cp ${typesCompRoot}/index.d.ts ${distEsRoot}/`, { stdio: 'inherit' });
componentNames.forEach((componentName) => {
  execSync(`cp -r ${typesCompRoot}/${componentName}/* ${distEsRoot}/${componentName}/`);
});

// 删除types
execSync(`rm -rf ${typesRoot}`, { stdio: 'inherit' });
