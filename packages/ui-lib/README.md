<p align="center">
  <img src="https://common-1258034138.cos.ap-guangzhou.myqcloud.com/ls-design%2Flogo.svg" align="center" width="100" />
<br />
<h3 align="center">LS-Design</h3>
<h2 align="center">基于Web Components的移动端优先组件库</h2>
</p>
   

> ### 文档网站放本地了，自己起docker run就行
>
> 不是买不起服务器，而是md更有性价比

   

## ⚙️ 安装

```bash
# vue或vanilla
npm install @ls-design/ui --save

# react
npm install @ls-design/ui-react --save
```
   

## 🚀 使用示例

```vue
<!-- vue -->
<template>
  <ls-button>普通按钮</ls-button>
</template>

<script>
  import '@ls-design/ui/dist/es/button'
</script>
```

```html
<!-- vanilla -->

<ls-button>普通按钮</ls-button>

<script type="module">
  import '@ls-design/ui/dist/es/button'
</script>
```
   

## 📖 组件列表
   

> 只提供了`react`文档，`vue`或`vanilla`方式如上 通过`<ls-组件名></ls-组件名>` 使用
> 其他命令式的组件可参照下方`react`文档
   

- ### 基本组件
  - [button 按钮](https://gitee.com/LShere/ls-design/tree/master/packages/ui-lib/src/components/button/zh-CN-react.md)
  - [cell 单元格](https://gitee.com/LShere/ls-design/tree/master/packages/ui-lib/src/components//cell/zh-CN-react.md)
  - [icon 图标](https://gitee.com/LShere/ls-design/tree/master/packages/ui-lib/src/components/icon/zh-CN-react.md)
  - [image 图片](https://gitee.com/LShere/ls-design/tree/master/packages/ui-lib/src/components/icon/zh-CN-react.md)
- ### 数据组件
  - [badge 徽标](https://gitee.com/LShere/ls-design/tree/master/packages/ui-lib/src/components/badge/zh-CN-react.md)
  - [grid 宫格](https://gitee.com/LShere/ls-design/tree/master/packages/ui-lib/src/components/grid/zh-CN-react.md)
  - [imagePreview 图片预览](https://gitee.com/LShere/ls-design/tree/master/packages/ui-lib/src/components/imagePreview/zh-CN-react.md)
  - [list 列表](https://gitee.com/LShere/ls-design/tree/master/packages/ui-lib/src/components/list/zh-CN-react.md)
  - [virtuallist 虚拟列表](https://gitee.com/LShere/ls-design/tree/master/packages/ui-lib/src/components/virtuallist/zh-CN-react.md)
  - [marquee 跑马灯](https://gitee.com/LShere/ls-design/tree/master/packages/ui-lib/src/components/marquee/zh-CN-react.md)
  - [noticebar 通知栏](https://gitee.com/LShere/ls-design/tree/master/packages/ui-lib/src/components/noticebar/zh-CN-react.md)
  - [progress 进度条](https://gitee.com/LShere/ls-design/tree/master/packages/ui-lib/src/components/progress/zh-CN-react.md)
  - [circle 环形进度条](https://gitee.com/LShere/ls-design/tree/master/packages/ui-lib/src/components/circle/zh-CN-react.md)
  - [skeleton 骨架屏](https://gitee.com/LShere/ls-design/tree/master/packages/ui-lib/src/components/skeleton/zh-CN-react.md)
  - [steps 步骤条](https://gitee.com/LShere/ls-design/tree/master/packages/ui-lib/src/components/steps/zh-CN-react.md)
  - [sticky 粘性布局](https://gitee.com/LShere/ls-design/tree/master/packages/ui-lib/src/components/sticky/zh-CN-react.md)
  - [swipe 轮播图](https://gitee.com/LShere/ls-design/tree/master/packages/ui-lib/src/components/swipe/zh-CN-react.md)
  - [tag 标签](https://gitee.com/LShere/ls-design/tree/master/packages/ui-lib/src/components/tag/zh-CN-react.md)
- ### 导航组件
  - [navbar 导航栏](https://gitee.com/LShere/ls-design/tree/master/packages/ui-lib/src/components/navbar/zh-CN-react.md)
  - [tab 标签页](https://gitee.com/LShere/ls-design/tree/master/packages/ui-lib/src/components/tab/zh-CN-react.md)
  - [tabbar 底部导航栏](https://gitee.com/LShere/ls-design/tree/master/packages/ui-lib/src/components/tabbar/zh-CN-react.md)
- ### 操作反馈
  - [actionsheet 动作面板](https://gitee.com/LShere/ls-design/tree/master/packages/ui-lib/src/components/actionsheet/zh-CN-react.md)
  - [collapse 折叠面板](https://gitee.com/LShere/ls-design/tree/master/packages/ui-lib/src/components/collapse/zh-CN-react.md)
  - [dialog 模态框](https://gitee.com/LShere/ls-design/tree/master/packages/ui-lib/src/components/dialog/zh-CN-react.md)
  - [loading 加载动画](https://gitee.com/LShere/ls-design/tree/master/packages/ui-lib/src/components/loading/zh-CN-react.md)
  - [overlay 遮罩层](https://gitee.com/LShere/ls-design/tree/master/packages/ui-lib/src/components/overlay/zh-CN-react.md)
  - [popover 气泡弹出框](https://gitee.com/LShere/ls-design/tree/master/packages/ui-lib/src/components/popover/zh-CN-react.md)
  - [popup 底部弹框](https://gitee.com/LShere/ls-design/tree/master/packages/ui-lib/src/components/popup/zh-CN-react.md)
  - [pullrefresh 下拉刷新](https://gitee.com/LShere/ls-design/tree/master/packages/ui-lib/src/components/pullrefresh/zh-CN-react.md)
  - [sharesheet 分享面板](https://gitee.com/LShere/ls-design/tree/master/packages/ui-lib/src/components/sharesheet/zh-CN-react.md)
  - [toast 消息提示](https://gitee.com/LShere/ls-design/tree/master/packages/ui-lib/src/components/toast/zh-CN-react.md)
  - [tooltip 文字提示](https://gitee.com/LShere/ls-design/tree/master/packages/ui-lib/src/components/tooltip/zh-CN-react.md)
  - [swipecell 滑动单元格](https://gitee.com/LShere/ls-design/tree/master/packages/ui-lib/src/components/swipecell/zh-CN-react.md)
- ### 表单组件
  - [cascade 级联选择器](https://gitee.com/LShere/ls-design/tree/master/packages/ui-lib/src/components/cascade/zh-CN-react.md)
  - [checkbox 多选框](https://gitee.com/LShere/ls-design/tree/master/packages/ui-lib/src/components/checkbox/zh-CN-react.md)
  - [input 输入框](https://gitee.com/LShere/ls-design/tree/master/packages/ui-lib/src/components/input/zh-CN-react.md)
  - [datetimepicker 日期时间选择器](https://gitee.com/LShere/ls-design/tree/master/packages/ui-lib/src/components/datetimepicker/zh-CN-react.md)
  - [picker 选择器](https://gitee.com/LShere/ls-design/tree/master/packages/ui-lib/src/components/picker/zh-CN-react.md)
  - [radio 单选框](https://gitee.com/LShere/ls-design/tree/master/packages/ui-lib/src/components/radio/zh-CN-react.md)
  - [rate 评分](https://gitee.com/LShere/ls-design/tree/master/packages/ui-lib/src/components/rate/zh-CN-react.md)
  - [stepper 步进器](https://gitee.com/LShere/ls-design/tree/master/packages/ui-lib/src/components/stepper/zh-CN-react.md)
  - [switch 开关](https://gitee.com/LShere/ls-design/tree/master/packages/ui-lib/src/components/switch/zh-CN-react.md)
  - [textarea 多行文本框](https://gitee.com/LShere/ls-design/tree/master/packages/ui-lib/src/components/textarea/zh-CN-react.md)
  - [upload 图片上传](https://gitee.com/LShere/ls-design/tree/master/packages/ui-lib/src/components/upload/zh-CN-react.md)
- ### 其它组件
  - [empty 空状态](https://gitee.com/LShere/ls-design/tree/master/packages/ui-lib/src/components/empty/zh-CN-react.md)
  - [countdown 倒计时](https://gitee.com/LShere/ls-design/tree/master/packages/ui-lib/src/components/countdown/zh-CN-react.md)
  
   

---
   



### 🙏 `ls-design`参考自著名开源项目 [quark-design](https://vue-quarkd.hellobike.com/#/)
> `quark-design`提供了诸多灵感和组件编写思路
