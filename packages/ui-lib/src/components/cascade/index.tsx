import BScroll from '@better-scroll/core';
import Wheel from '@better-scroll/wheel';
import { Property, createRef, CustomElement, State, LSElement } from '@ls-design/core';
import '../popup';
import '../button';
import Locale from '../locale';
import '@ls-design/icons/dist/es/close';
import style from './style.css';

BScroll.use(Wheel);
export interface PickerColumn {
  text: string;
  children?: PickerColumn[];
}
export interface SelectedColumn {
  value: string;
  index: number;
}
export interface Props {
  open: boolean;
  name?: string;
  title?: string;
  bottomhidden?: boolean;
  forbidmaskclick?: boolean;
}

export interface CustomEvent {
  close?: () => void;
  comfirm?: (e: { detail: { value: SelectedColumn[] } }) => void;
  change?: (e: { detail: { value: SelectedColumn[] } }) => void;
}
@CustomElement({
  tag: 'ls-cascade',
  style,
})
class Cascade extends LSElement {
  @Property({ type: Boolean })
  open = false;

  @Property()
  title = '';

  @Property()
  name = '';

  @Property({ type: Boolean })
  bottomhidden = false;

  @Property({ type: Boolean })
  forbidmaskclick = false;

  @State
  pickerData: string[][] = [];

  columns: PickerColumn[] = [];

  wheels: any[] = [];

  selectedIndexPair: number[] = [];

  depth = 1;

  wheelWrapper: any = createRef();

  setColumns(columns: PickerColumn[]) {
    if (!columns || columns.length < 1) {
      return;
    }
    this.columns = columns;
    const { current } = this.wheelWrapper;
    if (!current) {
      return;
    }

    // 对于级联选择器要求数据嵌套深度保持一致，因此取第一个计算深度
    const firstColumn = this.columns[0];
    this.depth = this.getDepths(firstColumn, 1);
    this.selectedIndexPair = new Array(this.depth).fill(0);
    if (this.depth <= 1) {
      return;
    }

    this.loadInitPickerData();

    for (let i = 0; i < this.depth; i += 1) {
      this.createWheel(current, i);
    }
  }

  getDepths(column: PickerColumn, depth: number): number {
    if (!column.children || column.children.length <= 0) {
      return depth;
    }
    depth += 1;
    return this.getDepths(column.children[0], depth);
  }

  loadInitPickerData() {
    let cursor: PickerColumn[] = this.columns;
    const tempPickerData = [...this.pickerData];
    for (let i = 0; i < this.depth; i += 1) {
      const values = cursor.map((item) => item.text);
      tempPickerData.splice(i, 1, values);
      cursor = cursor[0].children;
    }
    this.pickerData = tempPickerData;
  }

  createWheel = (wheelWrapper: any, i: number) => {
    if (!this.wheels[i]) {
      this.wheels[i] = new BScroll(wheelWrapper.children[i], {
        wheel: {
          selectedIndex: this.selectedIndexPair[i] || 0,
          wheelWrapperClass: 'ls-cascade-picker-wheel-scroll',
          wheelItemClass: 'ls-cascade-picker-wheel-item',
        },
      });
      // when any of wheels'scrolling ended , refresh data
      this.wheels[i].on('scrollEnd', () => {
        const currentSelectedIndexPair = this.wheels.map((wheel) => wheel.getSelectedIndex());
        this.changePickerData(currentSelectedIndexPair, this.selectedIndexPair);
        this.selectedIndexPair = currentSelectedIndexPair;
        const selectValues = this.getValues(false);
        this.$emit('change', { detail: { value: selectValues } });
      });
    } else {
      this.wheels[i].refresh();
    }
    return this.wheels[i];
  };

  changePickerData(newIndexPair: number[], oldIndexPair: number[]) {
    const tempPickerData = [...this.pickerData];
    let cursor: PickerColumn[] = this.columns;
    for (let i = 0; i < this.depth - 1; i += 1) {
      if (newIndexPair[i] !== oldIndexPair[i]) {
        for (let j = 0; j <= i; j += 1) {
          cursor = cursor[newIndexPair[j]].children;
        }

        for (let j = i; j < this.depth - 1; j += 1) {
          const value = cursor.map((item) => item.text);
          tempPickerData.splice(j + 1, 1, value);
          cursor = cursor[0].children;
        }
        // 让后续 wheel 滚动到第一个位置
        // for (let j = i + 1; j < this.depth; j += 1) {
        //   this.wheels[j].wheelTo(0, 10);
        // }
        break;
      }
    }
    this.pickerData = tempPickerData;
    setTimeout(() => {
      for (let i = 0; i < this.depth; i++) {
        this.wheels[i].refresh();
      }
    }, 1);
  }

  getValues(needRestore = true) {
    if (needRestore) {
      this.restorePosition();
    }

    const currentSelectedIndexPair = this.wheels.map((wheel) => wheel.getSelectedIndex());
    const selectValues = this.pickerData.map((column, i) => {
      const index = currentSelectedIndexPair[i];
      return {
        value: column[index],
        index,
      };
    });

    this.selectedIndexPair = currentSelectedIndexPair;
    return selectValues;
  }

  restorePosition() {
    this.wheels.forEach((wheel) => {
      wheel.restorePosition();
    });
  }

  popupClose = () => {
    this.restorePosition();
    this.$emit('close');
  };

  confirm = () => {
    const selectValues = this.getValues();
    this.$emit('confirm', { detail: { value: selectValues } });
  };

  renderWheel = () => {
    if (!this.pickerData || this.pickerData.length <= 0) {
      return null;
    }
    const wheels = this.pickerData.map((column) => (
      <div class="ls-cascade-picker-wheel">
        <ul class="ls-cascade-picker-wheel-scroll">
          {column.map((item: string) => (
            <li class="ls-cascade-picker-wheel-item">{item}</li>
          ))}
        </ul>
      </div>
    ));
    return wheels;
  };

  render() {
    return (
      <ls-popup
        open={this.open}
        position="bottom"
        safearea
        round
        forbidmaskclick={this.forbidmaskclick}
        onclose={this.popupClose}
      >
        <div class="ls-cascade-picker">
          <div class="ls-cascade-picker-header">
            <slot name="header">
              <span class="ls-cascade-picker-title">{this.title}</span>
              <div class="ls-cascade-picker-close-btn">
                <ls-icon-close size="25" onclick={this.popupClose} />
              </div>
            </slot>
          </div>
          <div class="ls-cascade-picker-content">
            <div class="ls-cascade-picker-mask-top"></div>
            <div class="ls-cascade-picker-mask-bottom"></div>
            <div class="ls-picker-current">
              <div class="ls-picker-current-mask"></div>
            </div>
            <div class="ls-cascade-picker-wheel-wrapper" ref={this.wheelWrapper}>
              {this.renderWheel()}
            </div>
          </div>
          {!this.bottomhidden && (
            <div class="ls-cascade-picker-bottom">
              <ls-button type="primary" size="big" onclick={this.confirm}>
                {Locale.current.confirm}
              </ls-button>
            </div>
          )}
        </div>
      </ls-popup>
    );
  }
}

export default Cascade;
