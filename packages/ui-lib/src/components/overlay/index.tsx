import { disableBodyScroll, enableBodyScroll, clearAllBodyScrollLocks } from 'body-scroll-lock';
import { Property, CustomElement, Fragment, createRef, LSElement } from '@ls-design/core';
import style from './style.css';
export interface Props {
  open: boolean;
  zindex?: number;
}
export interface CustomEvent {
  close: () => void;
}
@CustomElement({
  tag: 'ls-overlay',
  style,
})
class Overlay extends LSElement {
  @Property({
    type: Boolean,
  })
  open = false;

  @Property()
  zindex: number | string = 999;

  wrap: any = createRef();

  componentDidMount() {
    if (this.zindex) {
      this.style.zIndex = `${this.zindex}`;
    }
  }

  componentDidUpdate(propName: string, oldValue: string, newValue: string) {
    if (propName === 'open' && this.wrap && this.wrap.current) {
      const { current } = this.wrap;
      if (newValue) {
        disableBodyScroll(current);
      } else {
        enableBodyScroll(current);
      }
    }
  }

  componentWillUnmount() {
    clearAllBodyScrollLocks();
  }

  handleMaskClick = () => {
    this.$emit('close');
  };

  render() {
    return (
      <Fragment>
        <div class="ls-overlay" ref={this.wrap}>
          <slot></slot>
        </div>
        <div class="ls-overlay-mask" onClick={this.handleMaskClick} />
      </Fragment>
    );
  }
}

export default Overlay;
