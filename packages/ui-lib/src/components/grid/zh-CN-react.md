### Grid

---


#### 简介


宫格，可以在水平方向上把页面分隔成等宽度的区块


#### 安装


```ts
import { Grid, GridItem } from '@ls-design/ui-react';
```


#### 基本使用


通过 `icon` 属性设置格子内的图标，`text` 属性设置文字内容。


```tsx
<Grid>
  <GridItem
    icon="图标地址"
    text="文字"
  />
  <GridItem
    icon="图标地址"
    text="文字"
  />
  <GridItem
    icon="图标地址"
    text="文字"
  />
  <GridItem
    icon="图标地址"
    text="文字"
  />
</Grid>
```


#### 自定义列数


默认一行展示四个格子，可以通过 `column` 自定义列数。


```tsx
<Grid column="3">
  <GridItem
    icon="图标地址"
    text="文字"
  />
  <GridItem
    icon="图标地址"
    text="文字"
  />
  <GridItem
    icon="图标地址"
    text="文字"
  />
  <GridItem
    icon="图标地址"
    text="文字"
  />
  <GridItem
    icon="图标地址"
    text="文字"
  />
  <GridItem
    icon="图标地址"
    text="文字"
  />
</Grid>
```


#### 正方形格子


设置 `square` 属性后，格子的高度会和宽度保持一致。


```tsx
<Grid square>
  <GridItem
    icon="图标地址"
    text="文字"
  />
  <GridItem
    icon="图标地址"
    text="文字"
  />
  <GridItem
    icon="图标地址"
    text="文字"
  />
</Grid>
```


#### 无边框


设置 noborder 属性后，将不显示边框。


```tsx
<Grid noborder>
  <GridItem
    icon="图标地址"
    text="文字"
  />
  <GridItem
    icon="图标地址"
    text="文字"
  />
  <GridItem
    icon="图标地址"
    text="文字"
  />
  <GridItem
    icon="图标地址"
    text="文字"
  />
</Grid>
```


#### 自定义内容


通过默认插槽可以自定义格子展示的内容。


```tsx
<Grid>
  <GridItem>
    <img
      src="图标地址"
      style="width: 40px;"
    />
  </GridItem>
  <GridItem>
    <img
      src="图标地址"
      style="width: 40px;"
    />
  </GridItem>
  <GridItem>
    <img
      src="图标地址"
      style="width: 40px;"
    />
  </GridItem>
</Grid>
```


#### Props


| 参数     | 解释                   | 类型      | 默认值  |
| -------- | ---------------------- | --------- | ------- |
| column   | 显示列数               | `string`  | `4`     |
| noborder | 是否隐藏边框           | `boolean` | `false` |
| square   | 是否将格子固定为正方形 | `boolean` | `false` |


#### GridItem Props


| 参数     | 说明       | 类型     | 默认值 |
| -------- | ---------- | -------- | ------ |
| text     | 显示的文字 | `string` |        |
| icon     | 显示的图标 | `string` |        |
| iconsize | 图标大小   | `string` | `28px` |


#### 样式变量


提供了 CSS 变量 可自定义样式


| 名称                           | 说明                   | 默认值    |
| ------------------------------ | ---------------------- | --------- |
| `--grid-item-background-color` | 格子背景色             | `#FFFFFF` |
| `--grid-item-text-font-size`   | 格子文字大小           | `12px`    |
| `--grid-item-text-color`       | 格子文字颜色           | `#242729` |
| `--grid-item-text-margin-top`  | 格子文字距 icon 的距离 | `8px`     |
| `--grid-item-icon-font-size`   | icon 大小              | `28px`    |
| `--grid-item-hspacing`         | 格子内容左右内边距     | `16px`    |
| `--grid-item-vspacing`         | 格子内容上下内边距     | `16px`    |