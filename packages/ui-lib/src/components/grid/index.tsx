import { CustomElement, Property, createRef, State, LSElement } from '@ls-design/core';
import { slotAssignedElements } from '../../utils/common';
import style from './style.css';
import itemStyle from './itemStyle.css';

export interface Props {
  column?: number;
  noborder?: boolean;
  square?: boolean;
}
export interface ItemProps {
  text?: string;
  icon?: string;
  iconsize?: string;
}

@CustomElement({
  tag: 'ls-grid-item',
  style: itemStyle,
})
class LSGridItem extends LSElement {
  @Property()
  text = '';

  @Property()
  icon = '';

  @Property()
  iconsize?: string = undefined;

  @State
  style: any = {};

  setStyle = (style: any) => {
    this.style = style;
  };

  renderIcon = () => {
    if (!this.icon) {
      return null;
    }
    if (this.icon && this.icon.includes('http')) {
      return <img src={this.icon} class="ls-grid-icon" style={{ width: this.iconsize }} />;
    }
    return null;
  };

  render() {
    return (
      <div class="ls-grid-item" style={this.style}>
        <slot>
          {this.renderIcon()}
          <span class="ls-grid-text">{this.text}</span>
        </slot>
      </div>
    );
  }
}

export { LSGridItem };

@CustomElement({
  tag: 'ls-grid',
  style,
})
class LSGrid extends LSElement {
  @Property()
  column = '4';

  @Property({
    type: Boolean,
  })
  noborder = false; // 是否显示边框

  @Property({
    type: Boolean,
  })
  square = false; // 是否将格子固定为正方形

  slotRef: any = createRef();

  getNodeStyle = (index: number) => {
    const assignedNodes = this.slotRef.current?.assignedNodes();
    const nodes = slotAssignedElements(assignedNodes);
    const borderWidth = 0.5;
    let borderBottom: string | undefined = `${borderWidth}px solid #dddddd`;
    let borderRight: string | undefined = `${borderWidth}px solid #dddddd`;

    const column = parseInt(this.column);
    const itemWidth = (document.body.clientWidth - (column - 1) * borderWidth) / column;
    if ((index + 1) % column === 0 || index === nodes.length - 1) {
      // 最后一个或最后一列
      borderRight = undefined;
    }

    const rows = nodes.length % column > 0 ? Math.floor(nodes.length / column) + 1 : nodes.length / column;

    const currentRow = (index + 1) % column > 0 ? Math.floor((index + 1) / column) + 1 : (index + 1) / column;

    if (currentRow === rows) {
      // 最后一行
      borderBottom = undefined;
    }

    if (this.noborder) {
      // 无边框
      borderRight = undefined;
      borderBottom = undefined;
    }

    return {
      width: `${itemWidth}px`,
      height: this.square ? `${itemWidth}px` : 'auto',
      borderRight,
      borderBottom,
    };
  };

  handleSlotChange = () => {
    const { current } = this.slotRef;
    if (!current) {
      return;
    }

    const nodes = slotAssignedElements(current.assignedNodes());
    nodes.forEach((node: Element, i: number) => {
      const style = this.getNodeStyle(i);
      // @ts-ignore
      node.setStyle(style);
    });
  };

  render() {
    return (
      <div class="ls-grid">
        <slot onslotchange={this.handleSlotChange} ref={this.slotRef}></slot>
      </div>
    );
  }
}
export default LSGrid;
