import { Property, createRef, CustomElement, LSElement } from '@ls-design/core';
import '../picker';
import '@ls-design/icons/dist/es/close';
import style from './style.css';
import { clamp, padZero, times } from './utils';
import type { SelectColumn } from '.';
import type { FilterType, FormatterType } from './datePicker';

type TimeType = 'time';

@CustomElement({
  tag: 'ls-time-picker',
  style,
})
class TimePicker extends LSElement {
  @Property({ type: Boolean })
  open = false;

  @Property({ type: String })
  value: string | null;

  @Property({ type: String })
  type: TimeType = 'time';

  @Property({ type: String })
  title = '';

  @Property({ type: Number })
  minhour = 0;

  @Property({ type: Number })
  maxhour = 23;

  @Property({ type: Number })
  minminute = 0;

  @Property({ type: Number })
  maxminute = 59;

  @Property({ type: Boolean })
  showtoolbar = false;

  @Property({ type: String })
  confirmbuttontext = '';

  @Property({ type: String })
  cancelbuttontext = '';

  @Property({ type: Boolean })
  forbidmaskclick = false;

  pickerRef: any = createRef();
  originColumns: { defaultIndex: number; type: string; values: string[] }[] = [];
  columns: { defaultIndex: number; type: string; values: string[] }[] = [];
  currentTime: string | null = null;
  formatter: FormatterType = (type: string, value: string): string => value;
  filter: FilterType = (type: string, value: string[]): string[] => value;

  formatValue(value) {
    if (!value) {
      value = `${padZero(this.minhour)}:${padZero(this.minminute)}`;
    }
    let [hour, minute] = value.split(':');
    hour = padZero(clamp(Number(hour), Number(this.minhour), Number(this.maxhour)));
    minute = padZero(clamp(Number(minute), Number(this.minminute), this.maxminute));
    return `${hour}:${minute}`;
  }

  // 计算范围
  calcRanges() {
    return [
      {
        type: 'hour',
        range: [Number(this.minhour), Number(this.maxhour)],
      },
      {
        type: 'minute',
        range: [Number(this.minminute), Number(this.maxminute)],
      },
    ];
  }

  setColumns() {
    const ranges = this.calcRanges();
    const [hour, minute] = this.currentTime.split(':');
    const getDefaultIndex = (values, value) => {
      const index = values.indexOf(value);
      return index > -1 ? index : 0;
    };
    this.originColumns = ranges.map(({ type, range }) => {
      let values = times(range[1] - range[0] + 1, (index) => padZero(range[0] + index));
      if (this.filter) {
        values = this.filter(type, values);
      }
      return {
        defaultIndex: type === 'hour' ? getDefaultIndex(values, hour) : getDefaultIndex(values, minute),
        type,
        values,
      };
    });

    this.columns = this.originColumns.map(({ type, defaultIndex, values }) => {
      return {
        type,
        defaultIndex,
        values: values.map((value) => this.formatter(type, value)),
      };
    });
    this.pickerRef.current.setColumns(this.columns);
  }

  updateInnerValue(detail) {
    const values: number[] = detail.value.map((item) => parseInt(item.value, 10));
    const [hour, minute] = values;
    this.currentTime = this.formatValue(`${hour}:${minute}`);
    this.$emit('change', { detail: { value: this.currentTime } });
    this.setColumns();
  }

  onClose = () => {
    this.$emit('close');
  };

  confirm = () => {
    this.$emit('confirm', { detail: { value: this.currentTime } });
  };

  onChange = ({ detail }) => {
    this.updateInnerValue(detail);
  };

  componentDidMount() {
    this.currentTime = this.formatValue(this.value);
    this.setColumns();
  }

  getValues(): SelectColumn[] {
    return this.pickerRef.current.getValues();
  }

  setValue(value: string) {
    this.currentTime = this.formatValue(value);
    this.setColumns();
  }

  setFormatter(formatter: (type: string, value: string) => string) {
    this.formatter = formatter;
    this.setColumns();
  }

  setFilter(filter: (type: string, values: string[]) => string[]) {
    this.filter = filter;
    this.setColumns();
  }

  render() {
    return (
      <ls-picker
        ref={this.pickerRef}
        title={this.title}
        bottomhidden={this.showtoolbar}
        open={this.open}
        forbidmaskclick={this.forbidmaskclick}
        onclose={this.onClose}
        onchange={this.onChange}
        onconfirm={this.confirm}
      >
        {this.showtoolbar && (
          <div slot="header" class="ls-date-picker-header">
            <span class="ls-date-picker-close-btn" onclick={this.onClose}>
              {this.cancelbuttontext}
            </span>
            <span class="ls-date-picker-title">{this.title}</span>
            <span class="ls-date-picker-confirm-btn" onclick={this.confirm}>
              {this.confirmbuttontext}
            </span>
          </div>
        )}
      </ls-picker>
    );
  }
}

export default TimePicker;
