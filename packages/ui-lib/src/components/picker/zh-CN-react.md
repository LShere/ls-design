### Picker

---


#### 简介


选择器，支持单列选择和多列选择


#### 安装使用


```jsx
import { Picker, PickerRef } from "@ls-design/ui-react";
```


#### 基础用法


```js
export default () => {
  const [open, setOpen] = useState(false);
  const pickerRef = useRef<PickerRef>(null);

  const handleClose = () => setOpen(false);

  const handleConfirm = ({ detail }) => {
    const values = detail.value
      .map((column) => {
        return column.value;
      })
      .join("，");
    console.log(`当前选中：${values}`);
    setOpen(false);
  };

  useEffect(() => {
    // 模拟异步获取数据
    setTimeout(() => {
      const { current: pickerCurrent } = pickerRef;
      pickerCurrent.setColumns([
        {
          defaultIndex: 2,
          values: ["星期一", "星期二", "星期三", "星期四", "星期五"],
        },
        {
          defaultIndex: 0,
          values: ["上午", "下午"],
        },
      ]);
    }, 1000);
  }, []);

  const handleClick = () => setOpen(true);

  return (
    <div>
      <div onClick={handleClick}>基本使用</div>
      <Picker
        title="请选择时间"
        ref={pickerRef}
        open={open}
        onClose={handleClose}
        onConfirm={handleConfirm}
      />
    </div>
  );
};
```


#### Props


| 参数            | 说明                                       | 类型                                             | 默认值     |
| --------------- | ------------------------------------------ | ------------------------------------------------ | ---------- |
| open            | picker 是否显示                            | `boolean`                                        | `require`  |
| title           | 标题                                       | `string `                                        |
| confirmtext     | 确定按钮的文字                             | `string`                                         | `确认`     |
| bottomhidden    | 是否隐藏底部按钮（通常配合自定义头部使用） | `boolean`                                        | `false`    |
| forbidmaskclick | 是否禁止遮罩层点击                         | `boolean`                                        | `false`    |
| onClose         | 点击遮罩或者取消按钮                       | `() => void`                                     | `require ` |
| onConfirm       | 确定按钮点击回调                           | `（e: {detail:{value: SelectColumn[]}}）=> void` | `require`  |
| onChange        | picker 改变回调                            | `（e: {detail:{value: SelectColumn[]}}）=> void` | -          |


#### Methods


| 名称       | 说明                                           | 类型                                |
| ---------- | ---------------------------------------------- | ----------------------------------- |
| setColumns | 用于设置选择器数据                             | `(columns: PickerColumn[]) => void` |
| getValues  | 获取选择器选中的数据，通常配合自定义头部时使用 | `（）=> SelectColumn[]`             |


#### Slots


| 名称        | 说明       |
| ----------- | ---------- |
| header | 自定义头部 |


#### 类型定义

```js
type PickerColumn = {
  values: string[]
  defaultIndex: number
};

type SelectColumn = {
  value: string
  index: number
};
```



#### 样式变量


组件提供了以下 CSS变量，可用于自定义样式


| 名称                         | 说明     | 默认值                           |
| ---------------------------- | -------- | -------------------------------- |
| `--picker-title-font-size`   | 标题字号 | `18px`                           |
| `--picker-title-color`       | 标题颜色 | `#242729`                        |
| `--picker-title-font-weight` | 标题字重 | `500`                            |
| `--picker-title-font-family` | 标题字体 | `PingFangSC-Medium, PingFang SC` |
