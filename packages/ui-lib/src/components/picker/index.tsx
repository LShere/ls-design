import BScroll from '@better-scroll/core';
import Wheel from '@better-scroll/wheel';
import { Property, createRef, CustomElement, State, LSElement } from '@ls-design/core';
import '../popup';
import '../button';
import '@ls-design/icons/dist/es/close';
import style from './style.css';
import Locale from '../locale';
export interface PickerColumn {
  values: string[];
  defaultIndex: number;
}
export interface SelectColumn {
  value: string;
  index: number;
}
export interface Props {
  open: boolean;
  title?: string;
  confirmtext?: string;
  bottomhidden?: boolean;
  forbidmaskclick?: boolean;
}
export interface CustomEvent {
  close: () => void;
  confirm: (e: { detail: { value: { value: string; index: number }[] } }) => void;
  change?: (e: { detail: { value: { value: string; index: number }[] } }) => void;
}

BScroll.use(Wheel);
@CustomElement({
  tag: 'ls-picker',
  style,
})
class Picker extends LSElement {
  @Property({ type: Boolean })
  open = false;

  @Property({ type: Boolean })
  forbidmaskclick = false;

  @Property()
  name = '';

  @Property()
  title = '';

  @Property()
  confirmtext = '';

  @Property({ type: Boolean })
  bottomhidden = false;

  @State
  columns: PickerColumn[] = [];

  wheels: any[] = [];

  values: any[] = [];

  wheelWrapper: any = createRef();

  setColumns(columns: PickerColumn[]) {
    this.columns = columns;
    const { current } = this.wheelWrapper;
    if (!current) {
      return;
    }
    for (let i = 0; i < this.columns.length; i += 1) {
      this.createWheel(current, i);
    }
  }

  getValues(needRestore = true) {
    if (needRestore) {
      this.restorePosition();
    }

    const currentSelectedIndexPair = this.wheels.map((wheel) => {
      return wheel.getSelectedIndex();
    });
    const selectValues = this.columns.map((column, i) => {
      const index = currentSelectedIndexPair[i];
      return {
        value: column.values[index],
        index: index,
      };
    });

    return selectValues;
  }

  restorePosition() {
    this.wheels.forEach((wheel) => {
      wheel.restorePosition();
    });
  }

  confirm = () => {
    const selectValues = this.getValues();
    this.values = selectValues;
    this.$emit('confirm', { detail: { value: selectValues } });
  };

  debounce(fn, wait) {
    let timer: ReturnType<typeof setTimeout> | null = null;
    return function () {
      if (timer !== null) {
        clearTimeout(timer);
      }
      timer = setTimeout(fn, wait);
    };
  }

  createWheel = (wheelWrapper: any, i: number) => {
    if (!this.wheels[i]) {
      const column = this.columns[i];
      this.wheels[i] = new BScroll(wheelWrapper.children[i], {
        wheel: {
          selectedIndex: column.defaultIndex || 0,
          wheelWrapperClass: 'ls-picker-wheel-scroll',
          wheelItemClass: 'ls-picker-wheel-item',
        },
      });
      this.wheels[i].on(
        'scrollEnd',
        this.debounce(() => {
          const selectValues = this.getValues(false);
          this.$emit('change', { detail: { value: selectValues } });
        }, 30),
      );
    } else {
      this.wheels[i].refresh();
      this.wheels[i].wheelTo(this.columns[i].defaultIndex, 10);
    }
    return this.wheels[i];
  };

  componentDidMount = () => {};

  renderWheel = () => {
    if (!this.columns) {
      return null;
    }
    const wheels = this.columns.map((column) => {
      return (
        <div class="ls-picker-wheel">
          <ul class="ls-picker-wheel-scroll">
            {column.values.map((item: string) => {
              return <li class="ls-picker-wheel-item">{item}</li>;
            })}
          </ul>
        </div>
      );
    });
    return wheels;
  };
  popupClose = () => {
    this.restorePosition();
    this.$emit('close');
  };

  render() {
    return (
      <ls-popup
        open={this.open}
        position="bottom"
        safearea
        round
        forbidmaskclick={this.forbidmaskclick}
        onclose={this.popupClose}
      >
        <div class="ls-picker-container">
          <div class="ls-picker-header">
            <slot name="header">
              <span class="ls-picker-title">{this.title}</span>
              <div class="ls-picker-header-close-btn">
                <ls-icon-close size="25" onclick={this.popupClose} />
              </div>
            </slot>
          </div>
          <div class="ls-picker-content">
            <div class="ls-picker-mask-top"></div>
            <div class="ls-picker-mask-bottom"></div>
            <div class="ls-picker-current">
              <div class="ls-picker-current-mask"></div>
            </div>
            <div class="ls-picker-wheel-wrapper" ref={this.wheelWrapper}>
              {this.renderWheel()}
            </div>
          </div>
          {!this.bottomhidden && (
            <div class="ls-picker-bottom">
              <ls-button type="primary" size="big" onclick={this.confirm}>
                {this.confirmtext || Locale.current.confirm}
              </ls-button>
            </div>
          )}
        </div>
      </ls-popup>
    );
  }
}

export default Picker;
