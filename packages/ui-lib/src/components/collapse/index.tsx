import { Property, createRef, CustomElement, LSElement } from '@ls-design/core';
import '@ls-design/icons/dist/es/arrow-bottom';
import style from './style.css';

export interface Props {
  open?: boolean;
  title: string;
}

@CustomElement({
  tag: 'ls-collapse',
  style,
})
class Collapse extends LSElement {
  @Property({
    type: Boolean,
  })
  open = false;

  @Property({
    type: String,
  })
  title = '';

  detailsEl: any = createRef();

  componentDidMount() {
    if (this.open) {
      const { current } = this.detailsEl;
      current.open = true;
    }
  }

  shouldComponentUpdate(propName: string, oldValue: string | boolean, newValue: string | boolean): boolean {
    if (propName === 'open' && oldValue !== newValue) {
      const { current } = this.detailsEl;
      current.open = newValue;
    }
    return true;
  }

  render() {
    return (
      <details ref={this.detailsEl}>
        <summary>
          <slot name="title">{this.title}</slot>
          <slot name="icon">
            <ls-icon-arrow-bottom class="collapse-icon" size="16" />
          </slot>
          <div className="line"></div>
        </summary>

        <div class="expander-content">
          <slot></slot>
        </div>
      </details>
    );
  }
}

export default Collapse;
