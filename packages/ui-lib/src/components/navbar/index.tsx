import { Fragment, Property, CustomElement, LSElement } from '@ls-design/core';

import '@ls-design/icons/dist/es/arrow-left';
import '@ls-design/icons/dist/es/close';
import style from './style.css';
export interface Props {
  title: string;
  lefthide?: boolean;
  closehide?: boolean;
  righttext?: string;
  safearea?: boolean;
  iconsize?: string;
}
export interface CustomEvent {
  leftclick?: () => void;
  rightclick?: () => void;
  close?: () => void;
}
@CustomElement({
  tag: 'ls-navbar',
  style,
})
class Navbar extends LSElement {
  @Property()
  title = '';

  @Property()
  righttext = '';

  @Property()
  iconsize = '24';

  @Property({ type: Boolean })
  lefthide = false;

  @Property({ type: Boolean })
  closehide = false;

  leftClick = () => {
    this.$emit('leftclick');
  };

  rightClick = () => {
    this.$emit('rightclick');
  };

  closeClick = () => {
    this.$emit('close');
  };

  render() {
    return (
      <Fragment>
        {!this.lefthide && (
          <slot name="left" class="ls-navbar-left">
            <ls-icon-arrow-left id="left" size={this.iconsize} onClick={this.leftClick} class="back" />
            {!this.closehide && (
              <ls-icon-close id="left" size={this.iconsize} onClick={this.closeClick} class="close" />
            )}
          </slot>
        )}
        <div class="ls-navbar-title">
          <div>{this.title}</div>
        </div>
        <slot name="right" class="ls-navbar-right icon">
          <span id="right" onClick={this.rightClick}>
            {this.righttext}
          </span>
        </slot>
      </Fragment>
    );
  }
}

export default Navbar;
