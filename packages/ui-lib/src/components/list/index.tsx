import { Property, createRef, CustomElement, LSElement } from '@ls-design/core';
import delay from '../../utils/delay';
import { checkFalse } from '../../utils/common';
import { getUnuseParent } from '../../utils';
import style from './style.css';
import Locale from '../locale';

import '../loading';

export interface Props {
  loading: boolean;
  finished: boolean;
  error?: boolean;
  offset?: number;
  loadingtext?: string;
  finishedtext?: string;
  errortext?: string;
  textcolor?: string;
}
export interface CustomEvent {
  load?: () => void;
  reload?: () => void;
}

@CustomElement({
  tag: 'ls-list',
  style,
})
class LSList extends LSElement {
  @Property({
    type: Boolean,
  })
  error = false;

  @Property({
    type: Number,
  })
  offset = 300;

  @Property({
    type: Boolean,
  })
  finished = false;

  @Property()
  errortext = '';

  @Property()
  textcolor = '#879099';

  @Property()
  loadingtext = Locale.current.loading;

  @Property()
  finishedtext = '';

  @Property({
    type: Boolean,
  })
  loading = false;

  @Property({
    type: Boolean,
  })
  immediatecheck = true;

  placeholderRef: any = createRef();

  componentDidMount() {
    if (this.immediatecheck) {
      setTimeout(() => this.check({ auto: true }), 50);
    }
    window.addEventListener('scroll', this.check, true);
  }

  componentWillUnmount() {
    window.removeEventListener('scroll', this.check, true);
  }

  shouldComponentUpdate(propName: string, oldValue: string, newValue: string): boolean {
    if (propName === 'loading') {
      if (!checkFalse(oldValue) && checkFalse(newValue)) {
        this.check({ auto: true });
      }
    }
    return true;
  }

  check = delay(({ auto = false }) => {
    if (this.loading || !checkFalse(this.finished) || !checkFalse(this.error) || (getUnuseParent(this) && !auto)) {
      return;
    }
    if (this.placeholderRef && this.placeholderRef.current) {
      const { offset } = this;
      const placeholderRect = this.placeholderRef.current.getBoundingClientRect();

      const isReachEdge = placeholderRect.bottom - window.screen.height <= offset;

      if (isReachEdge) {
        this.$emit('load');
      }
    }
  });

  clickErrorText = () => {
    this.$emit('reload');
    this.check({ auto: true });
  };

  renderLoading = () => {
    return (
      <slot name="loading">
        <div class="ls-list-text">
          <ls-loading type="circular" color={this.textcolor} size="15">
            {this.loadingtext}
          </ls-loading>
        </div>
      </slot>
    );
  };

  renderFinishedText = () => {
    if (this.finishedtext) {
      return (
        <div class="ls-list-text" style={`color: ${this.textcolor}`}>
          {this.finishedtext}
        </div>
      );
    }
    return <slot name="finished"></slot>;
  };

  renderErrorText = () => {
    if (this.errortext) {
      return (
        <div class="ls-list-text" style={`color: ${this.textcolor}`} onClick={this.clickErrorText}>
          {this.errortext}
        </div>
      );
    }
    return <slot name="error" onClick={this.clickErrorText}></slot>;
  };

  render() {
    return (
      <div class="ls-list">
        <slot name="content"></slot>
        {this.loading && !this.finished && this.renderLoading()}
        {this.finished && this.renderFinishedText()}
        {this.error && this.renderErrorText()}
        <div ref={this.placeholderRef}></div>
      </div>
    );
  }
}
export default LSList;
