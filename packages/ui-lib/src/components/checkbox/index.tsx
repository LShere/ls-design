import { CustomElement, Property, State, createRef, LSElement } from '@ls-design/core';
import { slotAssignedElements, checkFalse } from '../../utils/common';
import style from './style.css';
export interface Props {
  shape?: 'round' | 'square';
  size?: 'normal' | 'big';
  disabled?: boolean;
  checked?: boolean;
}
export interface CustomEvent {
  change?: (e: { detail: { value: string } }) => void;
}

export interface GroupProps {
  value?: string;
}
export interface GroupCustomEvent {
  change: (e: { detail: { value: string[] } }) => void;
}
@CustomElement({
  tag: 'ls-checkbox',
  style,
})
class Checkbox extends LSElement {
  @Property()
  shape = 'round';

  @Property()
  size = 'normal';

  @Property()
  name = '';

  @Property({
    type: Boolean,
  })
  value = false;

  @Property({
    type: Boolean,
  })
  disabled = false;

  @Property({
    type: Boolean,
  })
  checked = false;

  @State
  classNames = '';

  slotRef: any = createRef();

  componentDidUpdate(): void {
    if (this.value !== this.checked) {
      this.value = this.checked;
    }
  }

  handleCheck = () => {
    if (this.disabled) {
      return;
    }
    this.$emit('change', {
      detail: {
        value: !this.checked,
      },
    });
  };

  render() {
    return (
      <div class="ls-checkbox-container" onClick={this.handleCheck}>
        <span class="ls-checkbox">
          <input type="checkbox" />
          <span class="ls-checkbox-show"></span>
          <img
            class="ls-checkbox-icon"
            src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACgAAAAoCAMAAAC7IEhfAAAAAXNSR0IArs4c6QAAANJQTFRFAAAA////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////hovIawAAAEV0Uk5TAAMFBwkKDA0PExYYGhwfISIkJykqLC0wMzY6RkpOUml4e4CUp7W4vMDDxsfK0dXY2drd3+Di5OXm5+nr7e/x9vf4+fr+srIpUgAAAJtJREFUGBntwUVyAgEARcEf3C24WxLc3Qm8+1+Joih2A8OGDTXdsljeItTalvSCyAQOXzIVnQIDmYrNgVVCBpy1n6zu4ktg8S0jZdjndJNcAbOYDFWAQ15XqTUwjcqYuwMcC5LSG2Ac1iOeNvBfVGYLjEJ6zN0ETtUdMAzqGVeDm35Azzn/uOr5ZcbxC3R9MmevnxtevcQmy0e6AIbeGU9gwCbMAAAAAElFTkSuQmCC"
          />
          <span class="ls-checkbox-disabled"></span>
        </span>
        <label for="checkbox" class={this.classNames}>
          <slot ref={this.slotRef}></slot>
        </label>
      </div>
    );
  }
}

export default Checkbox;

@CustomElement({
  tag: 'ls-checkbox-group',
  style,
})
class CheckboxGroup extends LSElement {
  @Property()
  value = '';

  slotRef: any = createRef();

  componentDidMount(): void {
    this.$on('change', this.eventListener);
  }

  shouldComponentUpdate(propName: string, oldValue: string, newValue: string): boolean {
    if (propName === 'value' && oldValue !== newValue) {
      this.init();
    }
    return true;
  }

  init() {
    const assignedNodes = this.slotRef.current?.assignedNodes();
    const nodes = slotAssignedElements(assignedNodes);
    const value = this.value ? this.value.split(',') : [];
    if (nodes?.length) {
      nodes.forEach((item: any) => {
        if (value.includes(item.name)) {
          item.checked = true;
        } else {
          item.checked = false;
        }
      });
    }
  }

  handleSlotChange = () => {
    this.init();
  };

  eventListener = (ev: any) => {
    if (ev.target === this) {
      return;
    }
    const value = this.value ? this.value.split(',') : [];
    if (!checkFalse(ev.detail.value)) {
      value.push(ev.target.name);
    } else {
      value.splice(
        value.findIndex((name) => name === ev.target.name),
        1,
      );
    }
    this.$emit('change', {
      detail: { value },
    });
    ev.stopPropagation();
  };

  render() {
    return (
      <div class="ls-checkbox-group-wrapper">
        <slot onslotchange={this.handleSlotChange} ref={this.slotRef}></slot>
      </div>
    );
  }
}

export { CheckboxGroup };
