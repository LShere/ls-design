import { Property, CustomElement, createRef, LSElement } from '@ls-design/core';

import style from './style.css';
export interface Props {
  type?: 'primary' | 'success' | 'danger' | 'warning';
  size?: 'small' | 'large';
  round?: boolean;
  plain?: boolean;
  light?: boolean;
  color?: string;
  textcolor?: string;
}
@CustomElement({ tag: 'ls-tag', style })
class Tag extends LSElement {
  @Property()
  type = '';

  @Property()
  color = '';

  @Property()
  textcolor = '';

  @Property()
  size = '';

  @Property({
    type: Boolean,
  })
  plain = false;

  @Property({
    type: Boolean,
  })
  round = false;

  @Property({
    type: Boolean,
  })
  light = false;

  wrap: any = createRef();

  render() {
    return (
      <span class="ls-tag" style={{ background: this.color, color: this.textcolor }}>
        <slot></slot>
      </span>
    );
  }
}

export default Tag;
