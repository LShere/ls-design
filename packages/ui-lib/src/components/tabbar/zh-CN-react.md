### Tabbar

---


#### 简介


底部导航栏


#### 安装使用


```jsx
import { Tabbar, TabbarItem } from "@ls-design/ui-react";
```


#### 基础用法


```html
<Tabbar onChange={onChange}>
  <TabbarItem>
    <ls-icon-home size="20" />
    <div>首页</div>
  </TabbarItem>
  <TabbarItem>
    <ls-icon-user size="20" />
    <div>我的</div>
  </TabbarItem>
</Tabbar>
```


#### Props


| 参数   | 说明               | 类型      | 默认值 |
| ------ | ------------------ | --------- | ------ |
| fixed  | 是否固定在底部     | `boolean` | `true` |
| active | 选中的名称或索引值 | `string`  | `0`    |


#### Tabbar Methods

| 名称   | 说明           | 类型                                                |
| ------ | -------------- | --------------------------------------------------- |
| change | 切换标签时触发 | `{e:{detail: {value: 选中的名称或索引值}}} => void` |


#### Slots


| 名称   | 说明           | 类型                                                |
| ------ | -------------- | --------------------------------------------------- |
| change | 切换标签时触发 | `{e:{detail: {value: 选中的名称或索引值}}} => void` |


#### 样式变量


组件提供了以下 CSS变量，可用于自定义样式


| 名称                    | 说明             | 默认值    |
| ----------------------- | ---------------- | --------- |
| `--tabbar-active-color` | 被激活菜单的颜色 | `#0088FF` |
