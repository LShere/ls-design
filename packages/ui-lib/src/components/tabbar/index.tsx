import { Property, createRef, CustomElement, LSElement } from '@ls-design/core';
import { slotAssignedElements } from '../../utils/common';
import style from './style.css';
import tabbarItemStyle from './tabbar-item.css';
export interface Props {
  fixed?: boolean;
  active?: string;
}
export interface CustomEvent {
  change: (e: { detail: { value: string } }) => void;
}
@CustomElement({ tag: 'ls-tabbar', style })
class Tabbar extends LSElement {
  @Property({
    type: Boolean,
  })
  fixed = false;

  @Property()
  active = '0';

  slotRef: any = createRef();

  shouldComponentUpdate(propName: string, oldValue: string, newValue: string): boolean {
    if (propName === 'active' && oldValue !== newValue) {
      const assignedNodes = this.slotRef.current?.assignedNodes();
      const elements = slotAssignedElements(assignedNodes);

      elements.forEach((item) => {
        if (item.getAttribute('index') === newValue) {
          item.setAttribute('active', '');
        } else {
          item.removeAttribute('active');
        }
      });
    }
    return true;
  }

  slotchange = () => {
    const assignedNodes = this.slotRef.current?.assignedNodes();
    const elements = slotAssignedElements(assignedNodes);

    elements.forEach((item, index) => {
      if (item.getAttribute('index') === null) {
        item.setAttribute('index', String(index));
      }

      item.addEventListener('click', this.eventListener);
      if (item.getAttribute('index') === this.active) {
        item.setAttribute('active', '');
      }
    });
  };

  eventListener = (e: any) => {
    this.active = e.currentTarget.getAttribute('index');

    this.$emit('change', {
      detail: { value: e.currentTarget.getAttribute('index') },
    });
  };

  render() {
    return (
      <div class="ls-tabbar">
        <slot ref={this.slotRef} onslotchange={this.slotchange}></slot>
      </div>
    );
  }
}

@CustomElement({ tag: 'ls-tabbar-item', style: tabbarItemStyle })
class TabbarItem extends LSElement {
  render() {
    return <slot></slot>;
  }
}

export { TabbarItem };

export default Tabbar;
