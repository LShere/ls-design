import { disableBodyScroll, enableBodyScroll, clearAllBodyScrollLocks } from 'body-scroll-lock';
import { Property, CustomElement, createRef, LSElement } from '@ls-design/core';

import style from './style.css';

export interface Props {
  title?: string;
  actions: Action[];
  cancelText?: string;
  titleColor?: string;
  titleFontSize?: number;
  cancelTextColor?: string;
  cancelTextFontSize?: number;
  select: (index: number, action: Action) => void;
  cancel?: () => void;
  close?: () => void;
  zIndex?: number;
}

interface Action {
  name: string;
  color?: string;
  fontSize?: number;
}

@CustomElement({
  tag: 'ls-actionsheet',
  style,
})
class ActionSheet extends LSElement {
  @Property({
    type: Boolean,
  })
  open = false;

  title: string | undefined = undefined;

  actions: Action[] = [];

  cancelText?: string = undefined;

  titleColor?: string = undefined;

  titleFontSize?: number = undefined;

  cancelTextColor?: string = undefined;

  cancelTextFontSize?: number = undefined;

  zIndex = 999;

  select: (index: number, action: Action) => void = () => {};

  cancel: () => void = () => {};

  close: () => void = () => {};

  wrap: any = createRef();

  componentDidMount() {
    if (this.zIndex) {
      this.style.zIndex = `${this.zIndex}`;
    }
    this.addEventListener('transitionend', this.handleTransitionend);
    this.addEventListener('click', this.handleHostClick);
  }

  shouldComponentUpdate(propName: string, oldValue: string | boolean, newValue: string | boolean): boolean {
    if (propName === 'open' && this.wrap && this.wrap.current) {
      const { current } = this.wrap;
      // 设置退出过渡动画
      if (newValue) {
        // 由关闭到打开
        current.classList.remove('ls-actionsheet-leave');
      } else if (oldValue && !newValue) {
        // 由打开到关闭
        current.classList.add('ls-actionsheet-leave');
      }
    }
    return true;
  }

  componentDidUpdate(propName: string, oldValue: string, newValue: string) {
    if (propName === 'open' && this.wrap && this.wrap.current) {
      const { current } = this.wrap;
      if (newValue) {
        disableBodyScroll(current);
      } else {
        enableBodyScroll(current);
      }
    }
  }

  componentWillUnmount() {
    clearAllBodyScrollLocks();
    this.removeEventListener('transitionend', this.handleTransitionend);
    this.removeEventListener('click', this.handleHostClick);
  }

  handleHostClick() {
    this.handleClick();
    if (this.close && typeof this.close === 'function') {
      this.close();
    }
  }

  handleClick = () => {
    this.open = false;
  };

  handleTransitionend(ev: TransitionEvent) {
    if (ev.propertyName === 'opacity' && !this.open) {
      document.body.removeChild(this);
    }
  }

  handleActionClick = (index: number, action: Action) => {
    this.handleClick();
    if (this.select && typeof this.select === 'function') {
      this.select(index, action);
    }
  };

  handleContainerClick(ev: PointerEvent) {
    ev.stopPropagation();
  }

  handleCancelClick = () => {
    this.handleClick();
    if (this.cancel && typeof this.cancel === 'function') {
      this.cancel();
    }
  };

  renderActions = () => {
    if (!this.actions || this.actions.length <= 0) {
      return null;
    }
    const actions = this.actions.map((action: Action, index: number) => {
      return (
        <div
          class="ls-actionsheet-actions"
          style={{
            color: action.color ? action.color : undefined,
            fontSize: action.fontSize ? `${action.fontSize}px` : undefined,
          }}
          onClick={() => {
            this.handleActionClick(index, action);
          }}
        >
          {action.name}
        </div>
      );
    });
    return actions;
  };

  render() {
    return (
      <div class="ls-actionsheet" ref={this.wrap} onClick={this.handleContainerClick}>
        {this.title && (
          <div
            class="ls-actionsheet-title"
            style={{
              color: this.titleColor ? this.titleColor : undefined,
              fontSize: this.titleFontSize ? this.titleFontSize : undefined,
            }}
          >
            {this.title}
          </div>
        )}
        <div class="ls-actionsheet-action">{this.renderActions()}</div>
        {this.cancelText && (
          <div class="ls-actionsheet-cancel">
            <div class="ls-actionsheet-cancel-gap"></div>
            <div
              class="ls-actionsheet-cancel-text"
              style={{
                color: this.cancelTextColor ? this.cancelTextColor : undefined,
                fontSize: this.cancelTextFontSize ? this.cancelTextFontSize : undefined,
              }}
              onClick={this.handleCancelClick}
            >
              {this.cancelText}
            </div>
          </div>
        )}
      </div>
    );
  }
}

// actionSheet只有函数调用的形式
export default function (params: Props): ActionSheet {
  const actionSheet = document.createElement('ls-actionsheet') as ActionSheet;

  const {
    title,
    actions,
    cancelText,
    titleColor,
    titleFontSize,
    cancelTextColor,
    cancelTextFontSize,
    select,
    cancel,
    close,
    zIndex,
  } = params;
  actionSheet.title = title;
  actionSheet.actions = actions;
  actionSheet.cancelText = cancelText;
  actionSheet.titleColor = titleColor;
  actionSheet.titleFontSize = titleFontSize;
  actionSheet.cancelTextColor = cancelTextColor;
  actionSheet.cancelTextFontSize = cancelTextFontSize;
  actionSheet.select = select;
  actionSheet.cancel = cancel;
  actionSheet.close = close;
  actionSheet.zIndex = zIndex;
  document.body.appendChild(actionSheet as unknown as HTMLElement);
  setTimeout(() => {
    actionSheet.open = true;
  }, 10);

  return actionSheet;
}
