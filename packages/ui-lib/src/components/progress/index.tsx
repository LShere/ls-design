import { Fragment, Property, CustomElement, LSElement } from '@ls-design/core';

import style from './style.css';
export interface Props {
  value: number;
  color?: string;
  showtext?: boolean;
}
@CustomElement({
  tag: 'ls-progress',
  style,
})
class LSProgress extends LSElement {
  @Property()
  color = 'linear-gradient(90deg, #FFC91C 0%, #FB990F 100%)';

  @Property()
  value = '';

  @Property({ type: Boolean })
  showtext = false;

  render() {
    return (
      <Fragment>
        <div class="wrap">
          <div class="progress active" style={{ background: this.color, width: `${this.value}%` }}></div>
        </div>
        <slot name="percent">{this.showtext && <span class="value">{this.value}%</span>}</slot>
      </Fragment>
    );
  }
}

export default LSProgress;
