### Icon

---


#### 简介


图标，需要使用前先安装


```bash
npm i @ls-design/icons
```


#### 基本使用


```tsx
// 使用

import '@ls-design/icons/dist/es/arrow-right';

export default () => {
  return <ls-icon-arrow-right />;
};
```


#### 图标大小


通过 `size` 属性用来设置图标的自定义大小，默认单位为 `px`。


```html
<ls-icon-user size="18" />
<ls-icon-user size="24" />
<ls-icon-user size="30" />
```


#### 图标颜色


通过 `color` 属性用来设置图标的颜色。


```html
<ls-icon-user size="40" color="#F44336" /> <ls-icon-user size="40" color="#3F51B5" />
```


#### Props


| 参数  | 解释                             | 参数类型             |
| :---- | :------------------------------- | :------------------- |
| color | 图标颜色                         | `string`             |
| size  | 图标大小，如 `20px` `2em` `2rem` | `string` \| `number` |
