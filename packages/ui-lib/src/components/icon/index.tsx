import { CustomElement, LSElement } from '@ls-design/core';
import style from './style.css';

@CustomElement({
  tag: 'ls-icon',
  style,
})
class LSIcon extends LSElement {
  render() {
    return <svg class="quark-icon-svg" aria-hidden="true" viewBox="0 0 1024 1024"></svg>;
  }
}

export default LSIcon;
