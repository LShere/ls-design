import { Property, CustomElement, createRef, LSElement } from '@ls-design/core';
import { slotAssignedElements } from '../../utils/common';
import style from './style.css';
import groupCss from './group.css';
export interface Props {
  direction?: 'horizontal' | 'vertical';
}
export interface ItemProps {
  title: string;
  status: 'done' | 'doing' | 'todo';
  content?: string;
  order?: string;
}
@CustomElement({ tag: 'ls-step', style })
class StepItem extends LSElement {
  @Property()
  order = '';

  @Property()
  status = '';

  @Property()
  title = '';

  @Property()
  content = '';

  containerRef: any = createRef();

  setDirection(direction: string) {
    const { current } = this.containerRef;
    if (current) {
      if (direction === 'vertical') {
        current.classList.remove('ls-step-horizontal');
      } else {
        current.classList.add('ls-step-horizontal');
      }
    }
  }

  render() {
    return (
      <div class="ls-step ls-step-horizontal" ref={this.containerRef}>
        <div class="ls-step">
          <div class="ls-step-head">
            <div class="ls-step-icon is-text">
              <div class="ls-step-inner">
                <slot name="order">{this.order}</slot>
              </div>
            </div>
          </div>
          <div class="ls-step-main">
            <div class="ls-step-title">
              <slot name="title">{this.title}</slot>
            </div>
            <div class="ls-step-content">
              <slot name="content">{this.content}</slot>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default StepItem;

@CustomElement({ tag: 'ls-steps', style: groupCss })
class Steps extends LSElement {
  @Property()
  direction = '';

  slotRef = createRef();

  handleSlotChange = () => {
    const { current } = this.slotRef;
    if (current) {
      const nodes = slotAssignedElements(current.assignedNodes());
      nodes.forEach((item: any) => {
        item.setDirection(this.direction || 'horizontal');
      });
    }
  };

  render() {
    return (
      <div id="step-container" class={this.direction === 'vertical' ? 'ls-vertical' : 'ls-steps ls-steps-horizontal'}>
        <slot onslotchange={this.handleSlotChange} ref={this.slotRef}></slot>
      </div>
    );
  }
}
export { Steps };
