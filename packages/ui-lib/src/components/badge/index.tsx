import { Property, State, createRef, CustomElement, LSElement } from '@ls-design/core';
import { classNames } from '../../utils/index';
import style from './style.css';

export interface Props {
  type?: 'dot' | 'round' | 'label';
  content?: string;
  size?: 'normal' | 'big';
  border?: boolean;
  max?: number;
}

@CustomElement({
  tag: 'ls-badge',
  style,
})
class LSBadge extends LSElement {
  @Property()
  type = 'round';

  @Property()
  content = '';

  @Property()
  size = 'normal';

  @Property({
    type: Boolean,
  })
  border = false;

  @Property()
  max = '99';

  @State
  classNames = '';

  slotRef: any = createRef();

  componentDidMount() {
    this.dealClass();
  }

  componentDidUpdate(propName: string, oldValue: string, newValue: string) {
    if (oldValue !== newValue) {
      this.dealClass();
    }
  }

  dealClass = () => {
    this.classNames = classNames('ls-badge-dealclass', {
      'ls-badge-fixed': this.slotRef.current?.assignedNodes().length,
      'ls-badge-hide': this.type !== 'dot' && (this.content === null || this.content === undefined || !this.content),
    });
  };

  renderContent = () => {
    if (/\d/g.test(this.content) && /\d/g.test(this.max) && Number(this.content) > Number(this.max)) {
      return '...';
    }
    return this.content;
  };

  render() {
    return (
      <div class="ls-badge">
        <div class={this.classNames}>{this.renderContent()}</div>
        <slot ref={this.slotRef} onslotchange={this.dealClass}></slot>
      </div>
    );
  }
}

export default LSBadge;
