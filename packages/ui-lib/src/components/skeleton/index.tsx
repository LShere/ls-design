import { CustomElement, Property, Fragment, LSElement } from '@ls-design/core';
import style from './style.css';
export interface Props {
  avatar?: boolean;
  avatarshape?: 'round' | 'square';
  title?: boolean;
  row: number;
  rowwidths?: string;
  hide?: boolean;
}
// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-ignore ,由于原生title 为 string类型，因此有冲突，所以此处使用了ignore
@CustomElement({
  tag: 'ls-skeleton',
  style,
})
class Skeleton extends LSElement {
  @Property({ type: Number })
  row = 1;

  @Property()
  rowwidths = '';

  @Property({ type: Boolean })
  avatar = false;

  @Property()
  avatarshape = 'round';

  @Property({ type: Boolean })
  hide = false;

  @Property({ type: Boolean })
  // eslint-disable-next-line @typescript-eslint/ban-ts-comment
  // @ts-ignore ,由于原生title 为 string类型，因此有冲突，所以此处使用了ignore
  title = false;

  getRowWidth = (index: number) => {
    let rowWidth = '100%';
    if (index === this.row - 1) {
      rowWidth = '60%';
    }

    if (this.rowwidths) {
      const splitWidths = this.rowwidths.split(',');
      if (splitWidths[index]) {
        rowWidth = splitWidths[index];
      }
    }
    return rowWidth;
  };

  render() {
    return (
      <Fragment>
        {!this.hide ? (
          <div class="skeleton-container">
            {this.avatar && <div class="skeleton-avatar"></div>}
            <div class="skeleton-content">
              {this.title && <h3 class="skeleton-title"></h3>}
              {new Array(Number(this.row)).fill(1).map((_, index) => {
                const rowWidth = this.getRowWidth(index);
                return (
                  <div
                    class="skeleton-row"
                    style={{
                      width: rowWidth,
                      marginTop: index === 0 && !this.title ? '0px' : undefined,
                    }}
                  ></div>
                );
              })}
            </div>
          </div>
        ) : (
          <slot></slot>
        )}
      </Fragment>
    );
  }
}

export default Skeleton;
