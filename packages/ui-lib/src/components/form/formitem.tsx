import { Property, CustomElement, createRef, LSElement, State } from '@ls-design/core';
import AsyncValidator from 'async-validator';

import '@ls-design/icons/dist/es/arrow-right';
import style from './formitem.css';
import { formTagNamesMap, getPropByPath, noop } from './utils';
import { debounce } from '../../utils/index';
import { slotAssignedElements } from '../../utils/common';
import type { IFormProps, Rules } from './type';

@CustomElement({
  tag: 'ls-form-item',
  style,
})
class FormItem extends LSElement {
  @Property({ type: String })
  prop: string;

  @Property({ type: String })
  label = '';

  @Property({ type: String })
  labelwidth = '';

  @Property({ type: Boolean })
  hidemessage = false;

  @Property({ type: Boolean })
  hideasterisk: false; // 是否隐藏必填 *

  @Property({ type: Boolean })
  islink = false;

  @State
  validateState = '';

  @State
  validateMessage = '';

  @State
  validateDisabled = false;

  @State
  itemNode = null;

  @State
  formRules: Rules | null = null;

  rules: Rules | null = null;

  @State
  formProps: IFormProps = {
    hideasterisk: false,
    hidemessage: false,
    labelwidth: '',
    labelsuffix: '',
    labelposition: 'left',
  };

  @State
  formModel = null;

  @State // 记录表单初始值，用于reset
  initialValue = null;

  defaultSlotRef: any = createRef();

  setFormProps(props: IFormProps) {
    this.formProps = props;
  }

  setFormModel(model) {
    if (!model || !this.prop) return;
    this.formModel = model;
    this.initialValue = getPropByPath(model, this.prop, true).v;
  }

  setRule(rule: Rules) {
    this.formRules = rule;
  }

  getRules(): Rules[] {
    const selfRules = this.rules;
    let formRules = this.formRules;
    if (formRules) {
      const prop = getPropByPath(formRules, this.prop || '', true);
      formRules = formRules ? prop.o[this.prop || ''] || prop.v : [];
    }
    return [].concat(selfRules || formRules || []);
  }

  validate(callback = noop) {
    this.validateDisabled = false;

    const rules = this.getRules();

    if (!rules || rules.length === 0) {
      callback();
      return true;
    }

    this.validateState = 'validating';

    const validator = new AsyncValidator({ [this.prop]: rules });
    validator.validate({ [this.prop]: this.getValue() }, { firstFields: true }, (errors, invalidFields) => {
      this.validateState = !errors ? 'success' : 'error';
      this.validateMessage = errors ? errors[0].message : '';
      callback(this.validateMessage, invalidFields);
    });
  }

  clearValidate() {
    this.validateState = '';
    this.validateMessage = '';
    this.validateDisabled = false;
  }

  resetField() {
    this.validateState = '';
    this.validateMessage = '';

    const prop = getPropByPath(this.formModel, this.prop, true);
    this.validateDisabled = true;
    if (Array.isArray(this.initialValue)) {
      prop.o[prop.k] = [].concat(this.initialValue);

      // uploader 恢复默认值
      if (this.itemNode.tagName === formTagNamesMap['LS-UPLOADER']) {
        const value = this.initialValue.map((item) => item.url || item.content);
        this.itemNode.setPreview(value);
      }
    } else {
      prop.o[prop.k] = this.initialValue;
    }

    setTimeout(() => {
      this.validateDisabled = false;
    }, 0);
  }

  getValue = () => {
    if (!this.prop) return;
    let value = null;
    if (this.itemNode) {
      const tagName = this.itemNode.tagName;
      if (
        tagName === formTagNamesMap['LS-RADIO'] ||
        tagName === formTagNamesMap['LS-CHECKBOX'] ||
        tagName === formTagNamesMap['LS-SWITCH']
      ) {
        value = this.itemNode.checked;
      } else if (tagName === formTagNamesMap['LS-UPLOADER']) {
        value = this.itemNode.values;
      } else {
        value = this.itemNode.value;
      }
    }
    if (this.formModel) {
      const prop = getPropByPath(this.formModel, this.prop, true);
      if (Array.isArray(value)) {
        prop.o[prop.k] = [].concat(value);
      } else {
        prop.o[prop.k] = value;
      }
    }
    return value;
  };

  defaultSlotChange = () => {
    if (!this.defaultSlotRef.current) return;
    const slotNodes = slotAssignedElements(this.defaultSlotRef.current?.assignedNodes());
    if (slotNodes.length > 0) {
      const formNode = slotNodes.find((node) => {
        return !!formTagNamesMap[node.tagName];
      });

      // 监听表单字段change blur
      if (formNode) {
        this.itemNode = formNode;
        this.itemNode.addEventListener('change', () => {
          this.onFieldChange();
        });
        if (
          this.itemNode.tagName === formTagNamesMap['LS-INPUT'] ||
          this.itemNode.tagName === formTagNamesMap['LS-TEXTAREA']
        ) {
          if (this.itemNode.disabled || this.itemNode.readonly) return;
          this.itemNode.addEventListener('blur', () => {
            this.onFieldBlur();
          });
        }
      }
    }
  };

  onFieldChange = debounce(() => {
    if (this.validateDisabled) return;
    this.validate();
  }, 200);

  onFieldBlur() {
    if (this.validateDisabled) return;
    this.validate();
  }

  isRequired() {
    const rules = this.getRules();
    return this.prop && rules && Array.isArray(rules) && rules.some((rule) => rule.required);
  }

  itemClass() {
    const classNames = ['ls-form-item'];
    this.validateState === 'success' ? classNames.push('is-success') : null;
    this.validateState === 'validating' ? classNames.push('is-validating') : null;

    if (this.hideasterisk || this.formProps.hideasterisk) {
      classNames.push('is-no-asterisk');
    }
    if (this.isRequired()) {
      classNames.push('is-required');
    }
    return classNames.join(' ');
  }

  errorMessageRender() {
    return (
      !this.formProps.hidemessage &&
      !this.hidemessage &&
      this.validateState === 'error' &&
      this.validateMessage && <div class="ls-form-item_error-msg">{this.validateMessage}</div>
    );
  }
  labelStyle() {
    const ret: any = {};
    if (this.formProps.labelwidth || this.labelwidth) {
      ret.width = this.labelwidth || this.formProps.labelwidth;
    }
    ret.textAlign = this.formProps.labelposition;
    return ret;
  }

  componentWillUnmount() {
    // 移除监听表单字段change blur
    if (this.itemNode) {
      this.itemNode.removeEventListener('change', () => {
        this.onFieldChange();
      });
      if (
        this.itemNode.tagName === formTagNamesMap['LS-INPUT'] ||
        this.itemNode.tagName === formTagNamesMap['LS-TEXTAREA']
      ) {
        if (this.itemNode.disabled || this.itemNode.readonly) return;
        this.itemNode.removeEventListener('blur', () => {
          this.onFieldBlur();
        });
      }
    }
  }

  render() {
    return (
      <div class={this.itemClass()}>
        <div class="ls-form-item__wrapper">
          <div class="ls-form-item__label-wrapper" style={this.labelStyle()}>
            <slot name="label">
              <div class="ls-form-item__label">
                {this.label}
                <span>{this.formProps.labelsuffix}</span>
              </div>
            </slot>
          </div>
          <div class="ls-form-item__main">
            <div class="ls-form-item__main-content">
              <slot ref={this.defaultSlotRef} onslotchange={this.defaultSlotChange} />
            </div>
            {this.errorMessageRender()}
          </div>
          <div class="ls-form-item__suffix">
            <slot name="suffix"></slot>
          </div>
          {this.islink && (
            <div class="ls-form-item__is-link">
              <ls-icon-arrow-right />
            </div>
          )}
        </div>
      </div>
    );
  }
}

export default FormItem;
