import { Fragment, Property, CustomElement, createRef, State, LSElement } from '@ls-design/core';
import style from './style.css';

export interface Props {
  title: string;
  speed?: number;
  paused?: boolean;
  reverse?: boolean;
}
@CustomElement({
  tag: 'ls-marquee',
  style,
})
export class LSMarquee extends LSElement {
  @Property()
  title = '';

  @Property()
  speed = '50';

  @Property({ type: Boolean })
  paused = false;

  @Property({ type: Boolean })
  reverse = false;

  @State
  animating = false;

  @State
  textWidth = 0;

  titleRef: any = createRef();

  componentDidMount(): void {
    this.start();
  }

  transitionEnd = () => {
    this.animating = false;
    this.start();
  };

  start = () => {
    const container = this;
    const text = this.titleRef.current;

    if (container.offsetWidth >= (this.textWidth || text.offsetWidth)) {
      this.animating = false;
      text.style.removeProperty('animation-duration');
      text.style.removeProperty('animation-name');
      return;
    }
    console.log(2222);

    if (this.animating) return;
    console.log(3333);

    const initial = !text.style.animationName;

    if (initial) {
      this.textWidth = text.offsetWidth;
      text.style.paddingLeft = `${container.offsetWidth}px`;
    }

    this.animating = true;
    text.style.animationDirection = this.reverse ? 'reverse' : 'normal';
    text.style.animationDuration = `${Math.round(text.offsetWidth / Number(this.speed))}s`;
    text.style.animationName = 'ls-marquee-animation';
  };

  render() {
    return (
      <Fragment>
        <span
          class={`ls-marquee-title ${this.paused ? 'ls-marquee-paused' : ''}`}
          ref={this.titleRef}
          onTransitionend={this.transitionEnd}
        >
          {this.title}
        </span>
      </Fragment>
    );
  }
}

export default LSMarquee;
