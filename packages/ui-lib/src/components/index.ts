import Button from './button';
import Loading from './loading';
import Icon from './icon';
import Image from './image';
import Locale, { zhCN, enUS } from './locale';
import Cell, { LSCellGroup as CellGroup } from './cell';
import Badge from './badge';
import Circle from './circle';
import Grid, { LSGridItem as GridItem } from './grid';
import Popup from './popup';
import imagepreview, { LSImagePreview as ImagePreivew } from './imagepreview';
import List from './list';
import VirtualList from './virtuallist';
import Marquee from './marquee';
import Noticebar from './noticebar';
import Progress from './progress';
import Skeleton from './skeleton';
import StepItem, { Steps } from './steps';
import Sticky from './sticky';
import Swipe, { SwipeItem } from './swipe';
import Tag from './tag';
import Navbar from './navbar';
import Tabs, { TabContent, TabNav } from './tab';
import Tabbar, { TabbarItem } from './tabbar';
import ActionSheet from './actionsheet';
import Collapse from './collapse';
import dialog, { Dialog } from './dialog';
import Overlay from './overlay';
import Popover from './popover';
import PullRefresh from './pullrefresh';
import ShareSheet from './sharesheet';
import SwipeCell from './swipecell';
import Tooltip from './tooltip';
import Cascade from './cascade';
import Checkbox, { CheckboxGroup } from './checkbox';
import Input from './input';
import Picker from './picker';
import DateTimePicker from './datetimepicker';
import Radio, { RadioGroup } from './radio';
import Rate from './rate';
import Stepper from './stepper';
import Textarea from './textarea';
import Switch from './switch';
import Form from './form';
import FormItem from './form/formitem';
import Upload from './upload';
import Empty from './empty';

export default {
  Button,
  Loading,
  Icon,
  Image,
  Cell,
  CellGroup,
  Badge,
  Circle,
  Grid,
  GridItem,
  Popup,
  ImagePreivew,
  imagepreview,
  List,
  VirtualList,
  Marquee,
  Noticebar,
  Progress,
  Skeleton,
  StepItem,
  Steps,
  Sticky,
  Swipe,
  SwipeItem,
  Tag,
  Navbar,
  Tabs,
  TabContent,
  TabNav,
  Tabbar,
  TabbarItem,
  ActionSheet,
  Collapse,
  Dialog,
  dialog,
  Overlay,
  Popover,
  PullRefresh,
  ShareSheet,
  SwipeCell,
  Tooltip,
  Cascade,
  Checkbox,
  CheckboxGroup,
  Input,
  Picker,
  DateTimePicker,
  Radio,
  RadioGroup,
  Rate,
  Stepper,
  Textarea,
  Switch,
  Form,
  FormItem,
  Upload,
  Empty,
  // Toast,
  Locale,
  zhCN,
  enUS,
};
