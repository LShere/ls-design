import { Property, State, CustomElement, createRef, LSElement } from '@ls-design/core';
import style from './style.css';
import { classNames } from '../../utils/index';

export interface PopoverAction {
  text: string;
  icon?: string;
  disabled?: boolean;
}
export interface Props {
  open: boolean;
  placement?:
    | 'top'
    | 'topleft'
    | 'topright'
    | 'left'
    | 'lefttop'
    | 'leftbottom'
    | 'right'
    | 'righttop'
    | 'rightbottom'
    | 'bottom'
    | 'bottomleft'
    | 'bottomright';
  zindex?: number;
  theme?: 'light' | 'dark';
  scrollhidden?: boolean;
}
export interface CustomEvent {
  close: () => void;
  select: (e: { detail: { action: PopoverAction; index: number } }) => void;
}

@CustomElement({
  tag: 'ls-popover',
  style,
})
class Popover extends LSElement {
  @Property()
  placement = 'bottom';

  @Property()
  theme = 'dark';

  @State
  actions: PopoverAction[] = [];

  @Property({
    type: Boolean,
  })
  open = false;

  @Property({
    type: Boolean,
  })
  scrollhidden = false;

  @Property()
  zindex = '999';

  tipsRef: any = createRef();

  componentDidMount() {
    if (this.zindex) {
      this.style.zIndex = this.zindex;
    }

    if (this.scrollhidden) {
      window.addEventListener('scroll', this.windowScrollListener);
    }

    document.addEventListener('click', (e) => {
      if (!e.composedPath().includes(this) && this.open) {
        // 打开状态，并且外界点击
        this.closeEmit();
      }
    });
  }

  shouldComponentUpdate(propName: string, oldValue: string | boolean, newValue: string | boolean): boolean {
    if (propName === 'open' && this.tipsRef && this.tipsRef.current) {
      const { current } = this.tipsRef;
      // 设置退出过渡动画
      if (newValue) {
        // 由关闭到打开
        current.classList.remove('ls-popover-leave');
      } else if (oldValue && !newValue) {
        // 由打开到关闭
        current.classList.add('ls-popover-leave');
      }
    }
    return true;
  }

  componentWillUnmount() {
    window.removeEventListener('scroll', this.windowScrollListener);
  }

  windowScrollListener = () => {
    if (this.open) {
      this.closeEmit();
    }
  };

  addRemoveAnnimation = () => {
    if (this.tipsRef && this.tipsRef.current) {
      const { current } = this.tipsRef;
      current.classList.add('ls-popover-leave');
    }
  };

  setActions = (actions: PopoverAction[]) => {
    if (!actions || actions.length <= 0) return;
    this.actions = actions;
  };

  closeEmit = () => {
    this.addRemoveAnnimation();
    this.open = false;
    this.$emit('close');
  };

  handleActionClick = (ev: PointerEvent, index: number) => {
    ev.stopPropagation();
    this.$emit('select', {
      detail: {
        action: this.actions[index],
        index: index,
      },
    });
  };

  renderIcon = (icon?: string) => {
    if (icon && icon.includes('http')) {
      return <img src={icon} style={{ marginRight: 10 }} class="ls-popover-icon" />;
    }
    return null;
  };

  renderActions = () => {
    if (
      this.placement === 'top' ||
      this.placement === 'topleft' ||
      this.placement === 'topright' ||
      this.placement === 'left' ||
      this.placement === 'lefttop' ||
      this.placement === 'leftbottom'
    ) {
      return (
        <div class="ls-popover-tips" ref={this.tipsRef}>
          <div class="ls-popover-content">
            <slot name="content">
              {this.actions.map((action: PopoverAction, index: number) => {
                const actionClass = classNames('ls-popover-action-container', {
                  'ls-action-container-disable': action.disabled && action.disabled === true,
                });
                return (
                  <div
                    class={actionClass}
                    onClick={(e) => {
                      this.handleActionClick(e, index);
                    }}
                  >
                    {this.renderIcon(action.icon)}
                    <div>{action.text}</div>
                  </div>
                );
              })}
            </slot>
          </div>
          <div class="ls-popover-triangle" />
        </div>
      );
    }
    return (
      <div class="ls-popover-tips" ref={this.tipsRef}>
        <div class="ls-popover-triangle" />
        <div class="ls-popover-content">
          <slot name="content">
            {this.actions.map((action: PopoverAction, index: number) => {
              const actionClass = classNames('ls-popover-action-container', {
                'ls-action-container-disable': action.disabled && action.disabled === true,
              });
              return (
                <div
                  class={actionClass}
                  onClick={(e) => {
                    this.handleActionClick(e, index);
                  }}
                >
                  {this.renderIcon(action.icon)}
                  <div>{action.text}</div>
                </div>
              );
            })}
          </slot>
        </div>
      </div>
    );
  };

  render() {
    return (
      <div class="ls-popover">
        <slot></slot>
        {this.renderActions()}
      </div>
    );
  }
}

export default Popover;
