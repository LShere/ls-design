import { Property, CustomElement, LSElement } from '@ls-design/core';
import style from './style.css';
import '../loading';

export interface Props {
  checked?: boolean;
  disabled?: boolean;
  loading?: boolean;
  size?: number;
  color?: string;
  inactivecolor?: string;
  beforechange?: string;
}

export interface CustomEvent {
  change?: (e: { detail: { value: boolean } }) => void;
}

@CustomElement({ tag: 'ls-switch', style })
class Switch extends LSElement {
  @Property({ type: Boolean })
  disabled = false;

  @Property({ type: Boolean })
  checked = false;

  @Property({ type: Boolean })
  loading = false;

  @Property()
  size = '';

  @Property()
  color = '#08f';

  @Property()
  inactivecolor = '#e1e6eb';

  @Property()
  name = '';

  @Property()
  beforechange = '';

  handleChange = async () => {
    if (this.disabled || this.loading) {
      return;
    }
    // 完全受控组件，内部不需要更改this.check
    const newValue = !this.checked;
    this.$emit('change', {
      detail: {
        value: newValue,
      },
    });
  };

  render() {
    const inlineStyle = {
      fontSize: this.size ? (16 * Number(this.size)) / 30 : 16, // 高度为基准
      '--switch-active-color': this.color,
      '--switch-inactive-color': this.inactivecolor,
    };

    return (
      <div style={inlineStyle} class={this.loading && 'ls-switch-loading'}>
        <input type="checkbox" id="ls-switch" class="ls-switch" onClick={this.handleChange} />
        <label for="ls-switch">
          {this.loading && (
            <div class="ls-switch-loading-wrapper">
              <ls-loading
                size={Number(this.size) ? (16 * Number(this.size)) / 30 : 16}
                color={this.checked ? this.color : this.inactivecolor}
                type="spinner"
              ></ls-loading>
            </div>
          )}
        </label>
      </div>
    );
  }
}

export default Switch;
