import { CustomElement, Property, createRef, State, LSElement } from '@ls-design/core';
import Locale from '../locale';
import style from './style.css';

type autoCompleteType = 'off' | 'on';
export interface Props {
  value?: string;
  placeholder?: string;
  rows?: number;
  maxlength?: number;
  showcount?: boolean;
  autocomplete?: boolean;
  disabled?: boolean;
  readonly?: boolean;
  id?: string;
}
export interface CustomEvent {
  input?: (e: { target: { value: string } }) => void;
  focus?: () => void;
  blur?: () => void;
  change?: (e: { target: { value: string } }) => void;
}
@CustomElement({ tag: 'ls-textarea', style })
class TextArea extends LSElement {
  @Property()
  name = '';

  @Property()
  rows = 2;

  @Property({
    type: Boolean,
  })
  autosize = false;

  @Property()
  placeholder: string = Locale.current.placehold;

  @Property()
  maxlength: number | string = '';

  @Property({
    type: Boolean,
  })
  showcount = false;

  @Property({
    type: Boolean,
  })
  readonly = false;

  @Property({
    type: Boolean,
  })
  disabled = false;

  @Property()
  autocomplete: autoCompleteType = 'off';

  @Property()
  id = '';

  @State
  value = '';

  textAreaRef: any = createRef();

  evenFn = (type: string) => (e: Event) => {
    if (!this.textAreaRef && !this.textAreaRef.current) return;
    e.stopPropagation();
    const { current } = this.textAreaRef;
    this.value = current.value;
    this.$emit(type, { detail: { value: this.value } });
    if (type === 'change' && this.autosize) {
      // current.style.height = "auto";
      current.style.height = `${current.scrollHeight}px`;
    }
  };

  render() {
    return (
      <div class="ls-textarea">
        <textarea
          class="ls-text-area"
          ref={this.textAreaRef}
          value={this.value}
          id={this.id}
          rows={this.rows}
          disabled={this.disabled}
          autocomplete={this.autocomplete}
          maxlength={this.maxlength}
          placeholder={this.placeholder}
          readonly={this.readonly}
          onChange={this.evenFn('change')}
          onInput={this.evenFn('change')}
          onBlur={this.evenFn('blur')}
          onFocus={this.evenFn('focus')}
          onClick={this.evenFn('click')}
        ></textarea>
        {this.showcount && (
          <div class="ls-textarea-text-count">
            {this.value.length || 0}
            {this.maxlength && <span>/{this.maxlength}</span>}
          </div>
        )}
      </div>
    );
  }
}

export default TextArea;
