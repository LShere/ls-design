### CountDown

---


#### 简介


倒计时组件


#### 安装使用


```jsx
import { CountDown } from "@ls-design/ui-react";
```


#### 基础用法

```html
<CountDown time="7200000" />
```


#### 格式化


format 属性控制，间隔符仅支持一位字符串


```html
<CountDown time="360000" format="MM:SS" />
<CountDown time="360000" format="MM-SS" />
<CountDown time="360000" format="MM SS" />
```


### 结束回调


当倒计时为 0 的时候，触发结束事件


```jsx
export default () => {
  const onEnd = () => {
    Toast.text("countdown finished");
  };
  return <CountDown time="5000" onEnd={onEnd} />;
};
```


#### Props


| 参数   | 说明                                          | 类型       | 默认值     |
| ------ | --------------------------------------------- | ---------- | ---------- |
| time   | 标准时间戳                                    | string     |
| format | 支持 HH MM SS、MM:SS 间隔符可自定义一位字符串 | string     | `HH:MM:SS` |
| onEnd  | 倒计时为 0 触发事件                           | () => void |



#### 样式变量


组件提供了以下 CSS变量，可用于自定义样式


| 名称                            | 说明             | 默认值    |
| ------------------------------- | ---------------- | --------- |
| `--countdown-num-min-width`     | 数字内容最小宽度 | `18px`    |
| `--countdown-num-background`    | 数字内容背景色   | `#F4433D` |
| `--countdown-num-padding`       | 数字内容 padding | `0 4px`   |
| `--countdown-num-border-radius` | 数字圆角         | `4px`     |
| `--countdown-dot-color`         | 间隔符颜色       | `#fff`    |
| `--countdown-dot-margin`        | 间隔符间距       | `0 2px`   |
