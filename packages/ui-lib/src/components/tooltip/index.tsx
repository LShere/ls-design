import { Property, CustomElement, createRef, LSElement } from '@ls-design/core';
import '@ls-design/icons/dist/es/close';
import style from './style.css';
export interface Props {
  open: boolean;
  placement?:
    | 'top'
    | 'topleft'
    | 'topright'
    | 'left'
    | 'lefttop'
    | 'leftbottom'
    | 'right'
    | 'righttop'
    | 'rightbottom'
    | 'bottom'
    | 'bottomleft'
    | 'bottomright';
  zindex?: number;
  tips: string;
  closeable?: boolean;
  autoclose?: boolean;
  opentime?: number;
  scrollhidden?: boolean;
}
export interface CustomEvent {
  close: () => void;
}
@CustomElement({ tag: 'ls-tooltip', style })
class Tooltip extends LSElement {
  @Property()
  placement = 'bottom';

  @Property()
  tips = '';

  @Property({
    type: Boolean,
  })
  open = false;

  @Property({
    type: Boolean,
  })
  closeable = false;

  @Property({
    type: Boolean,
  })
  autoclose = false;

  @Property()
  opentime = 3000;

  @Property({
    type: Boolean,
  })
  scrollhidden = false;

  @Property()
  zindex = '999';

  @Property()
  size = '';

  timer?: ReturnType<typeof setTimeout> | null = undefined;

  tipsRef: any = createRef();

  componentDidMount() {
    if (this.zindex) {
      this.style.zIndex = this.zindex;
    }

    if (this.scrollhidden) {
      window.addEventListener('scroll', this.windowScrollListener);
    }

    document.addEventListener('click', (e) => {
      if (!e.composedPath().includes(this) && this.open) {
        // 打开状态，并且外界点击
        this.closeEmit();
      }
    });

    this.addTimer();
  }

  shouldComponentUpdate(propName: string, oldValue: string | boolean, newValue: string | boolean): boolean {
    if (propName === 'open' && this.tipsRef && this.tipsRef.current) {
      const { current } = this.tipsRef;
      // 设置退出过渡动画
      if (newValue) {
        // 由关闭到打开
        current.classList.remove('ls-tooltip-leave');
        this.addTimer();
      } else if (oldValue && !newValue) {
        // 由打开到关闭
        current.classList.add('ls-tooltip-leave');
        this.clearTimer();
      }
    }
    return true;
  }

  componentWillUnmount() {
    window.removeEventListener('scroll', this.windowScrollListener);
    this.clearTimer();
  }

  windowScrollListener = () => {
    if (this.open) {
      this.closeEmit();
    }
  };

  addTimer = () => {
    if (!this.open) {
      return;
    }
    if (this.autoclose) {
      if (this.timer) {
        clearTimeout(this.timer);
      }
      this.timer = setTimeout(() => {
        this.closeEmit();
      }, this.opentime);
    }
  };

  clearTimer = () => {
    if (this.timer) {
      clearTimeout(this.timer);
    }
  };

  addRemoveAnimation = () => {
    if (this.tipsRef && this.tipsRef.current) {
      const { current } = this.tipsRef;
      current.classList.add('ls-tooltip-leave');
    }
  };

  closeEmit = () => {
    this.addRemoveAnimation();
    this.open = false;
    this.$emit('close');
  };

  handleTipsClick = (ev: PointerEvent) => {
    ev.stopPropagation();
    if (!this.closeable) {
      return;
    }
    this.closeEmit();
  };

  renderCloseIcon = () => {
    if (!this.closeable) {
      return null;
    }
    return <ls-icon-close style={{ opacity: 0.7, marginLeft: 9 }} />;
  };

  renderTips = () => {
    if (
      this.placement === 'top' ||
      this.placement === 'topleft' ||
      this.placement === 'topright' ||
      this.placement === 'left' ||
      this.placement === 'lefttop' ||
      this.placement === 'leftbottom'
    ) {
      return (
        <div class="ls-tooltip-tips" ref={this.tipsRef} onClick={this.handleTipsClick}>
          <div class="ls-tooltip-content">
            <div>{this.tips}</div>
            {this.renderCloseIcon()}
          </div>
          <div class="ls-tooltip-triangle" />
        </div>
      );
    }
    return (
      <div class="ls-tooltip-tips" ref={this.tipsRef} onClick={this.handleTipsClick}>
        <div class="ls-tooltip-triangle" />
        <div class="ls-tooltip-content">
          <div>{this.tips}</div>
          {this.renderCloseIcon()}
        </div>
      </div>
    );
  };

  render() {
    return (
      <div class="ls-tooltip">
        <slot></slot>
        {this.renderTips()}
      </div>
    );
  }
}

export default Tooltip;
