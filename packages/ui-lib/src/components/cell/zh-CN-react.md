### Cell

---


#### 简介


单元格


```ts
import { Cell } from '@ls-design/ui-react';
```


#### 基本使用


```tsx

export default () => {
  return (
    <Cell title="这是标题" />
    <Cell title="这是标题" islink />
    <Cell title="这是标题" desc="描述文字" />
    <Cell title="这是标题" desc="描述文字" islink />
  )
}
```


#### 链接跳转


```tsx
import { Cell } from '@quarkd/quark-react';

export default () => {
  return (
    <Cell title="路由跳转" to="#/button" islink />
    <Cell title="链接跳转" to="https://baidu.com" islink />
  )
}
```


#### 图标展示


```tsx
export default () => {
  return <Cell title="这是标题" icon="图标地址" islink />;
};
```


#### 分组用法


```tsx
export default () => {
  return (
    <CellGroup>
      <Cell title="这是标题" islink />
      <Cell title="这是标题" islink />
    </CellGroup>
  );
};
```


#### 自定义样式


设置标题最大宽度，省略号展示


```tsx
export default () => {
  return <Cell title="这是标题很长长长长长长长长长长长长长长长长长长" />;
};
```


```css
/* CSS 省略号展示 */
.my-cell {
  --cell-title-white-space: nowrap;
  --cell-title-text-overflow: ellipsis;
}
```


#### 自定义右侧描述


通过 Slot（`Cell` 包裹的内容）可以自定义右侧描述。


```tsx
export default () => {
  return (
    <Cell title="标题">
      <div>自定义内容</div>
    </Cell>
  );
};
```


#### 图标按钮


通过 `icon` 属性设置图标


```tsx
<Button type="primary" icon="图标地址">
  {' '}
  喜欢{' '}
</Button>
```


#### Props


| 参数   | 解释           | 参数类型  |
| :----- | :------------- | :-------- |
| title  | 标题           | `string`  |
| desc   | 描述文字       | `string`  |
| to     | 跳转链接       | `string`  |
| islink | 是否显示右箭头 | `boolean` |
| icon   | 左侧图标       | `string`  |


#### Slots


| 名称            | 解释            |
| :-------------- | :-------------- |
| slot            | 自定义右侧信息  |
| slot name=title | 自定义标题      |
| slot name=icon  | 自定义左侧 icon |


#### 样式变量


提供了 CSS 变量 可自定义样式


| 名称                       | 解释             | 默认值                            |
| -------------------------- | ---------------- | --------------------------------- |
| `--cell-title-font-size`   | 标题字体大小     | `14px`                            |
| `--cell-title-color`       | 标题字体颜色     | `#666`                            |
| `--cell-title-width`       | 标题字体最大宽度 |
| `--cell-title-font-weight` | 标题字重         |
| `--cell-title-font-family` | 标题字体         | `PingFangSC-Regular, PingFang SC` |
| `--cell-title-white-space` | 标题是否换行     | `nowrap`                          |
| `--cell-desc-font-size`    | 描述字体大小     |
| `--cell-desc-color`        | 描述字体颜色     | `#969799`                         |
| `--cell-desc-width`        | 描述字体最大宽度 | `14px`                            |
| `--cell-desc-white-space`  | 描述是否换行     | `nowrap`                          |
| `--cell-desc-font-weight`  | 描述字重         |
| `--cell-desc-font-family`  | 描述字体         | `PingFangSC-Regular, PingFang SC` |
| `--cell-icon-font-size`    | 图标大小         | `16px`                            |
| `--cell-quark-icon-color`  | 图标颜色         | `#969799`                         |
| `--cell-hspacing`          | cell 左右内边距  | `16px`                            |
| `--cell-vspacing`          | cell 上下内边距  | `13px`                            |
