import { Property, CustomElement, LSElement } from '@ls-design/core';
import '@ls-design/icons/dist/es/arrow-right';
import cellStyle from './cellStyle.css';
import cellGroupCss from './cellGroupStyle.css';

export interface Props {
  title: string;
  desc?: string;
  to?: string;
  islink?: boolean;
  icon?: string;
}

@CustomElement({
  tag: 'ls-cell',
  style: cellStyle,
})
class LSCell extends LSElement {
  @Property()
  title = '';

  @Property()
  icon = '';

  @Property()
  desc = '';

  @Property()
  to = '';

  @Property({
    type: Boolean,
  })
  islink = false;

  handleNavigation = () => {
    if (this.to) {
      window.location.href = this.to;
    }
  };

  renderIcon = () => {
    if (this.icon && this.icon.includes('http')) {
      return <img src={this.icon} class="ls-cell-icon" style={{ marginRight: 4 }} />;
    }
    return null;
  };

  render() {
    return (
      <div class="ls-cell" onClick={this.handleNavigation}>
        <slot name="icon">{this.renderIcon()}</slot>
        <div class="ls-cell-title" id="title">
          <slot name="title">{this.title}</slot>
        </div>
        <slot>
          {this.desc && (
            <div class="ls-cell-desc" id="desc">
              {this.desc}
            </div>
          )}
          {this.islink && <ls-icon-arrow-right id="arrow" />}
        </slot>
      </div>
    );
  }
}

export default LSCell;

@CustomElement({
  tag: 'ls-cell-group',
  style: cellGroupCss,
})
class LSCellGroup extends LSElement {
  render() {
    return <slot></slot>;
  }
}
export { LSCellGroup };
