import { CustomElement, Property, State, LSElement } from '@ls-design/core';
import style from './style.css';
import { isObject } from '../../utils/common';

export type StrokeLinecap = 'round' | 'square' | 'butt';
export type CircleStartPosition = 'left' | 'right' | 'top' | 'bottom';

const ROTATE_ANGLE_MAP: Record<CircleStartPosition, number> = {
  right: 0,
  bottom: 90,
  left: 180,
  top: 270,
};

export interface Props {
  rate?: number;
  strokewidth?: number;
  size?: number;
  color?: string;
  layercolor?: string;
  fill?: string;
  strokelinecap?: StrokeLinecap;
  anticlockwise?: boolean;
  startposition?: CircleStartPosition;
}
let uid = 0;
@CustomElement({
  tag: 'ls-circle',
  style,
})
class LSCircle extends LSElement {
  @Property({ type: Number })
  rate = 0;

  @Property({ type: Number })
  strokewidth = 2;

  @Property({ type: Number })
  size = 100;

  @Property()
  color = '#08f';

  @Property()
  layercolor = '#fff';

  @Property()
  fill = 'none';

  @Property()
  strokelinecap: StrokeLinecap = 'round';

  @Property({ type: Boolean })
  anticlockwise = false;

  @Property()
  startposition: CircleStartPosition = 'top';

  stokeId = '';

  @State
  gradientColor: Record<string, string> | null = null;

  componentDidMount() {
    this.stokeId = `ls-circle-${uid++}`;
  }

  renderLayer() {
    // 计算周长
    const PERIMETER = Math.PI * (Number(this.size) - Number(this.strokewidth));
    const layerStyle = {
      fill: 'none',
      stroke: this.layercolor,
      strokeWidth: this.strokewidth,
      strokeDasharray: PERIMETER,
      strokeDashoffset: 0,
    };

    const coordinates = this.size / 2;
    const r = (this.size - this.strokewidth) / 2;
    return <circle class="circle-wrapper-layer" cx={coordinates} cy={coordinates} r={r} style={layerStyle} />;
  }

  renderHover() {
    const PERIMETER = Math.PI * (Number(this.size) - Number(this.strokewidth));
    const rate = Math.min(100, Math.max(0, Number(this.rate)));
    const color = isObject(this.gradientColor) ? `url(#${this.stokeId})` : this.color;

    const hoverStyle = {
      fill: this.fill,
      stroke: color,
      strokeWidth: this.strokewidth,
      strokeDasharray: PERIMETER,
      strokeDashoffset: PERIMETER - (PERIMETER * (!this.anticlockwise ? rate : -rate)) / 100,
      strokeLinecap: this.strokelinecap,
    };
    const coordinates = this.size / 2;
    const r = (this.size - this.strokewidth) / 2;

    return <circle class="circle-wrapper-hover" cx={coordinates} cy={coordinates} r={r} style={hoverStyle} />;
  }

  renderGradient() {
    const color = this.gradientColor;
    if (!isObject(color)) return;

    const Stops = Object.keys(color)
      .sort((a, b) => parseFloat(a) - parseFloat(b))
      .map((key, index) => <stop key={index} offset={key} stop-color={color[key]} />);

    return (
      <defs>
        <linearGradient id={this.stokeId} x1="100%" y1="0%" x2="0%" y2="0%">
          {Stops}
        </linearGradient>
      </defs>
    );
  }

  setGradient(color: Record<string, string>) {
    this.gradientColor = color;
  }

  render() {
    const getSizeStyle = {
      width: this.size + 'px',
      height: this.size + 'px',
    };
    const svgStyle = {
      width: this.size + 'px',
      height: this.size + 'px',
      transform: `rotate(${ROTATE_ANGLE_MAP[this.startposition]}deg)`,
    };
    return (
      <div class="circle-wrapper" style={getSizeStyle}>
        <svg style={svgStyle}>
          {this.renderGradient()}
          {this.renderLayer()}
          {this.renderHover()}
        </svg>
        <div class="circle-wrapper-content">
          <slot />
        </div>
      </div>
    );
  }
}

export default LSCircle;
