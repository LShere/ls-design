import { Property, State, CustomElement, Fragment, createRef, LSElement } from '@ls-design/core';
import BScroll from '@better-scroll/core';
import Slide from '@better-scroll/slide';
import style from './style.css';

import '../popup';

BScroll.use(Slide);

export interface Props {
  open: boolean;
}
@CustomElement({
  tag: 'ls-image-preview',
  style,
})
class LSImagePreview extends LSElement {
  @Property({
    type: Boolean,
  })
  open = false;

  @State
  slide: any = null;

  @State
  images: string[] = [];

  @State
  index = 0;

  startX = 0;

  startY = 0;

  endX?: number = 0;

  endY?: number = 0;

  isFn = false;

  onClose: null | ((index: number) => void) = null;
  onChange: null | ((index: number) => void) = null;

  wrapRef = createRef() as any;

  init = async () => {
    if (!this.images.length) return;
    if (this.slide) return;
    const index = this.index > this.images.length ? 0 : this.index;
    try {
      await this.initSlide(index);
    } catch (error) {
      console.log(error, 'error');
    }
    this.eventBind();
    if (this.isFn) this.open = true;
  };

  initSlide(index: number) {
    return new Promise((resolve) => {
      setTimeout(() => {
        this.slide = new BScroll(this.wrapRef.current, {
          scrollX: true,
          scrollY: false,
          autoplay: false,
          loop: false,
          momentum: false,
          bounce: false,
          tap: 'tap',
          click: true,
          preventDefaultException: {
            className: /(^|\s)ls-img(\s|$)/,
          },
          slide: {
            loop: true,
            threshold: 100,
            autoplay: false,
            startPageXIndex: index,
          },
        });
        this.slide.on('slideWillChange', (page: any) => {
          if (!page) return;
          this.index = page.pageX;
          if (this.onChange) this.onChange(page.pageX);
          this.$emit('change', page.pageX);
        });
        resolve(true);
      }, 100);
    });
  }

  componentWillUnmount(): void {
    if (this.slide) this.slide.destroy();
    this.slide = null;
  }

  setData = ({
    images,
    startPosition,
    close,
    change,
  }: {
    images: string[];
    startPosition: number;
    close: () => void | null;
    change: (index: number) => void | null;
  }) => {
    this.images = images;
    this.index = startPosition || 0;
    this.onClose = close;
    this.onChange = change;
    if (this.wrapRef && this.wrapRef.current) this.init();
  };

  myClose = () => {
    if (this.onClose) this.onClose(this.index);
    this.open = false;
  };

  eventBind() {
    this.removeEvent();
    this.addEventListener('touchstart', this.handleTouchStart);
    this.addEventListener('touchmove', this.handleTouchMove);
    this.addEventListener('touchend', this.handleTouchEnd);
  }

  removeEvent = () => {
    this.removeEventListener('touchstart', this.handleTouchStart);
    this.removeEventListener('touchmove', this.handleTouchMove);
    this.removeEventListener('touchend', this.handleTouchEnd);
  };

  handleTouchStart = (e: TouchEvent) => {
    this.startX = e.changedTouches[0].clientX;
    this.startY = e.changedTouches[0].clientY;
    this.endX = undefined;
    this.endY = undefined;
  };

  handleTouchMove = (e: TouchEvent) => {
    this.endX = e.changedTouches[0].clientX;
    this.endY = e.changedTouches[0].clientY;
  };

  handleTouchEnd = () => {
    const angle = this.angle({ X: this.startX, Y: this.startY }, { X: this.endX, Y: this.endY });
    if (this.endX === undefined || this.endY === undefined) {
      this.myClose();
      return;
    }
    if (Math.abs(angle) > 30) {
      // 偏移角度大于30%，关闭popup
      this.myClose();
      return;
    }
  };

  angle(start: { X: number; Y: number }, end: { X: number; Y: number }) {
    const X = end.X - start.X;
    const Y = end.Y - start.Y;
    // 返回角度 /Math.atan()返回数字的反正切值
    return (360 * Math.atan(Y / X)) / (2 * Math.PI);
  }
  close() {
    this.open = false;
  }

  render() {
    const showIndex = `${this.index + 1}`;
    return (
      <Fragment>
        <slot
          name="indicator"
          class="ls-imagepreview-indicator"
          style={{ display: this.open ? 'inline-block' : 'none' }}
        >
          <p>
            {showIndex}/{this.images.length}
          </p>
        </slot>

        <ls-popup position="center" open={this.open}>
          <div class="ls-imagepreview-slide">
            <div class="ls-imagepreview-slide-wrapper" ref={this.wrapRef}>
              <div class="ls-imagepreview-slide-content">
                {this.images.map((item, index) => (
                  <div key={index} class="ls-imagepreview-item">
                    <div class="ls-preview-image">
                      <img src={item} class="ls-img" onClick={this.myClose} />
                    </div>
                  </div>
                ))}
              </div>
            </div>
          </div>
        </ls-popup>
      </Fragment>
    );
  }
}

interface IImagePreview {
  images: string[];
  startPosition?: number;
  close?: () => void;
  change?: (index: number) => void;
}
// 函数调用
export default function imagePreview(params: IImagePreview): LSImagePreview {
  const preview = document.createElement('ls-image-preview') as LSImagePreview;
  document.body.appendChild(preview);
  const { images = [], startPosition, close, change } = params;
  preview.isFn = true;
  preview.setData({ images, startPosition, close, change });
  return preview;
}

export { LSImagePreview };
