### ImagePreview

---


#### 简介


图片预览


#### 安装


`imagepreview` 为函数式用法，`ImagePreivew` 为组件式用法


```ts
import { imagepreview, ImagePreview, ImagePreivewRef } from '@ls-design/ui-react';
```


#### 基本使用


图片预览


```tsx
imagepreview({
  images: ["1.png", "2.png", "3.png"],
});
```


#### 指定初始位置


指定图片预览位置默认从 1 开始


```tsx
imagepreview({
  startPosition: 3,
  images: ["1.png", "2.png", "3.png"],
});
```


#### 监听滑动事件


```tsx
imagepreview({
  images: ["1.png", "2.png", "3.png"],
  change: (index) => Toast.text(`当前移动位置${index + 1}`),
});
```


#### 监听关闭事件


```tsx
imagepreview({
  images: ["1.png", "2.png", "3.png"],
  close: (index) => Toast.text(`当前关闭位置${index + 1}`),
});
```


#### 标签式调用


```tsx
export default () => {
  const preview = useRef<ImagePreviewRef>(null);
  const [open, setOpen] = useState(false);
  const baseUrls = [
    "图片1",
    "图片2",
    "图片3",
  ];

  const componentsClick = () => {
    setOpen(true);
    preview.current.setData({
      images: baseUrls,
    });
  };
  return (
    <div>
      <div onClick={componentsClick} />
      <ImagePreview ref={preview} open={open} />
    </div>
  );
};
```


#### Props


| 参数 | 说明           | 类型      | 默认值  |
| ---- | -------------- | --------- | ------- |
| open | 标签式调用属性 | `boolean` | `false` |


#### 方法


| 名称    | 说明                 | 类型                      |
| ------- | -------------------- | ------------------------- |
| setData | 用于设置图片预览数据 | `(data: Options) => void` |


#### 类型定义


```ts
type Options = {
  images: string[] // 要显示的图片数组
  startPosition?: number // 默认显示位置
  change?: (index: number) => void // 图片滑到事件
  close: (index: number) => void // 组件关闭事件
};
```


#### slot


| 名称           | 说明           |
| -------------- | -------------- |
| name=indicator | 自定义分页显示 |