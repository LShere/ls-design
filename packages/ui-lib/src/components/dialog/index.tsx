import { disableBodyScroll, enableBodyScroll, clearAllBodyScrollLocks } from 'body-scroll-lock';
import { Fragment, Property, CustomElement, createRef, LSElement } from '@ls-design/core';
import '../button';
import '@ls-design/icons/dist/es/close';
import style from './style.css';
import Locale from '../locale';

interface DialogParams {
  title?: string;
  content?: string;
  oktext?: string;
  canceltext?: string;
  zindex?: number;
  autoclose?: boolean;
  notitle?: boolean;
  nofooter?: boolean;
  type?: 'modal' | 'confirm';
  hideclose?: boolean;
  maskclosable?: boolean;
  btnvertical?: boolean;
}
export interface Props extends DialogParams {
  open?: boolean;
}

export interface CustomEvent {
  confirm: () => void;
  cancel: () => void;
  close: () => void;
}

@CustomElement({
  tag: 'ls-dialog',
  style,
})
class Dialog extends LSElement {
  @Property()
  zindex = '999';

  @Property()
  title = '';

  @Property({ type: Boolean })
  notitle = false;

  @Property()
  content: string | HTMLElement = '';

  @Property()
  oktext = Locale.current.confirm;

  @Property()
  canceltext = Locale.current.cancel;

  @Property()
  type = 'modal';

  @Property({ type: Boolean })
  open = false;

  @Property({ type: Boolean })
  autoclose = true;

  @Property({ type: Boolean })
  nofooter = false;

  @Property({ type: Boolean })
  hideclose = false;

  @Property({ type: Boolean })
  maskclosable = false;

  @Property({ type: Boolean })
  btnvertical = false;

  btnActive: any = null;

  dRemove = false;

  close: any = null;

  cancel: any = null;

  confirm: any = null;

  hasChangeBodyStyle = false;

  bodyRef: any = createRef();

  componentDidMount(): void {
    if (this.zindex) {
      this.style.zIndex = `${this.zindex}`;
    }
    if (this.shadowRoot) this.shadowRoot.addEventListener('transitionend', this.transitionendChange);
  }

  componentWillUnmount(): void {
    clearAllBodyScrollLocks();
  }

  shouldComponentUpdate(propName: string, oldValue: string, newValue: string) {
    if (propName === 'open' && newValue !== oldValue && this.bodyRef.current) {
      const { current } = this.bodyRef;
      if (!newValue) {
        enableBodyScroll(current);
        current.classList.remove('ls-dialog-enter');
        current.classList.add('ls-dialog-leave');
        this.btnActive = document.activeElement;
      } else {
        disableBodyScroll(current);
        current.classList.remove('ls-dialog-leave');
        current.classList.add('ls-dialog-enter');
      }
    }
    return true;
  }

  componentDidUpdate(propName: string, oldValue: string, newValue: string) {
    if (propName === 'open' && newValue !== oldValue && this.bodyRef.current) {
      const { current } = this.bodyRef;
      if (!newValue) {
        enableBodyScroll(current);
      } else {
        disableBodyScroll(current);
      }
    }
  }

  closeIconClick = () => {
    this.$emit('close');
    if (this.close) this.close();
    this.hide();
  };

  cancelClick = () => {
    this.$emit('cancel');
    if (this.cancel) this.cancel();
    this.hide();
  };

  okClick = () => {
    this.$emit('confirm');
    if (this.confirm) this.confirm();
    this.hide();
  };

  hide() {
    if (this.autoclose) this.open = false;
  }

  // slotChangeEvent = () => {
  //   const node = this.shadowRoot?.querySelector(".ls-dialog-body slot");
  // };

  transitionendChange = () => {
    if (!this.open && this.dRemove) {
      document.body.removeChild(this);
      // 创建自定义对象 onclose
      this.$emit('close');
      if (this.btnActive) this.btnActive.focus();
    }
  };

  // 渲染上下按钮
  renderBtnVertical = () => {
    const footerClass = this.btnvertical ? 'ls-dialog-footer ls-dialog-vertical' : 'ls-dialog-footer';
    return (
      <div class={footerClass}>
        <ls-button type="primary" onClick={this.okClick}>
          {this.oktext}
        </ls-button>

        <ls-button class="ls-dialog-cancel-btn" onClick={this.cancelClick}>
          {this.canceltext}
        </ls-button>
      </div>
    );
  };

  handleClickMask = () => {
    if (this.maskclosable) {
      this.$emit('close');
      this.hide();
    }
  };

  render() {
    const footerClass = this.btnvertical ? 'ls-dialog-footer ls-dialog-vertical' : 'ls-dialog-footer';

    return (
      <Fragment>
        <div class="ls-dialog" ref={this.bodyRef}>
          {!this.hideclose && (
            <slot name="close">
              <ls-icon-close class="ls-dialog-close-btn" size="25" onClick={this.closeIconClick} />
            </slot>
          )}
          <div class="ls-dialog-content">
            {!this.notitle && (
              <slot name="title">
                <p class="ls-dialog-title">{this.title}</p>
              </slot>
            )}
            <slot>
              {this.content && (
                <div
                  class="ls-dialog-body-wrap"
                  style={{
                    marginBottom: this.content ? 20 : 0,
                  }}
                >
                  <div class="ls-dialog-body">{this.content}</div>
                </div>
              )}
            </slot>
            {!this.nofooter && (
              <slot name="footer">
                {this.btnvertical ? (
                  this.renderBtnVertical()
                ) : (
                  <div class={footerClass}>
                    <ls-button
                      class="ls-dialog-cancel-btn"
                      style={{
                        display: this.type === 'confirm' ? 'none' : 'flex',
                      }}
                      onClick={this.cancelClick}
                    >
                      {this.canceltext}
                    </ls-button>
                    <ls-button class="ls-dialog-confirm-btn" type="primary" onClick={this.okClick}>
                      {this.oktext}
                    </ls-button>
                  </div>
                )}
              </slot>
            )}
          </div>
        </div>
        <div class="ls-dialog-mask" onClick={this.handleClickMask} />
      </Fragment>
    );
  }
}

// 函数调用
export default function (params: DialogParams & CustomEvent): Dialog {
  const dialog: any = document.createElement('ls-dialog');
  const {
    title = '',
    content = '',
    oktext = Locale.current.confirm,
    canceltext = Locale.current.cancel,
    confirm,
    cancel,
    close,
    zindex,
    autoclose = true,
    nofooter = false,
    type = '',
    hideclose = false,
    maskclosable = false,
    btnvertical = false,
  } = params;
  dialog.dRemove = true;
  dialog.title = title;
  dialog.innerHTML = content;
  dialog.content = content;
  dialog.oktext = oktext;
  dialog.canceltext = canceltext;
  dialog.cancel = cancel;
  dialog.confirm = confirm;
  dialog.close = close;
  dialog.autoclose = autoclose;
  dialog.nofooter = nofooter;
  dialog.zindex = zindex;
  dialog.type = type;
  dialog.hideclose = hideclose;
  dialog.maskclosable = maskclosable;
  dialog.btnvertical = btnvertical;
  document.body.appendChild(dialog);
  setTimeout(() => {
    dialog.open = true;
  });
  return dialog;
}
export { Dialog };
