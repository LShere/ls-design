### Button
---


#### 简介  


按钮
```ts
import { Button } from '@ls-design/ui-react';
```

#### 基本使用
```tsx
<Button>默认按钮</Button>
```

#### 按钮类型


支持 `priamry` 、`success` 、`danger`、`warning`四种类型


```tsx
<Button>默认按钮</Button>
<Button type="primary">主要按钮</Button>
<Button type="success">成功按钮</Button>
<Button type="danger">危险按钮</Button> <Button type="warning">警告按钮</Button>
```


#### 朴素按钮


`plain` 属性可以设置朴素按钮，朴素按钮的文字为按钮颜色，背景为白色


```tsx
<Button plain type="primary">主要按钮</Button>
<Button plain type="success">成功按钮</Button>
```

#### 浅色按钮


`light` 可以设置浅色按钮，浅色按钮的文字为按钮颜色，背景为对应的浅色


```tsx
<Button light type="primary">主要按钮</Button>
<Button light type="success">成功按钮</Button>
```


#### 按钮尺寸


支持 `big` 、`small` 和默认尺寸


```tsx
<Button type="primary" size="small">小号尺寸</Button>
<Button type="primary" size="big">大号尺寸</Button>
<Button type="primary" size="large">特大尺寸</Button>
<Button type="primary">普通尺寸</Button>
```


#### 禁用状态


通过 `loading` 属性设置加载状态， 其中 `loadingcolor` 属性控制 `loading` 颜色，`loadingsize` 属性控制 loading 大小，`loadingtype` 属性控制 loading 类型，loading 参考 loading 组件


```tsx
<Button loading type="danger" loadtype="circular">加载中...</Button> <Button loading type="warning">加载中...</Button>
<Button onClick="{changeLoading}" loading="{isLoading}" type="success"> Click me! </Button>
```


#### 图标按钮


通过 `icon` 属性设置图标


```tsx
<Button type="primary" icon="图标地址">
  {' '}
  喜欢{' '}
</Button>
```


#### Props


| 参数         | 解释                                             | 参数类型        |
| :----------- | :----------------------------------------------- | :-------------- |
| type         | 类型， `priamry`、`success`、`danger`、`warning` | `string`        |
| size         | 尺寸, `small`、`normal`、`big`、`large`          | `string`        |
| disabled     | 是否禁用                                         | `boolean`       |
| icon         | 图标                                             | `string`        |
| shape        | 形状, `square`                                   | `string`        |
| plain        | 是否为朴素按钮                                   | `boolean`       |
| light        | 是否为浅色按钮                                   | `boolean`       |
| loading      | 是否为loading态                                  | `boolean`       |
| loadtype     | 加载图标类型， `circular`                        | `string`        |
| loadingcolor | 加载颜色                                         | `CSSProperties` |
| loadingsize  | 加载图标大小                                     | `string`        |


#### 样式变量


提供了 CSS 变量 可自定义样式


| 名称                         | 解释           | 默认值 |
| ---------------------------- | -------------- | ------ |
| `--button-height`            | 按钮高度       | `32px` |
| `--button-hspacing`          | 按钮左右内边距 | `12px` |
| `--button-font-size`         | 按钮字体大小   | `14px` |
| `--button-border-radius`     | 按钮圆角       | `8px`  |
| `--button-color`             | 文字颜色       | `#fff` |
| `--button-icon-hspacing`     | icon 左右间距  | `6px`  |
| `--button-big-border-radius` | 大尺寸按钮圆角 | `8px`  |
