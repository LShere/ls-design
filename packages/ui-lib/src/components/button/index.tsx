import { Property, CustomElement, createRef, LSElement } from '@ls-design/core';
import '../loading';
import style from './style.css';
import { pxToVw } from '@/utils/common';

export interface Props {
  type?: 'primary' | 'success' | 'danger' | 'warning';
  size?: 'small' | 'normal' | 'big' | 'large';
  icon?: string;
  shape?: 'round' | 'square';
  plain?: boolean;
  loading?: boolean;
  loadtype?: 'circular' | 'spinner';
  loadingcolor?: string;
  loadingsize?: number;
  disabled?: boolean;
}
@CustomElement({
  tag: 'ls-button',
  style,
})
class LSButton extends LSElement {
  @Property({
    type: Boolean,
  })
  disabled = false;

  @Property()
  size: string;

  @Property()
  type = '';

  @Property()
  icon?: string = undefined;

  @Property()
  shape: string;

  @Property({
    type: Boolean,
  })
  loading = false;

  @Property()
  loadtype: string;

  @Property()
  loadingcolor: string;

  @Property()
  loadingsize: number;

  @Property({
    type: Boolean,
  })
  plain = false;

  @Property({
    type: Boolean,
  })
  light = false;

  slotRef: any = createRef();

  isActive: true;

  renderIcon = () => {
    if (this.icon && this.icon.startsWith('http')) {
      return <img class="ls-button-icon" src={this.icon}></img>;
    }
    if (this.loading) {
      return (
        <ls-loading
          class="ls-button-load"
          color={this.loadingcolor ? this.loadingcolor : '#fff'}
          size={this.loadingsize ? pxToVw(this.loadingsize) : pxToVw(20)}
          type={this.loadtype ? this.loadtype : 'spinner'}
        />
      );
    }
    return null;
  };

  componentDidMount() {
    this.slotRef.current.addEventListener('click', (e: Event) => {
      if (this.disabled || this.loading) {
        e.stopPropagation();
      }
    });
    this.slotRef.current.addEventListener('touchstart', () => {});
  }

  render() {
    return (
      <div ref={this.slotRef} class="ls-button">
        {this.renderIcon()}
        <slot></slot>
      </div>
    );
  }
}

export default LSButton;
