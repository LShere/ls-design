import '@ls-design/icons/dist/es/success-circle';
import '@ls-design/icons/dist/es/error-circle';
import '@ls-design/icons/dist/es/warning-circle';
import { clearAllBodyScrollLocks } from 'body-scroll-lock';

import { Fragment, Property, CustomElement, createRef, LSElement } from '@ls-design/core';
import style from './style.css';
import '../overlay';
import '../loading';

let toast = null;
@CustomElement({ tag: 'ls-toast', style })
class Toast extends LSElement {
  @Property()
  type = '';

  @Property()
  icon = '';

  @Property()
  position: Position = 'middle';

  @Property({ type: Boolean })
  show = false;

  @Property()
  content: any = '';

  dRemove = false;

  timer: any = '';

  iconSize = 40;

  iconColor = '#ffffff';

  zIndex = 9999;

  loadingIconDirection: 'horizontal' | 'vertical' = 'vertical';

  iconRef: any = createRef();

  toastRef: any = createRef();

  loadingtRef: any = createRef();

  static allowMultiple = false;

  componentDidMount(): void {
    const el = this.toastRef.current;
    if (el) {
      el.addEventListener('transitionend', () => {
        this.dRemove = false;
        // 有两次动画开始和借宿，结束的时候在移除节点
        if (!el.classList.contains('ls-toast-leave')) return;
        if (el && el.parentNode && !this.show) {
          try {
            // document.body.removeChild(this);
          } catch (error) {
            // todo something
          }
        }
      });
    }
  }

  // // 删除 toast dom
  // transitionendChange = () => {
  //   if (!this.show && this.dRemove) {
  //     document.body.removeChild(this);
  //   }
  // };

  shouldComponentUpdate(propName: string, oldValue: string | boolean, newValue: string | boolean): boolean {
    if (propName === 'show') {
      const { current: loadingtCurrent } = this.loadingtRef;
      if (this.toastRef && this.toastRef.current) {
        const { current } = this.toastRef;
        // 设置退出过渡动画
        if (newValue) {
          // 由关闭到打开
          current.classList.remove('ls-toast-leave');
        } else {
          if (loadingtCurrent) loadingtCurrent.classList.add('ls-loading-leave');
          current.classList.add('ls-toast-leave');
        }
      }
    }
    return true;
  }

  hide = () => {
    this.show = false;
    if (this.type === 'loading') clearAllBodyScrollLocks();
  };

  renderIcon = () => {
    if (this.type === 'success') {
      return <ls-icon-success-circle color={this.iconColor} size={this.iconSize} ref={this.iconRef} />;
    } else if (this.type === 'failure') {
      return <ls-icon-error-circle color={this.iconColor} size={this.iconSize} ref={this.iconRef} />;
    } else if (this.type === 'warning') {
      return <ls-icon-warning-circle color={this.iconColor} size={this.iconSize} ref={this.iconRef} />;
    }
    return null;
  };

  renderLoading = () => {
    return (
      <ls-overlay open={this.show} zIndex={this.zIndex}>
        <div class={`ls-toast ls-toast--${this.loadingIconDirection}`} ref={this.toastRef}>
          {this.type !== 'text' && this.renderIcon()}
          <ls-loading ref={this.loadingtRef} size={this.iconSize} color="#ffffff" type="circular" vertical></ls-loading>
          <slot>{this.content}</slot>
        </div>
      </ls-overlay>
    );
  };
  renderOther = () => (
    <div class="ls-toast" ref={this.toastRef} style={{ zIndex: this.zIndex }}>
      {this.type !== 'text' && this.renderIcon()}
      <slot>{this.content}</slot>
    </div>
  );
  render() {
    return <Fragment>{this.type === 'loading' ? this.renderLoading() : this.renderOther()}</Fragment>;
  }
}

interface TOptions {
  msg: string;
  duration?: number;
  type: string;
  close?: any;
  show?: boolean;
  size?: number;
  zIndex?: number;
  position?: Position;
}
type Position = 'top' | 'middle' | 'bottom';

type ToastParams = {
  duration?: number;
  close?: () => void;
  size?: number;
  position?: Position;
  zIndex?: number;
};

const defaultOptions = {
  msg: '',
  duration: 2000,
  type: 'text',
  close: null,
};

const mountToast = (opts: TOptions) => {
  const {
    type,
    msg,
    duration = 2000,
    close,
    size = 40,
    zIndex = 9999,
    position = 'middle',
    loadingIconDirection = 'vertical',
  } = { ...defaultOptions, ...opts };
  let mountToast = null;
  if (Toast.allowMultiple) {
    mountToast = document.createElement('ls-toast');
  } else {
    if (!toast) {
      toast = document.createElement('ls-toast');
    }
    mountToast = toast;
  }
  if (mountToast.timer) clearTimeout(mountToast.timer);
  mountToast.type = type;
  mountToast.content = msg;
  mountToast.iconSize = size;
  mountToast.zIndex = zIndex;
  mountToast.position = position;
  mountToast.loadingIconDirection = loadingIconDirection;
  document.body.appendChild(mountToast);
  setTimeout(() => {
    mountToast.show = true;
  });
  mountToast.dRemove = true;

  if (duration !== 0) {
    mountToast.timer = setTimeout(() => {
      // TODO：关闭的回调函数。怎么调用比较合适？
      mountToast.show = false;
      if (close) close();
    }, duration);
  }
  // eslint-disable-next-line consistent-return
  return mountToast;
};

const errorMsg = (msg: string) => {
  if (!msg) {
    console.warn('[Toast]: msg cannot empty');
  }
};
// export { Toast };

export default {
  text: function (msg = '', opts: ToastParams = {}): Toast {
    errorMsg(msg);
    return mountToast({ ...opts, type: 'text', msg });
  },

  success: function (msg = '', opts: ToastParams = {}): Toast {
    errorMsg(msg);
    return mountToast({ ...opts, type: 'success', msg });
  },

  error: function (msg = '', opts: ToastParams = {}): Toast {
    errorMsg(msg);
    return mountToast({ ...opts, type: 'failure', msg });
  },

  warning: function (msg = '', opts: ToastParams = {}): Toast {
    errorMsg(msg);
    return mountToast({ ...opts, type: 'warning', msg });
  },

  loading: function (
    msg = '',
    opts: ToastParams & {
      loadingIconDirection?: 'horizontal' | 'vertical';
    } = {},
  ): Toast {
    errorMsg(msg);
    return mountToast({
      size: opts.loadingIconDirection === 'horizontal' ? 16 : 40,
      ...opts,
      type: 'loading',
      msg,
    });
  },

  hide: function () {
    if (toast) toast.hide();
  },

  allowMultiple: function (): void {
    Toast.allowMultiple = true;
  },
};
