import { disableBodyScroll, enableBodyScroll, clearAllBodyScrollLocks } from 'body-scroll-lock';

import { Property, CustomElement, Fragment, createRef, LSElement } from '@ls-design/core';
import '@ls-design/icons/dist/es/close';
import style from './style.css';

export interface Props {
  position?: 'top' | 'bottom' | 'left' | 'right';
  round?: boolean;
  open: boolean;
  closeable?: boolean;
  forbidmaskclick?: boolean;
  safearea?: boolean;
  zindex?: number;
}
export interface CustomEvent {
  clickoverlay?: () => void;
  close?: () => void;
  closed?: () => void;
  opened?: () => void;
}

@CustomElement({
  tag: 'ls-popup',
  style,
})
class LSPopup extends LSElement {
  @Property({
    type: Boolean,
  })
  open = false;

  @Property({
    type: Boolean,
  })
  forbidmaskclick = false;

  @Property({
    type: Boolean,
  })
  safearea = false;

  @Property({
    type: Boolean,
  })
  round = false;

  @Property({
    type: Boolean,
  })
  closeable = false;

  @Property()
  position = 'bottom';

  @Property()
  zindex?: number | string = undefined;

  wrap: any = createRef();

  componentDidMount() {
    if (this.zindex) {
      this.style.zIndex = `${this.zindex}`;
    }
  }

  shouldComponentUpdate(propName: string, oldValue: string | boolean, newValue: string | boolean): boolean {
    if (propName === 'open' && this.wrap && this.wrap.current) {
      const { current } = this.wrap;
      // 设置退出过渡动画
      if (newValue) {
        // 由关闭到打开
        current.classList.remove('leave');
      } else if (oldValue && !newValue) {
        // 由打开到关闭
        current.classList.add('leave');
      }
    }
    return true;
  }

  componentDidUpdate(propName: string, oldValue: string, newValue: string) {
    if (propName === 'open' && this.wrap && this.wrap.current) {
      const { current } = this.wrap;
      if (newValue) {
        disableBodyScroll(current);
        this.$emit('opened');
      } else {
        enableBodyScroll(current);
        this.$emit('closed');
      }
    }
  }

  componentWillUnmount() {
    clearAllBodyScrollLocks();
  }

  dispatchClose() {
    // 标签调用触发
    this.$emit('close');
  }

  handleCloseBtnClick = () => {
    this.dispatchClose();
  };

  handleClick = () => {
    this.$emit('clickoverlay');
    if (this.forbidmaskclick) {
      return;
    }
    this.dispatchClose();
  };

  render() {
    return (
      <Fragment>
        <div class="ls-popup" ref={this.wrap}>
          {this.closeable && (
            <div class="ls-popup-close-btn" onClick={this.handleCloseBtnClick}>
              <ls-icon-close size="24" />
            </div>
          )}
          <slot></slot>
        </div>
        <div class="ls-popup-mask" onClick={this.handleClick} />
      </Fragment>
    );
  }
}

export default LSPopup;
