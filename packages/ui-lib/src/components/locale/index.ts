import zhCN from './lang/zh-CN';
import enUS from './lang/en-US';
import assign from 'lodash.assign';
export { zhCN, enUS };

type Lange<T> = { [E in keyof T]: T[E] };

export class Local {
  static current: Lange<typeof zhCN> = zhCN;
  static use(newLang) {
    this.current = newLang;
  }
  static add(message) {
    const tempCurrent = assign({}, this.current, message);
    this.current = tempCurrent;
  }
}

export default Local;
