### Image

---


#### 简介


增强版的 img 标签，提供多种图片填充模式，支持图片懒加载、加载中提示、加载失败提示。


#### 安装使用


```jsx
import { Image } from '@ls-design/ui-react';
```


#### 基础用法


基础用法与原生 img 标签一致，可以设置 src、width、height、alt 等原生属性。


```html
<image src="图片链接" />
```


#### 圆形图片


```html
<image src="图片链接" width="100" height="100" round />
```


#### 懒加载


```html
<image src="图片链接" width="100" height="100" lazy />
```


#### Props


| 参数    | 解释                                                                                                     | 类型                 | 默认值  |
| ------- | -------------------------------------------------------------------------------------------------------- | -------------------- | ------- |
| width   | 宽度，默认单位为 px                                                                                      | `number`或`string`   | -       |
| height  | 高度，默认单位为 px                                                                                      | `number` 或 `string` | -       |
| fit     | 图片填充模式，等同于原生的[object-fit](https://developer.mozilla.org/zh-CN/docs/Web/CSS/object-fit) 属性 | `string`             | `fill`  |
| lazy    | 懒加载                                                                                                   | `boolean`            | `false` |
| round   | 是否显示为圆形                                                                                           | `boolean`            | `false` |
| radius  | 外边框圆角                                                                                               | `number` 或 `string` | -       |
| alt     | 定义了图像的备用文本描述                                                                                 | `string`             | -       |
| onLoad  | 图片加载完毕时触发                                                                                       | `() => void`         | -       |
| onError | 图片加载失败时触发                                                                                       | `(error) => void`    | -       |


#### Slots


| 名称    | 解释                       |
| ------- | -------------------------- |
| loading | 自定义加载中的提示内容     |
| error   | 自定义加载失败时的提示内容 |


#### 样式变量


组件提供了以下 CSS变量，可用于自定义样式


| 名称             | 说明     | 默认值 |
| ---------------- | -------- | ------ |
| `--image-height` | 图片高度 | `100%` |
| `--image-width`  | 图片宽度 | `100%` |
