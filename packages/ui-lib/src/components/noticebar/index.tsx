import { Property, createRef, CustomElement, LSElement } from '@ls-design/core';

import '@ls-design/icons/dist/es/arrow-right';
import '@ls-design/icons/dist/es/notify';

import style from './style.css';
export interface Props {
  text?: string;
  multiple?: boolean;
  closehide?: boolean;
  right?: string;
  safearea?: boolean;
  iconsize?: string;
  bgcolor?: string;
  righthide?: boolean;
  lefthide?: boolean;
}
export interface CustomEvent {
  rightclick?: () => void;
}

@CustomElement({
  tag: 'ls-noticebar',
  style,
})
class LSNoticebar extends LSElement {
  @Property()
  text = '';

  @Property()
  bgcolor = '';

  @Property()
  color = '';

  @Property({ type: Boolean })
  lefthide = false;

  @Property({ type: Boolean })
  righthide = false;

  @Property()
  multiple = 1;

  rightSlotRef = createRef();

  handleRightClick = () => {
    this.$emit('rightclick');
  };

  handleRightSlotChange = () => {
    const { current } = this.rightSlotRef;
    if (current) {
      const hasChild = current.assignedNodes().length;
      current.style.paddingRight = hasChild ? '0px' : '11px';
    }
  };

  render() {
    return (
      <div style={{ backgroundColor: this.bgcolor, color: this.color }}>
        <slot name="left" class="ls-noticebar-left">
          {!this.lefthide && <ls-icon-notify size="15" />}
        </slot>
        <slot name="text">
          <span class="ls-noticebar-text" style={{ WebkitLineClamp: this.multiple }}>
            {this.text}
          </span>
        </slot>
        <slot name="right" class="ls-noticebar-right" ref={this.rightSlotRef} onslotchange={this.handleRightSlotChange}>
          {!this.righthide && <ls-icon-arrow-right size="15" onClick={this.handleRightClick} />}
        </slot>
      </div>
    );
  }
}

export default LSNoticebar;
