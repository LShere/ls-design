/**
 * 将像素单位转换成 vw 单位
 *
 * @param value 像素值
 * @param viewportWidth 设计稿宽度，默认为 375
 * @returns 转换后的值
 * @throws {Error} 当输入值包含非法单位时抛出异常
 */
export const pxToVw = (
  value: string | number,
  { viewportWidth = 375 }: { viewportWidth?: number } = {},
): string | number => {
  const pxNum = parseFloat(value.toString());
  if (!Number.isNaN(pxNum)) {
    const vwNum = (pxNum / viewportWidth) * 100;
    const result = vwNum.toFixed(5) + 'vw';
    return result;
  }
  return value;
};

export const checkFalse = (value: any): boolean => {
  const isFalseArray = [null, 'false', false];
  const isFalse = isFalseArray.includes(value);
  return isFalse;
};

export const isNumber = (val: any): boolean => {
  if (val === '' || val === ' ' || val === null || val === undefined) {
    return false;
  }
  if (Number.isNaN(Number(val))) {
    return false;
  }
  return true;
};

export const addUnit = (value?: string | number): string | undefined => {
  if (value !== undefined && value !== null) {
    if (typeof value === 'number' || /^\d+(\.\d+)?$/.test(value)) {
      return `${value}px`;
    }
    return String(value);
  }
  return undefined;
};

export const throttle = (func: Function, delay = 0, atleast = 200) => {
  let timer: ReturnType<typeof setTimeout> | null = null;
  let lastRun = 0;
  return (...args: any) => {
    const now = Number(new Date());
    if (timer) {
      clearTimeout(timer);
    }
    if (now - lastRun > atleast) {
      lastRun = now;
      func.apply(null, args);
    } else {
      timer = setTimeout(() => {
        func.apply(null, args);
      }, delay);
    }
  };
};

export const inBrowser = typeof window !== 'undefined';

export const slotAssignedElements = (nodes: any[]) => {
  if (!nodes || nodes.length <= 0) {
    return [];
  }
  return nodes.filter((node) => node.nodeType === Node.ELEMENT_NODE);
};

export const vwToPx = (value: string) => {
  return (Number(value.replace('vw', '')) * document.documentElement.clientWidth) / 100;
};

export const vhToPx = (value: string) => {
  return (Number(value.replace('vh', '')) * document.documentElement.clientHeight) / 100;
};

export const remToPx = (value: string) => {
  const htmlFontSize = Number(window.getComputedStyle(document.documentElement).fontSize.replace('px', ''));
  return Number(value.replace('rem', '')) * htmlFontSize;
};

export const percentToHeightPx = (value: string) => {
  return (Number(value.replace('%', '')) * document.documentElement.clientHeight) / 100;
};

export const isMap = (val: unknown): val is Map<any, any> => toTypeString(val) === '[object Map]';
export const isSet = (val: unknown): val is Set<any> => toTypeString(val) === '[object Set]';

export const isDate = (val: unknown): val is Date => val instanceof Date;

export const objectToString = Object.prototype.toString;
export const toTypeString = (value: unknown): string => objectToString.call(value);

export const toRawType = (value: unknown): string => {
  // extract "RawType" from strings like "[object RawType]"
  return toTypeString(value).slice(8, -1);
};

export const isFunction = (val: unknown): val is Function => typeof val === 'function';
export const isString = (val: unknown): val is string => typeof val === 'string';
export const isSymbol = (val: unknown): val is symbol => typeof val === 'symbol';
export const isObject = (val: unknown): val is Record<any, any> => val !== null && typeof val === 'object';

export const isPromise = <T = any>(val: unknown): val is Promise<T> => {
  return isObject(val) && isFunction(val.then) && isFunction(val.catch);
};

export const getRect = (el: HTMLElement) => {
  if (el?.getBoundingClientRect) {
    return el.getBoundingClientRect();
  }

  return {
    width: 0,
    height: 0,
  };
};
