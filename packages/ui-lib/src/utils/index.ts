export type ScrollElement = HTMLElement | Window;

export const defaultRoot = window;

function isElement(node: Element) {
  const ELEMENT_NODE_TYPE = 1;
  return node.tagName !== 'HTML' && node.tagName !== 'BODY' && node.nodeType === ELEMENT_NODE_TYPE;
}

/** 向顶层查找不可见的父元素 */
export function getUnuseParent(el: Element, root: ScrollElement | undefined = defaultRoot) {
  let node = el;

  while (node && node !== root && isElement(node)) {
    const { display, height, visibility } = window.getComputedStyle(node);
    if (display === 'none' || height.startsWith('0') || visibility === 'hidden') {
      return node;
    }
    node = node.parentNode as Element;
  }

  return null;
}

export const classNames = (...rest: any): string => {
  const classes = [];

  for (let i = 0; i < rest.length; i++) {
    const val = rest[i];
    if (!val) continue;

    const valType = typeof val;
    if (valType === 'number' || valType === 'string') {
      classes.push(val);
    } else if (Array.isArray(valType)) {
      // 递归
      const innerVal = classNames(...val);
      if (innerVal) {
        classes.push(innerVal);
      }
    } else if (valType === 'object') {
      if (val.toString === Object.prototype.toString) {
        for (const key in val) {
          if (Object.prototype.hasOwnProperty.call(val, key) && val[key]) {
            classes.push(key);
          }
        }
      } else {
        classes.push(val.toString());
      }
    }
  }

  return classes.join(' ');
};

const overflowScrollReg = /scroll|auto/i;

export const getScrollParent = (el: Element, root: ScrollElement | undefined = defaultRoot) => {
  let node = el;
  while (node && node !== root && isElement(root as any)) {
    const { overflowY } = window.getComputedStyle(node);
    if (overflowScrollReg.test(overflowY)) {
      return node;
    }
    node = node.parentNode as Element;
  }
  return root;
};

export const clamp = (num: number, min: number, max: number): number => Math.min(Math.max(num, min), max);

export function debounce(func, delay) {
  let timerId: ReturnType<typeof setTimeout> | null;
  return (...args) => {
    clearTimeout(timerId);
    timerId = setTimeout(() => func(...args), delay);
  };
}
