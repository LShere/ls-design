export interface BaseProps extends React.DetailedHTMLProps<React.HTMLAttributes<HTMLElement>, HTMLElement> {
  name?: string;
}
export type ComponentBaseProps = Omit<BaseProps, 'onChange' | 'onSelect' | 'onFocus' | 'onInput' | 'onBlur'>;
export type ReactifyProps<Props extends Record<string, any>, Event extends Record<string, any>> = Props & {
  [E in keyof Event as E extends string ? `on${Capitalize<E>}` : E]: Event[E];
};
