import reactify from '@ls-design/reactify';
import '@ls-design/ui/dist/es/switch';
import type { FC } from 'react';
import type { Props, CustomEvent } from '@ls-design/ui/dist/es/switch';
import type { ComponentBaseProps, ReactifyProps } from '../../types';

type SwitchProps = ComponentBaseProps & ReactifyProps<Props, CustomEvent>;
type SwitchType = FC<SwitchProps>;

const Switch = reactify('ls-switch') as SwitchType;
export default Switch;
