import type { FC } from 'react';
import reactify from '@ls-design/reactify';
import '@ls-design/ui/dist/es/empty';
import type { Props } from '@ls-design/ui/dist/es/empty';
import type { ComponentBaseProps, ReactifyProps } from '../../types';

type EmptyProps = ComponentBaseProps & ReactifyProps<Props, {}>;
type EmptyType = FC<EmptyProps>;

const Empty = reactify('ls-empty') as EmptyType;
export default Empty;
