import type { FC } from 'react';
import reactify from '@ls-design/reactify';
import '@ls-design/ui/dist/es/checkbox';
import type { Props, CustomEvent } from '@ls-design/ui/dist/es/checkbox';
import type { ComponentBaseProps, ReactifyProps } from '../../types';

type CheckBoxProps = ComponentBaseProps & ReactifyProps<Props, CustomEvent>;
type CheckboxType = FC<CheckBoxProps>;

const Checkbox = reactify('ls-checkbox') as CheckboxType;
export default Checkbox;
