import reactify from '@ls-design/reactify';
import '@ls-design/ui/dist/es/overlay';
import type { FC } from 'react';
import type { Props, CustomEvent } from '@ls-design/ui/dist/es/overlay';
import type { ComponentBaseProps, ReactifyProps } from '../../types';

type OverlayProps = ComponentBaseProps & ReactifyProps<Props, CustomEvent>;
type OverlayType = FC<OverlayProps>;

const Overlay = reactify('ls-overlay') as OverlayType;
export default Overlay;
