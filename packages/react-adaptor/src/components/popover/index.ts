import reactify from '@ls-design/reactify';
import '@ls-design/ui/dist/es/popover';
import type { FC } from 'react';
import type { Props, CustomEvent, PopoverAction } from '@ls-design/ui/dist/es/popover';
import type { ComponentBaseProps, ReactifyProps } from '../../types';

type PopoverProps = ComponentBaseProps & ReactifyProps<Props, CustomEvent>;
interface Ref {
  setActions: (actions: PopoverAction[]) => void;
}
type PopoverType = FC<PopoverProps>;
type PopoverRef = Ref & HTMLElement;
const Popover = reactify('ls-popover') as PopoverType;
export { PopoverRef };
export default Popover;
