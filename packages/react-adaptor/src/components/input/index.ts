import type { FC } from 'react';
import reactify from '@ls-design/reactify';
import '@ls-design/ui/dist/es/input';
import type { Props, CustomEvent, Rule } from '@ls-design/ui/dist/es/input';
import type { ComponentBaseProps, ReactifyProps } from '../../types';

type InputProps = ComponentBaseProps & ReactifyProps<Props, CustomEvent>;

interface Ref {
  setRules: (rule: Rule[]) => void;
}

type InputType = FC<InputProps>;
type InputRef = Ref & HTMLElement;
const Input = reactify('ls-input') as InputType;

export { InputRef };
export default Input;
