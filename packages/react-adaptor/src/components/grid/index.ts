import type { FC } from 'react';
import reactify from '@ls-design/reactify';
import '@ls-design/ui/dist/es/grid';
import type { Props } from '@ls-design/ui/dist/es/grid';
import type { ComponentBaseProps, ReactifyProps } from '../../types';

type GridProps = ComponentBaseProps & ReactifyProps<Props, {}>;
type GridType = FC<GridProps>;

const Grid = reactify('ls-grid') as GridType;
export default Grid;
