import reactify from '@ls-design/reactify';
import type { FC } from 'react';
import '@ls-design/ui/dist/es/dialog';
import type { Props, CustomEvent } from '@ls-design/ui/dist/es/dialog';
import type { ComponentBaseProps, ReactifyProps } from '../../types';

type DialogProps = ComponentBaseProps & ReactifyProps<Props, CustomEvent>;
type DialogType = FC<DialogProps>;

const Dialog = reactify('ls-dialog') as DialogType;
export default Dialog;
