import reactify from '@ls-design/reactify';
import '@ls-design/ui/dist/es/imagepreview';
import type { FC } from 'react';
import type { Props } from '@ls-design/ui/dist/es/imagepreview';
import type { ComponentBaseProps, ReactifyProps } from '../../types';

type ImagePreviewProps = ComponentBaseProps & ReactifyProps<Props, {}>;
interface Options {
  images: string[]; // 要显示的图片数组
  startPosition?: number; // 默认显示位置
  change?: (index: number) => void; // 图片滑到事件
  close: (index: number) => void; // 组件关闭事件
}
interface Ref {
  setData: (data: Options) => void;
}
type ImagePreviewRef = Ref & HTMLElement;
type ImagePreviewType = FC<ImagePreviewProps>;
const ImagePreview = reactify('ls-image-preview') as ImagePreviewType;

export default ImagePreview;
export { ImagePreviewRef };
