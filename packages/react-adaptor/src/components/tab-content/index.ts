import reactify from '@ls-design/reactify';
import '@ls-design/ui/dist/es/tab';
import type { FC } from 'react';
import type { ContentProps } from '@ls-design/ui/dist/es/tab';
import type { ComponentBaseProps, ReactifyProps } from '../../types';

type TabContentProps = ComponentBaseProps & ReactifyProps<ContentProps, {}>;
type TabContentType = FC<TabContentProps>;

const TabContent = reactify('ls-tab-content') as TabContentType;
export default TabContent;
