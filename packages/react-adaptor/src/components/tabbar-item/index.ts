import reactify from '@ls-design/reactify';
import '@ls-design/ui/dist/es/tabbar';
import type { FC } from 'react';
import type { ComponentBaseProps } from '../../types';
import { ReactifyProps } from '../../types';

type TabbarItemProps = ComponentBaseProps;
type TabbarItemType = FC<TabbarItemProps>;

const TabbarItem = reactify('ls-tabbar-item') as TabbarItemType;
export default TabbarItem;
