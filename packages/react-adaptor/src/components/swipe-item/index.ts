import reactify from '@ls-design/reactify';
import '@ls-design/ui/dist/es/swipe';
import type { FC } from 'react';
import type { ComponentBaseProps } from '../../types';

type SwipeItemProps = ComponentBaseProps;
type SwipeItemType = FC<SwipeItemProps>;

const SwipeItem = reactify('ls-swipe-item') as SwipeItemType;
export default SwipeItem;
