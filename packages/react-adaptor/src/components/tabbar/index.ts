import reactify from '@ls-design/reactify';
import '@ls-design/ui/dist/es/tabbar';
import type { FC } from 'react';
import type { Props, CustomEvent } from '@ls-design/ui/dist/es/tabbar';
import type { ComponentBaseProps, ReactifyProps } from '../../types';

type TabbarProps = ComponentBaseProps & ReactifyProps<Props, CustomEvent>;
type TabbarType = FC<TabbarProps>;

const Tabbar = reactify('ls-tabbar') as TabbarType;
export default Tabbar;
