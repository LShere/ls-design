import type { FC } from 'react';
import reactify from '@ls-design/reactify';
import '@ls-design/ui/dist/es/checkbox';
import type { GroupProps, CustomEvent } from '@ls-design/ui/dist/es/checkbox';
import type { ComponentBaseProps, ReactifyProps } from '../../types';

type CheckBoxGroupProps = ComponentBaseProps & ReactifyProps<GroupProps, CustomEvent>;
type CheckboxGroupType = FC<CheckBoxGroupProps>;

const CheckboxGroup = reactify('ls-checkbox-group') as CheckboxGroupType;
export default CheckboxGroup;
