import type { FC } from 'react';
import reactify from '@ls-design/reactify';
import '@ls-design/ui/dist/es/cascade';
import type { Props, CustomEvent, PickerColumn, SelectedColumn } from '@ls-design/ui/dist/es/cascade';
import type { ComponentBaseProps, ReactifyProps } from '../../types';

type CascadePickerProps = ComponentBaseProps & ReactifyProps<Props, CustomEvent>;
interface Ref {
  setColumns: (columns: PickerColumn[]) => void;
  getValues: () => SelectedColumn[];
}
type CascadeRef = Ref & HTMLElement;
type CascadeType = FC<CascadePickerProps>;
const Cascade = reactify('ls-cascade') as CascadeType;
export { CascadeRef };
export default Cascade;
