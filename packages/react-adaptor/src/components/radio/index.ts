import reactify from '@ls-design/reactify';
import '@ls-design/ui/dist/es/radio';
import type { FC } from 'react';
import type { Props } from '@ls-design/ui/dist/es/radio';
import type { ComponentBaseProps, ReactifyProps } from '../../types';

type RadioProps = ComponentBaseProps & ReactifyProps<Props, {}>;
type RadioType = FC<RadioProps>;

const Radio = reactify('ls-radio') as RadioType;
export default Radio;
