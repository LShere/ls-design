import type { FC } from 'react';
import reactify from '@ls-design/reactify';
import '@ls-design/ui/dist/es/badge';
import type { Props } from '@ls-design/ui/dist/es/badge';
import type { ComponentBaseProps, ReactifyProps } from '../../types';

type BadgeProps = ComponentBaseProps & ReactifyProps<Props, {}>;
type BadgeType = FC<BadgeProps>;

const Badge = reactify('ls-badge') as BadgeType;
export default Badge;
