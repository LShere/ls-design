import reactify from '@ls-design/reactify';
import '@ls-design/ui/dist/es/rate';
import type { FC } from 'react';
import type { Props, CustomEvent } from '@ls-design/ui/dist/es/rate';
import type { ComponentBaseProps, ReactifyProps } from '../../types';

type RateProps = ComponentBaseProps & ReactifyProps<Props, CustomEvent>;
type RateType = FC<RateProps>;

const Rate = reactify('ls-rate') as RateType;
export default Rate;
