import type { FC } from 'react';
import reactify from '@ls-design/reactify';
import '@ls-design/ui/dist/es/grid';
import type { ItemProps } from '@ls-design/ui/dist/es/grid';
import type { ComponentBaseProps, ReactifyProps } from '../../types';

type GridItemProps = ComponentBaseProps & ReactifyProps<ItemProps, {}>;
type GridItemType = FC<GridItemProps>;

const GridItem = reactify('ls-grid-item') as GridItemType;
export default GridItem;
