import type { FC } from 'react';
import reactify from '@ls-design/reactify';
import '@ls-design/ui/dist/es/form';
import type { IFormItemProps } from '@ls-design/ui/dist/es/form/type';
import type { ComponentBaseProps, ReactifyProps } from '../../types';

type FormItemProps = ComponentBaseProps & ReactifyProps<IFormItemProps, {}>;
type FormItemType = FC<FormItemProps>;

const FormItem = reactify('ls-form-item') as FormItemType;
export default FormItem;
