import reactify from '@ls-design/reactify';
import '@ls-design/ui/dist/es/sticky';
import type { FC } from 'react';
import type { Props } from '@ls-design/ui/dist/es/sticky';
import type { ComponentBaseProps, ReactifyProps } from '../../types';

type StickyProps = ComponentBaseProps & ReactifyProps<Props, {}>;
type StickyType = FC<StickyProps>;

const Sticky = reactify('ls-sticky') as StickyType;
export default Sticky;
