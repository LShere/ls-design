import reactify from '@ls-design/reactify';
import '@ls-design/ui/dist/es/noticebar';
import type { FC } from 'react';
import type { Props, CustomEvent } from '@ls-design/ui/dist/es/noticebar';
import type { ComponentBaseProps, ReactifyProps } from '../../types';

type NoticebarProps = ComponentBaseProps & ReactifyProps<Props, CustomEvent>;
type NoticebarType = FC<NoticebarProps>;

const Noticebar = reactify('ls-noticebar') as NoticebarType;
export default Noticebar;
