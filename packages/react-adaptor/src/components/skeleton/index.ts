import reactify from '@ls-design/reactify';
import '@ls-design/ui/dist/es/skeleton';
import type { FC } from 'react';
import type { Props } from '@ls-design/ui/dist/es/skeleton';
import type { ComponentBaseProps, ReactifyProps } from '../../types';

type SkeletonProps = ComponentBaseProps & ReactifyProps<Props, {}>;
type SkeletonType = FC<SkeletonProps>;

const Skeleton = reactify('ls-skeleton') as SkeletonType;
export default Skeleton;
