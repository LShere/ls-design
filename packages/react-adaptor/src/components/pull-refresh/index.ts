import reactify from '@ls-design/reactify';
import '@ls-design/ui/dist/es/pullrefresh';
import type { FC } from 'react';
import type { Props, CustomEvent } from '@ls-design/ui/dist/es/pullrefresh';
import type { ComponentBaseProps, ReactifyProps } from '../../types';

type PullRefreshProps = ComponentBaseProps & ReactifyProps<Props, CustomEvent>;
type PullRefreshType = FC<PullRefreshProps>;

const PullRefresh = reactify('ls-pull-refresh') as PullRefreshType;
export default PullRefresh;
