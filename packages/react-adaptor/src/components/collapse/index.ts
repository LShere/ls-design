import reactify from '@ls-design/reactify';
import type { FC } from 'react';
import '@ls-design/ui/dist/es/collapse';
import type { Props } from '@ls-design/ui/dist/es/collapse';
import type { ComponentBaseProps, ReactifyProps } from '../../types';

type CollapseProps = ComponentBaseProps & ReactifyProps<Props, {}>;
type CollapseType = FC<CollapseProps>;

const Collapse = reactify('ls-collapse') as CollapseType;
export default Collapse;
