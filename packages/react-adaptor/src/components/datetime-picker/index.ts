import reactify from '@ls-design/reactify';
import '@ls-design/ui/dist/es/datetimepicker';
import type { FC } from 'react';
import type { Props, CustomEvent, SelectColumn } from '@ls-design/ui/dist/es/datetimepicker';
import type { ComponentBaseProps, ReactifyProps } from '../../types';

type DatetimePickerProps = ComponentBaseProps & ReactifyProps<Props, CustomEvent>;
type DatetimePickerType = FC<DatetimePickerProps>;
interface Ref {
  setValue: (value: Date | string) => void;
  getValues: () => SelectColumn[];
  setFormatter: (type: string, value: string) => string;
  setFilter: (type: string, values: string[]) => string[];
}
type DatetimePickerRef = Ref & HTMLElement;
const DatetimePicker = reactify('ls-datetime-picker') as DatetimePickerType;
export { DatetimePickerRef };
export default DatetimePicker;
