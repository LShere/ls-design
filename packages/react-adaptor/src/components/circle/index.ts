import type { FC } from 'react';
import reactify from '@ls-design/reactify';
// import "quarkd/lib/cell";
import '@ls-design/ui/dist/es/circle';
import type { Props } from '@ls-design/ui/dist/es/circle';
import { StrokeLinecap, CircleStartPosition } from '@ls-design/ui/dist/es/circle';
import type { ComponentBaseProps, ReactifyProps } from '../../types';

type CircleProps = ComponentBaseProps & ReactifyProps<Props, {}>;
type CircleType = FC<CircleProps>;

interface Ref {
  setGradient: (color: Record<string, string>) => void;
}
type CircleRef = Ref & HTMLElement;

const Circle = reactify('ls-circle') as CircleType;

export default Circle;
export { CircleRef, CircleType, StrokeLinecap, CircleStartPosition };
