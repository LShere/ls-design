import reactify from '@ls-design/reactify';
import '@ls-design/ui/dist/es/tag';
import type { FC } from 'react';
import type { Props } from '@ls-design/ui/dist/es/tag';
import type { ComponentBaseProps, ReactifyProps } from '../../types';

type TagProps = ComponentBaseProps & ReactifyProps<Props, {}>;
type TagType = FC<TagProps>;

const Tag = reactify('ls-tag') as TagType;
export default Tag;
