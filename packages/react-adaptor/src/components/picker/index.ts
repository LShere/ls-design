import reactify from '@ls-design/reactify';
import '@ls-design/ui/dist/es/picker';
import type { FC } from 'react';
import type { Props, CustomEvent, PickerColumn, SelectColumn } from '@ls-design/ui/dist/es/picker';
import type { ComponentBaseProps, ReactifyProps } from '../../types';

type PickerProps = ComponentBaseProps & ReactifyProps<Props, CustomEvent>;
type PickerType = FC<PickerProps>;
interface Ref {
  setColumns: (columns: PickerColumn[]) => void;
  getValues: () => SelectColumn[];
}
type PickerRef = Ref & HTMLElement;
const Picker = reactify('ls-picker') as PickerType;
export { PickerRef };
export default Picker;
