import type { FC } from 'react';
import reactify from '@ls-design/reactify';
import '@ls-design/ui/dist/es/countdown';
import type { Props, CustomEvent } from '@ls-design/ui/dist/es/countdown';
import type { ComponentBaseProps, ReactifyProps } from '../../types';

type CountDownProps = ComponentBaseProps & ReactifyProps<Props, CustomEvent>;
type CountDownType = FC<CountDownProps>;

const CountDown = reactify('ls-countdown') as CountDownType;
export default CountDown;
