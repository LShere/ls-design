import reactify from '@ls-design/reactify';
import '@ls-design/ui/dist/es/list';
import type { FC } from 'react';
import type { Props, CustomEvent } from '@ls-design/ui/dist/es/list';
import type { ComponentBaseProps, ReactifyProps } from '../../types';

type ListProps = ComponentBaseProps & ReactifyProps<Props, CustomEvent>;
type ListType = FC<ListProps>;

const List = reactify('ls-list') as ListType;
export default List;
