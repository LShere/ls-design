import reactify from '@ls-design/reactify';
import '@ls-design/ui/dist/es/tab';
import type { FC } from 'react';
import type { Props, CustomEvent } from '@ls-design/ui/dist/es/tab';
import type { ComponentBaseProps, ReactifyProps } from '../../types';

type TabsProps = ComponentBaseProps & ReactifyProps<Props, CustomEvent>;
type TabsType = FC<TabsProps>;

const Tabs = reactify('ls-tabs') as TabsType;
export default Tabs;
