import reactify from '@ls-design/reactify';
import '@ls-design/ui/dist/es/steps';
import type { FC } from 'react';
import type { Props } from '@ls-design/ui/dist/es/steps';
import type { ComponentBaseProps, ReactifyProps } from '../../types';

type StepsProps = ComponentBaseProps & ReactifyProps<Props, {}>;
type StepsType = FC<StepsProps>;

const Steps = reactify('ls-steps') as StepsType;
export default Steps;
