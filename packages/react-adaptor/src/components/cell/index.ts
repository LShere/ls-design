import type { FC } from 'react';
import reactify from '@ls-design/reactify';
import '@ls-design/ui/dist/es/cell';
import type { Props } from '@ls-design/ui/dist/es/cell';
import type { ComponentBaseProps, ReactifyProps } from '../../types';

type ButtonProps = ComponentBaseProps & ReactifyProps<Props, {}>;
type ButtonType = FC<ButtonProps>;

const Button = reactify('ls-cell') as ButtonType;
export default Button;
