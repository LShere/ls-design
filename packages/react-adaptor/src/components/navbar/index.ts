import reactify from '@ls-design/reactify';
import '@ls-design/ui/dist/es/navbar';
import type { FC } from 'react';
import type { Props, CustomEvent } from '@ls-design/ui/dist/es/navbar';
import type { ComponentBaseProps, ReactifyProps } from '../../types';

type NavbarProps = ComponentBaseProps & ReactifyProps<Props, CustomEvent>;
type NavbarType = FC<NavbarProps>;

const Navbar = reactify('ls-navbar') as NavbarType;
export default Navbar;
