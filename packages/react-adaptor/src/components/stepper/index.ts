import reactify from '@ls-design/reactify';
import '@ls-design/ui/dist/es/stepper';
import type { FC } from 'react';
import type { Props, CustomEvent } from '@ls-design/ui/dist/es/stepper';
import type { ComponentBaseProps, ReactifyProps } from '../../types';

type StepperProps = ComponentBaseProps & ReactifyProps<Props, CustomEvent>;
type StepperType = FC<StepperProps>;

const Stepper = reactify('ls-stepper') as StepperType;
export default Stepper;
