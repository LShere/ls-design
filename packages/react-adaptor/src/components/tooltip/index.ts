import reactify from '@ls-design/reactify';
import '@ls-design/ui/dist/es/tooltip';
import type { FC } from 'react';
import type { Props, CustomEvent } from '@ls-design/ui/dist/es/tooltip';
import type { ComponentBaseProps, ReactifyProps } from '../../types';

type TooltipProps = ComponentBaseProps & ReactifyProps<Props, CustomEvent>;
type TooltipType = FC<TooltipProps>;

const Tooltip = reactify('ls-tooltip') as TooltipType;
export default Tooltip;
