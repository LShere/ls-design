import reactify from '@ls-design/reactify';
import '@ls-design/ui/dist/es/swipe';
import type { FC } from 'react';
import type { Props, CustomEvent } from '@ls-design/ui/dist/es/swipe';
import type { ComponentBaseProps, ReactifyProps } from '../../types';

type SwipeProps = ComponentBaseProps & ReactifyProps<Props, CustomEvent>;
type SwipeType = FC<SwipeProps>;

const Swipe = reactify('ls-swipe') as SwipeType;
export default Swipe;
