import type { FC } from 'react';
import reactify from '@ls-design/reactify';
import '@ls-design/ui/dist/es/cell';
import type { ComponentBaseProps } from '../../types';

type CellGroupProps = ComponentBaseProps;
type CellGroupType = FC<CellGroupProps>;

const CellGroup = reactify('ls-cell-group') as CellGroupType;
export default CellGroup;
