import reactify from '@ls-design/reactify';
import '@ls-design/ui/dist/es/textarea';
import type { FC } from 'react';
import type { Props, CustomEvent } from '@ls-design/ui/dist/es/textarea';
import type { ComponentBaseProps, ReactifyProps } from '../../types';

type TextAreaProps = ComponentBaseProps & ReactifyProps<Props, CustomEvent>;
type TextAreaType = FC<TextAreaProps>;

const TextArea = reactify('ls-textarea') as TextAreaType;
export default TextArea;
