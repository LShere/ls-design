import reactify from '@ls-design/reactify';
import '@ls-design/ui/dist/es/image';
import type { FC } from 'react';
import type { Props, CustomEvent } from '@ls-design/ui/dist/es/image';
import type { ComponentBaseProps, ReactifyProps } from '../../types';

type ImageProps = ComponentBaseProps & ReactifyProps<Props, CustomEvent>;
type ImageType = FC<ImageProps>;

const Image = reactify('ls-image') as ImageType;
export default Image;
