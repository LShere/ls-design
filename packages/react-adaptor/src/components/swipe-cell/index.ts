import reactify from '@ls-design/reactify';
import '@ls-design/ui/dist/es/swipecell';
import type { FC } from 'react';
import type {
  Props,
  SwipeCellPosition,
  BeforeCloseFunc as SwipeCellBeforeClose,
} from '@ls-design/ui/dist/es/swipecell';
import { SwipeCellSide } from '@ls-design/ui/dist/es/swipecell';
import type { ComponentBaseProps, ReactifyProps } from '../../types';

type SwipeCellProps = ComponentBaseProps & ReactifyProps<Props, {}>;

type SwipeCellType = FC<SwipeCellProps>;

interface Ref {
  open: (side: SwipeCellPosition) => void;
  close: (position: SwipeCellPosition) => void;
  setBeforeClose: (fn: SwipeCellBeforeClose) => void;
}

type SwipeCellRef = Ref & HTMLElement;

const SwipeCell = reactify('ls-swipe-cell') as SwipeCellType;
export default SwipeCell;
export { SwipeCellRef };
