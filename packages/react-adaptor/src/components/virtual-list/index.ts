import reactify from '@ls-design/reactify';
import '@ls-design/ui/dist/es/virtuallist';
import type { FC } from 'react';
import type { Props } from '@ls-design/ui/dist/es/virtuallist';
import type { ComponentBaseProps, ReactifyProps } from '../../types';

type VirtualListProps = ComponentBaseProps & ReactifyProps<Props, {}>;
type VirtualListType = FC<VirtualListProps>;

const VirtualList = reactify('ls-virtuallist') as VirtualListType;
export default VirtualList;
