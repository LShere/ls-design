import reactify from '@ls-design/reactify';
import '@ls-design/ui/dist/es/marquee';
import type { FC } from 'react';
import type { Props } from '@ls-design/ui/dist/es/marquee';
import type { ComponentBaseProps, ReactifyProps } from '../../types';

type MarqueeProps = ComponentBaseProps & ReactifyProps<Props, {}>;
type MarqueeType = FC<MarqueeProps>;

const Marquee = reactify('ls-marquee') as MarqueeType;
export default Marquee;
