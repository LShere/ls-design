import reactify from '@ls-design/reactify';
import '@ls-design/ui/dist/es/radio';
import type { FC } from 'react';
import type { GroupProps, GroupCustomEvent } from '@ls-design/ui/dist/es/radio';
import type { ComponentBaseProps, ReactifyProps } from '../../types';

type RadioGroupProps = ComponentBaseProps & ReactifyProps<GroupProps, GroupCustomEvent>;
type RadioGroupType = FC<RadioGroupProps>;

const RadioGroup = reactify('ls-radio-group') as RadioGroupType;
export default RadioGroup;
