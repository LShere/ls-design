import reactify from '@ls-design/reactify';
import '@ls-design/ui/dist/es/loading';
import type { FC } from 'react';
import type { Props } from '@ls-design/ui/dist/es/loading';
import type { ComponentBaseProps, ReactifyProps } from '../../types';

type LoadingProps = ComponentBaseProps & ReactifyProps<Props, {}>;
type LoadingType = FC<LoadingProps>;

const Loading = reactify('ls-loading') as LoadingType;
export default Loading;
