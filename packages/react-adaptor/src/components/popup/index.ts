import reactify from '@ls-design/reactify';
import '@ls-design/ui/dist/es/popup';
import type { FC } from 'react';
import type { Props, CustomEvent } from '@ls-design/ui/dist/es/popup';
import type { ComponentBaseProps, ReactifyProps } from '../../types';

type PopupProps = ComponentBaseProps & ReactifyProps<Props, CustomEvent>;
type PopupType = FC<PopupProps>;

const Popup = reactify('ls-popup') as PopupType;
export default Popup;
