import reactify from '@ls-design/reactify';
import '@ls-design/ui/dist/es/upload';
import type { FC } from 'react';
import type { Props, CustomEvent } from '@ls-design/ui/dist/es/upload';
import type { ComponentBaseProps, ReactifyProps } from '../../types';

type UploadProps = ComponentBaseProps & ReactifyProps<Props, CustomEvent>;
interface Ref {
  setBeforeUpload: (fn: () => boolean) => void;
  setPreview: (url: string[]) => void;
}
type UploadRef = Ref & HTMLElement;
type UploadType = FC<UploadProps>;
const Upload = reactify('ls-upload') as UploadType;
export { UploadRef };
export default Upload;
