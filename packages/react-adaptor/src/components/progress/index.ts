import reactify from '@ls-design/reactify';
import '@ls-design/ui/dist/es/progress';
import type { FC } from 'react';
import type { Props } from '@ls-design/ui/dist/es/progress';
import type { ComponentBaseProps, ReactifyProps } from '../../types';

type ProgressProps = ComponentBaseProps & ReactifyProps<Props, {}>;
type ProgressType = FC<ProgressProps>;

const Progress = reactify('ls-progress') as ProgressType;
export default Progress;
