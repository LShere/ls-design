import reactify from '@ls-design/reactify';
import '@ls-design/ui/dist/es/steps';
import type { FC } from 'react';
import type { ItemProps } from '@ls-design/ui/dist/es/steps';
import type { ComponentBaseProps, ReactifyProps } from '../../types';

type StepItemProps = ComponentBaseProps & ReactifyProps<ItemProps, {}>;
type StepItemType = FC<StepItemProps>;

const StepItem = reactify('ls-step') as StepItemType;
export default StepItem;
