import type { FC } from 'react';
import reactify from '@ls-design/reactify';
import '@ls-design/ui/dist/es/form';
import type { IFormProps } from '@ls-design/ui/dist/es/form/type';
import { Rules } from '@ls-design/ui/dist/es/form/type';
import type { ComponentBaseProps, ReactifyProps } from '../../types';

type FormProps = ComponentBaseProps & ReactifyProps<IFormProps, {}>;
type FormType = FC<FormProps>;

interface Ref {
  validate: (callback: any) => Promise<boolean> | void;
  validateField: (props: string | string[], callback: any) => void;
  clearValidate: (props?: string[] | string) => void;
  resetFields: () => void;
  setRules: (rules: Rules) => void;
  setModel: (model: Record<string, any>) => void;
  getValues: () => Record<string, any>;
}

type FormRef = Ref & HTMLElement;
const Form = reactify('ls-form') as FormType;
export { FormRef, FormProps, FormType, Rules };
export default Form;
