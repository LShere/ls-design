import { defineConfig } from 'vite';
import react from '@vitejs/plugin-react';
import cssVariable from '@ls-design/rollup-plugin-css-variable';
import variableMap from './global-css.js';
import legacy from '@vitejs/plugin-legacy';
import history from 'connect-history-api-fallback';

import path from 'path';

console.log('__dirname', __dirname);

const historyFallbackPlugin = () => {
  return {
    name: 'vite-plugin-history',
    configureServer(server) {
      server.middlewares.use(
        history({
          rewrites: [{ from: /\/mobile/, to: '/mobile.html' }],
          htmlAcceptHeaders: ['text/html'],
        }),
      );
    },
  };
};

const plugins = [
  cssVariable({
    variableMap,
    prefix: 'ls-',
  }),
  legacy({ targets: ['defaults', 'not IE 11'] }),
  react({
    jsxRuntime: 'classic',
    babel: {
      plugins: [
        ['@babel/plugin-proposal-decorators', { legacy: true }],
        ['@babel/plugin-proposal-class-properties', { loose: true }],
      ],
    },
  }),
  historyFallbackPlugin(),
];

// https://vitejs.dev/config/
export default defineConfig({
  plugins,
  css: {
    preprocessorOptions: {
      less: {
        javascriptEnabled: true,
      },
    },
  },
  resolve: {
    extensions: ['.js', '.ts', '.tsx', '.json'],
    alias: [
      {
        find: '@',
        replacement: path.resolve(__dirname, 'src'),
      },
    ],
  },
  build: {
    outDir: 'dist',
    emptyOutDir: true,
    cssMinify: true,
    minify: true,
    rollupOptions: {
      // 配合rollupOptions的input配置多页面入口
      input: {
        mobile: path.resolve(__dirname, 'mobile.html'),
        pc: path.resolve(__dirname, 'index.html'),
      },
    },
  },
});
