import React from 'react';
import { Empty } from '@ls-design/ui-react';

type DemoItemProps = {
  title: string;
  children: React.ReactNode;
};

const DemoItemWrapper = ({ title, children }: DemoItemProps) => {
  return (
    <div style={{ display: 'flex', flexDirection: 'column' }}>
      <h3>{title}</h3>
      <div style={{ display: 'flex', flexDirection: 'column' }}>{children}</div>
    </div>
  );
};
export default function Demo() {
  return (
    <div
      style={{
        padding: 0,
        display: 'flex',
        flexDirection: 'column',
        rowGap: 20,
        minHeight: 'calc(100vh - 48px)',
      }}
    >
      <DemoItemWrapper title="基础用法">
        <Empty />
      </DemoItemWrapper>
      <DemoItemWrapper title="描述文字">
        <Empty title="描述文字" desc="快去添加数据吧~" />
      </DemoItemWrapper>
      <DemoItemWrapper title="底部按钮">
        <Empty title="暂无数据" desc="快去添加数据吧~" buttontext="快去下单吧" />
      </DemoItemWrapper>
      <DemoItemWrapper title="自定义图片">
        <Empty
          desc="描述文字"
          imagesize="100"
          image="https://m.hellobike.com/resource/helloyun/13459/fkntv_custom-empty-image.png"
        />
      </DemoItemWrapper>
    </div>
  );
}
