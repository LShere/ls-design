import React, { useState, useEffect, useRef } from 'react';
import type { PopoverRef } from '@ls-design/ui-react';
import { Popover } from '@ls-design/ui-react';

type DemoItemProps = {
  title: string;
  children: React.ReactNode;
};

const DemoItemWrapper = ({ title, children }: DemoItemProps) => {
  return (
    <div style={{ display: 'flex', flexDirection: 'column' }}>
      <h3>{title}</h3>
      <div style={{ display: 'flex', flexDirection: 'column' }}>{children}</div>
    </div>
  );
};
export default function Demo() {
  const [open, setOpen] = useState(false);
  const openRef = useRef<PopoverRef>(null);
  const actions = [{ text: '选项一' }, { text: '选项二' }, { text: '选项三' }];

  const handleClick = () => setOpen(true);
  const handleClose = () => setOpen(false);

  useEffect(() => {
    const { current: lightCurrent } = openRef;
    lightCurrent!.setActions(actions);
  }, []);

  return (
    <div className="demo">
      <Popover
        ref={openRef}
        open={open}
        onClose={handleClose}
        onSelect={({ detail }) => {
          const { action } = detail;
          console.log(action.text);
          handleClose();
        }}
      >
        <div className="quark-popover" onClick={handleClick}>
          基本使用
        </div>
      </Popover>
    </div>
  );
}
