import React from 'react';
import { Rate } from '@ls-design/ui-react';

type DemoItemProps = {
  title: string;
  children: React.ReactNode;
};

const DemoItemWrapper = ({ title, children }: DemoItemProps) => {
  return (
    <div style={{ display: 'flex', flexDirection: 'column' }}>
      <h3>{title}</h3>
      <div style={{ display: 'flex', flexDirection: 'column' }}>{children}</div>
    </div>
  );
};
export default function Demo() {
  return (
    <div
      style={{
        padding: 0,
        display: 'flex',
        flexDirection: 'column',
        rowGap: 20,
        minHeight: 'calc(100vh - 48px)',
      }}
    >
      <DemoItemWrapper title="基本用法">
        <Rate value="1"></Rate>
      </DemoItemWrapper>
      <DemoItemWrapper title="自定义颜色">
        <Rate value="2" active-color="green"></Rate>
      </DemoItemWrapper>
      <DemoItemWrapper title="禁用状态">
        <Rate value="2" disabled></Rate>
      </DemoItemWrapper>
      <DemoItemWrapper title="只读状态">
        <Rate value="1" readonly></Rate>
      </DemoItemWrapper>
    </div>
  );
}
