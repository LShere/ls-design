import { Tabbar, TabbarItem } from '@ls-design/ui-react';
import React from 'react';

export default function Demo() {
  return (
    <Tabbar>
      <TabbarItem>
        <div>首页</div>
      </TabbarItem>
      <TabbarItem>
        <div>我的</div>
      </TabbarItem>
    </Tabbar>
  );
}
