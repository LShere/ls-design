import { Cell } from '@ls-design/ui-react';
import React from 'react';

export default function CellDemo() {
  return (
    <>
      <Cell title="这是标题" />
      <Cell title="这是标题" islink />
      <Cell title="这是标题" desc="描述文字" />
      <Cell title="这是标题" desc="描述文字" islink />
    </>
  );
}
