import React from 'react';
import { SwipeCell, Cell, Button, Empty } from '@ls-design/ui-react';

type DemoItemProps = {
  title: string;
  children: React.ReactNode;
};

const DemoItemWrapper = ({ title, children }: DemoItemProps) => {
  return (
    <div style={{ display: 'flex', flexDirection: 'column' }}>
      <h3>{title}</h3>
      <div style={{ display: 'flex', flexDirection: 'column' }}>{children}</div>
    </div>
  );
};
export default function Demo() {
  return (
    <div
      style={{
        padding: 0,
        display: 'flex',
        flexDirection: 'column',
        rowGap: 20,
        minHeight: 'calc(100vh - 48px)',
      }}
    >
      <DemoItemWrapper title="基本用法">
        <SwipeCell>
          <Cell title="这是标题" desc="描述文字" />
          <div slot="left">
            <Button type="primary" shape="square">
              选择
            </Button>
          </div>
          <div slot="right">
            <Button type="danger" shape="square">
              删除
            </Button>
            <Button type="primary" shape="square">
              收藏
            </Button>
          </div>
        </SwipeCell>
      </DemoItemWrapper>
      <DemoItemWrapper title="自定义内容">
        <SwipeCell>
          <Empty title="暂无数据" desc="快去添加数据吧~" type="local" />
          <div slot="right">
            <Button type="primary" shape="square">
              添加
            </Button>
          </div>
        </SwipeCell>
      </DemoItemWrapper>
      <DemoItemWrapper title="自定义内容">
        <SwipeCell>
          <Empty title="暂无数据" desc="快去添加数据吧~" type="local" />
          <div slot="right">
            <Button type="primary" shape="square">
              添加
            </Button>
          </div>
        </SwipeCell>
      </DemoItemWrapper>
    </div>
  );
}
