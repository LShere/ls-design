import React, { useRef, useEffect } from 'react';
import type { UploadRef } from '@ls-design/ui-react';
import { Upload } from '@ls-design/ui-react';

type DemoItemProps = {
  title: string;
  children: React.ReactNode;
};

const DemoItemWrapper = ({ title, children }: DemoItemProps) => {
  return (
    <div style={{ display: 'flex', flexDirection: 'column' }}>
      <h3>{title}</h3>
      <div style={{ display: 'flex', flexDirection: 'column' }}>{children}</div>
    </div>
  );
};
export default function Demo() {
  const before = useRef<UploadRef>(null);
  const oversize = useRef<UploadRef>(null);

  const afterRead = (file) => {
    console.log(file.file.name);
  };

  const beforeUpload = (files) => {
    const r = files.every((file) => file.type === 'image/jpg');
    if (!r) {
      console.log('请上传 jpg 格式图片');
      return false;
    }
    return true;
  };

  useEffect(() => {
    before.current.beforeUpload = beforeUpload;
  }, []);
  return (
    <div
      style={{
        padding: 0,
        display: 'flex',
        flexDirection: 'column',
        rowGap: 20,
        minHeight: 'calc(100vh - 48px)',
      }}
    >
      <DemoItemWrapper title="基本用法">
        <Upload onAfterread={afterRead} />
      </DemoItemWrapper>
      <DemoItemWrapper title="限制上传数量">
        <Upload maxcount={2} preview />
      </DemoItemWrapper>
      <DemoItemWrapper title="前置钩子">
        <Upload preview ref={before} />
      </DemoItemWrapper>
      <DemoItemWrapper title="禁止上传">
        <Upload disabled></Upload>
      </DemoItemWrapper>
      <DemoItemWrapper title="只读预览模式">
        <Upload preview readonly />
      </DemoItemWrapper>
    </div>
  );
}
