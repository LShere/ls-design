import React, { useState } from 'react';
import { Popup } from '@ls-design/ui-react';

type DemoItemProps = {
  title: string;
  children: React.ReactNode;
};

const DemoItemWrapper = ({ title, children }: DemoItemProps) => {
  return (
    <div style={{ display: 'flex', flexDirection: 'column' }}>
      <h3>{title}</h3>
      <div style={{ display: 'flex', flexDirection: 'column' }}>{children}</div>
    </div>
  );
};
export default function Demo() {
  const [open, setOpen] = useState(false);

  const handleClose = () => setOpen(false);
  const handleOpen = () => setOpen(true);

  return (
    <div>
      <div onClick={handleOpen}>基本使用</div>
      <Popup open={open} onClose={handleClose}>
        <div>第二行</div>
        <div>第三行</div>
        <div>第四行</div>
        <div>第五行</div>
        <div>第六行</div>
      </Popup>
    </div>
  );
}
