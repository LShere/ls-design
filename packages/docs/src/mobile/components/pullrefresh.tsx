import React, { useState } from 'react';
import { PullRefresh } from '@ls-design/ui-react';

type DemoItemProps = {
  title: string;
  children: React.ReactNode;
};

const DemoItemWrapper = ({ title, children }: DemoItemProps) => {
  return (
    <div style={{ display: 'flex', flexDirection: 'column' }}>
      <h3>{title}</h3>
      <div style={{ display: 'flex', flexDirection: 'column' }}>{children}</div>
    </div>
  );
};
export default function Demo() {
  const [loading, setloading] = useState(false);
  const [count, setCount] = useState(0);

  const onload = () => {
    setloading(true);
    setTimeout(() => {
      console.log('刷新成功');

      setloading(false);
      setCount((count) => count + 1);
    }, 1000);
  };

  return (
    <div style={{ overflowY: 'auto', height: 200 }}>
      <PullRefresh dark loading={loading} onRefresh={onload}>
        <div slot="content" className="pull-content">
          刷新次数: {count}
        </div>
      </PullRefresh>
    </div>
  );
}
