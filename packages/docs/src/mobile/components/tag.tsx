import React from 'react';
import { Tag } from '@ls-design/ui-react';

type DemoItemProps = {
  title: string;
  children: React.ReactNode;
};

const DemoItemWrapper = ({ title, children }: DemoItemProps) => {
  return (
    <div style={{ display: 'flex', flexDirection: 'column' }}>
      <h3>{title}</h3>
      <div style={{ display: 'flex', flexDirection: 'column' }}>{children}</div>
    </div>
  );
};
export default function Demo() {
  return (
    <div
      style={{
        padding: 0,
        display: 'flex',
        flexDirection: 'column',
        rowGap: 20,
        minHeight: 'calc(100vh - 48px)',
      }}
    >
      <DemoItemWrapper title="基本用法">
        <Tag>正</Tag>
        <Tag type="primary">正</Tag>
        <Tag>正</Tag> <Tag size="large">正</Tag>
        <Tag plain>正</Tag>
        <Tag light>正</Tag>
        <Tag size="large" color="#ffe1e1" textcolor="#ad0000">
          正
        </Tag>
        <Tag size="large" color="linear-gradient(135deg, #667eea, #764ba2)">
          正
        </Tag>
        <Tag className="custom-border-color" plain size="large" color="#ffe1e1" textcolor="#ad0000">
          正
        </Tag>
      </DemoItemWrapper>
    </div>
  );
}
