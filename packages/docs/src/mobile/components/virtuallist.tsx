import React, { useState, useRef, useEffect } from 'react';
import { VirtualList } from '@ls-design/ui-react';

type DemoItemProps = {
  title: string;
  children: React.ReactNode;
};

const DemoItemWrapper = ({ title, children }: DemoItemProps) => {
  return (
    <div style={{ display: 'flex', flexDirection: 'column' }}>
      <h3>{title}</h3>
      <div style={{ display: 'flex', flexDirection: 'column' }}>{children}</div>
    </div>
  );
};
export default function Demo() {
  const [list, setList] = useState(new Array(100).fill(0).map((_, i) => i + 1));
  const virtualListRef = useRef(null);
  useEffect(() => {
    virtualListRef.current!.setListData(list);
    virtualListRef.current!.setRenderItem(renderItem);
  }, []);

  const onLoad = () => {
    const len = list.length;
    const arr = new Array(100).fill(0).map((_, i) => len + i + 1);
    const newList = [...list, ...arr];
    setList(newList);
    virtualListRef.current!.setListData(newList);
  };

  const renderItem = (text) => {
    const div = `
      <div style="height: 50px; line-height: 50px; text-align: center;">${text}</div>
    `;
    return div;
  };

  return (
    <div style={{ width: 100 }}>
      <VirtualList ref={virtualListRef} itemheight={50} containerheight={500} onLoad={onLoad} />
    </div>
  );
}
