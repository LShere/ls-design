import React from 'react';
import { ShareSheet } from '@ls-design/ui-react';

type DemoItemProps = {
  title: string;
  children: React.ReactNode;
};

const DemoItemWrapper = ({ title, children }: DemoItemProps) => {
  return (
    <div style={{ display: 'flex', flexDirection: 'column' }}>
      <h3>{title}</h3>
      <div style={{ display: 'flex', flexDirection: 'column' }}>{children}</div>
    </div>
  );
};
export default function Demo() {
  const showBase = () => {
    ShareSheet({
      options: [
        {
          name: '微信',
          icon: 'https://m.hellobike.com/resource/helloyun/16682/LY3mn00VTX.png',
        },
        {
          name: '微信朋友圈',
          icon: 'https://m.hellobike.com/resource/helloyun/16682/QOiMPs9BLj.png',
        },
        {
          name: 'QQ',
          icon: 'https://m.hellobike.com/resource/helloyun/16682/J4TWX9Jpca.png',
        },
        {
          name: 'QQ空间',
          icon: 'https://m.hellobike.com/resource/helloyun/16682/wG7wG2CHQx.png',
        },
        {
          name: '微博',
          icon: 'https://m.hellobike.com/resource/helloyun/16682/vt_vyR3M8I.png',
        },
        {
          name: '二维码',
          icon: 'https://m.hellobike.com/resource/helloyun/16682/hvu4xjJpNY.png',
        },
      ],
      select: (index, action) => {},
      cancel: () => {},
      close: () => {},
    });
  };

  return (
    <div>
      <div onClick={showBase} title="基本使用"></div>
    </div>
  );
}
