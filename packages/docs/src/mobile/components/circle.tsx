import React from 'react';
import { Circle } from '@ls-design/ui-react';

type DemoItemProps = {
  title: string;
  children: React.ReactNode;
};

const DemoItemWrapper = ({ title, children }: DemoItemProps) => {
  return (
    <div style={{ display: 'flex', flexDirection: 'column' }}>
      <h3>{title}</h3>
      <div style={{ display: 'flex', flexDirection: 'column' }}>{children}</div>
    </div>
  );
};
export default function Demo() {
  return (
    <div
      style={{
        padding: 0,
        display: 'flex',
        flexDirection: 'column',
        rowGap: 20,
        minHeight: 'calc(100vh - 48px)',
      }}
    >
      <DemoItemWrapper title="基础用法">
        <Circle rate={10}>{10}%</Circle>;
      </DemoItemWrapper>
      <DemoItemWrapper title="大小定制">
        <Circle rate={10} size="80">
          大小定制
        </Circle>
      </DemoItemWrapper>
      <DemoItemWrapper title="颜色定制">
        <Circle rate={10} color="#02b357">
          颜色定制
        </Circle>
      </DemoItemWrapper>
      <DemoItemWrapper title="宽度定制">
        <Circle rate={10} strokewidth={5}>
          宽度定制
        </Circle>
      </DemoItemWrapper>
      <DemoItemWrapper title="轨道颜色">
        <Circle rate={10} layercolor="#f00">
          轨道颜色
        </Circle>
      </DemoItemWrapper>
      <DemoItemWrapper title="逆时针">
        <Circle rate={10} anticlockwise>
          轨道颜色
        </Circle>
      </DemoItemWrapper>
      <DemoItemWrapper title="起始位置">
        <Circle rate={10} startposition="right">
          右侧
        </Circle>
        <Circle rate={10} startposition="bottom">
          下侧
        </Circle>
        <Circle rate={10} startposition="left">
          左侧
        </Circle>
      </DemoItemWrapper>
    </div>
  );
}
