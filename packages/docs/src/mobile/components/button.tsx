import React from 'react';
import { Button } from '@ls-design/ui-react';

export default function ButtonDemo() {
  return <Button style={{ background: '#fff' }}>普通按钮</Button>;
}
