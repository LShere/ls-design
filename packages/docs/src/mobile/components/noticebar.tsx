import React from 'react';
import { Noticebar } from '@ls-design/ui-react';

type DemoItemProps = {
  title: string;
  children: React.ReactNode;
};

const DemoItemWrapper = ({ title, children }: DemoItemProps) => {
  return (
    <div style={{ display: 'flex', flexDirection: 'column' }}>
      <h3>{title}</h3>
      <div style={{ display: 'flex', flexDirection: 'column' }}>{children}</div>
    </div>
  );
};
export default function Demo() {
  return (
    <div
      style={{
        padding: 0,
        display: 'flex',
        flexDirection: 'column',
        rowGap: 20,
        minHeight: 'calc(100vh - 48px)',
      }}
    >
      <DemoItemWrapper title="基本用法">
        <Noticebar text="text"></Noticebar>
      </DemoItemWrapper>
      <DemoItemWrapper title="文字行数限制">
        <Noticebar text="multipleText" multiple="2"></Noticebar>
      </DemoItemWrapper>
      <DemoItemWrapper title="样式设置">
        <Noticebar text="text" color="red" bgcolor="#ddd"></Noticebar>
      </DemoItemWrapper>
      <DemoItemWrapper title="图标隐藏">
        <Noticebar text="隐藏右侧" righthide></Noticebar>
        <Noticebar text="隐藏左侧" lefthide></Noticebar>
      </DemoItemWrapper>
      <DemoItemWrapper title="两端自定义">
        <Noticebar text="text">
          <div slot="left">左侧内容</div>
          <div slot="right">右侧内容</div>
        </Noticebar>
      </DemoItemWrapper>
    </div>
  );
}
