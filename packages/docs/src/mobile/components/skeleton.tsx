import React from 'react';
import { Skeleton } from '@ls-design/ui-react';

type DemoItemProps = {
  title: string;
  children: React.ReactNode;
};

const DemoItemWrapper = ({ title, children }: DemoItemProps) => {
  return (
    <div style={{ display: 'flex', flexDirection: 'column' }}>
      <h3>{title}</h3>
      <div style={{ display: 'flex', flexDirection: 'column' }}>{children}</div>
    </div>
  );
};
export default function Demo() {
  return (
    <div
      style={{
        padding: 0,
        display: 'flex',
        flexDirection: 'column',
        rowGap: 20,
        minHeight: 'calc(100vh - 48px)',
      }}
    >
      <DemoItemWrapper title="基本用法">
        <Skeleton row={2} />
      </DemoItemWrapper>
      <DemoItemWrapper title="显示头像">
        <Skeleton title avatar row={2} />
      </DemoItemWrapper>
      <DemoItemWrapper title="显示子组件">
        <Skeleton avatar row={2}>
          <div>子内容</div>
        </Skeleton>
      </DemoItemWrapper>
      <DemoItemWrapper title="设置段落宽度">
        <Skeleton row={2} rowwidths="100%,100%,60%" />
      </DemoItemWrapper>
    </div>
  );
}
