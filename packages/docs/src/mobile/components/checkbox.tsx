import React, { useState } from 'react';
import { Checkbox } from '@ls-design/ui-react';

type DemoItemProps = {
  title: string;
  children: React.ReactNode;
};

const DemoItemWrapper = ({ title, children }: DemoItemProps) => {
  return (
    <div style={{ display: 'flex', flexDirection: 'column' }}>
      <h3>{title}</h3>
      <div style={{ display: 'flex', flexDirection: 'column' }}>{children}</div>
    </div>
  );
};
export default function Demo() {
  const [checked, setCheck] = useState(true);

  const handleChange = () => {
    setCheck(() => !checked);
  };

  return (
    <>
      <Checkbox checked={checked} onChange={handleChange}>
        Apple
      </Checkbox>
      <Checkbox checked={false}>Orange</Checkbox>
    </>
  );
}
