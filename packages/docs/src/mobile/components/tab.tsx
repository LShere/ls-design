import { TabContent, Tabs } from '@ls-design/ui-react';
import React from 'react';

export default function Demo() {
  return (
    <Tabs activekey="1">
      <TabContent label="tab1"> tab1 content </TabContent>
      <TabContent label="tab2"> tab2 content </TabContent>
      <TabContent label="tab3" disabled>
        {' '}
        tab3 content{' '}
      </TabContent>
      <TabContent label="tab4"> tab4 content </TabContent>
    </Tabs>
  );
}
