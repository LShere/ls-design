import React from 'react';
import '@ls-design/icons/dist/es/arrow-bottom';

export default function Icon() {
  return (
    <div>
      <ls-icon-arrow-bottom></ls-icon-arrow-bottom>
    </div>
  );
}
