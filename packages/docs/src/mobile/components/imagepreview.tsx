import React from 'react';
import { imagePreview, ImagePreview, ImagePreivewRef, Button, Toast } from '@ls-design/ui-react';

type DemoItemProps = {
  title: string;
  children: React.ReactNode;
};

const DemoItemWrapper = ({ title, children }: DemoItemProps) => {
  return (
    <div style={{ display: 'flex', flexDirection: 'column' }}>
      <h3>{title}</h3>
      <div style={{ display: 'flex', flexDirection: 'column' }}>{children}</div>
    </div>
  );
};

const imgs = [
  'https://images.pexels.com/photos/18322030/pexels-photo-18322030.jpeg?auto=compress&cs=tinysrgb&w=1600&lazy=load',
  'https://images.pexels.com/photos/18799511/pexels-photo-18799511.jpeg?auto=compress&cs=tinysrgb&w=1600&lazy=load',
  'https://images.pexels.com/photos/18686438/pexels-photo-18686438.jpeg?auto=compress&cs=tinysrgb&w=1600&lazy=load',
];
export default function Demo() {
  return (
    <div
      style={{
        padding: 0,
        display: 'flex',
        flexDirection: 'column',
        rowGap: 20,
        minHeight: 'calc(100vh - 48px)',
      }}
    >
      <DemoItemWrapper title="基本使用">
        <Button
          onClick={() =>
            imagePreview({
              images: imgs,
            })
          }
        >
          点击预览
        </Button>
      </DemoItemWrapper>
      <DemoItemWrapper title="指定默认位置">
        <Button
          onClick={() =>
            imagePreview({
              startPosition: 3,
              images: imgs,
            })
          }
        >
          点击预览
        </Button>
      </DemoItemWrapper>
      <DemoItemWrapper title="监听滑动事件">
        <Button
          onClick={() =>
            imagePreview({
              images: imgs,
              onChange: (index: number) => Toast.text(`当前移动位置${index + 1}`),
            })
          }
        >
          点击预览
        </Button>
      </DemoItemWrapper>
    </div>
  );
}
