import React, { useState } from 'react';
import { Tooltip } from '@ls-design/ui-react';

type DemoItemProps = {
  title: string;
  children: React.ReactNode;
};

const DemoItemWrapper = ({ title, children }: DemoItemProps) => {
  return (
    <div style={{ display: 'flex', flexDirection: 'column' }}>
      <h3>{title}</h3>
      <div style={{ display: 'flex', flexDirection: 'column' }}>{children}</div>
    </div>
  );
};
export default function Demo() {
  const [open, setOpen] = useState(false);
  const handleClick = () => setOpen(true);
  const handleClose = () => setOpen(false);

  return (
    <div>
      <Tooltip tips="气泡文字" open={open} onClose={handleClose}>
        <div onClick={handleClick}>top 位置</div>
      </Tooltip>
    </div>
  );
}
