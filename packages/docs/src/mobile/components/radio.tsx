import React, { useState } from 'react';
import { Radio, RadioGroup } from '@ls-design/ui-react';

type DemoItemProps = {
  title: string;
  children: React.ReactNode;
};

const DemoItemWrapper = ({ title, children }: DemoItemProps) => {
  return (
    <div style={{ display: 'flex', flexDirection: 'column' }}>
      <h3>{title}</h3>
      <div style={{ display: 'flex', flexDirection: 'column' }}>{children}</div>
    </div>
  );
};
export default function Demo() {
  const [value, setValue] = useState('apple');
  const handleChange = ({ detail }) => {
    setValue(() => detail.value);
  };

  return (
    <>
      <RadioGroup value={value} onChange={handleChange}>
        <Radio name="apple">苹果</Radio>
        <Radio name="banana">香蕉</Radio>
      </RadioGroup>
    </>
  );
}
