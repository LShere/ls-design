import React from 'react';
import { Toast, Button } from '@ls-design/ui-react';

type DemoItemProps = {
  title: string;
  children: React.ReactNode;
};

const DemoItemWrapper = ({ title, children }: DemoItemProps) => {
  return (
    <div style={{ display: 'flex', flexDirection: 'column' }}>
      <h3>{title}</h3>
      <div style={{ display: 'flex', flexDirection: 'column' }}>{children}</div>
    </div>
  );
};
export default function Demo() {
  return (
    <div
      style={{
        padding: 0,
        display: 'flex',
        flexDirection: 'column',
        rowGap: 20,
        minHeight: 'calc(100vh - 48px)',
      }}
    >
      <DemoItemWrapper title="基本使用">
        <Button onClick={() => Toast.text('网络失败，请稍后再试~')}>基础使用</Button>
      </DemoItemWrapper>
      <DemoItemWrapper title="文字提示位置">
        <Button
          onClick={() =>
            Toast.text('网络失败，请稍后再试~', {
              position: 'top',
            })
          }
        >
          提示位置
        </Button>
      </DemoItemWrapper>
      <DemoItemWrapper title="不同类型提示">
        <Button onClick={() => Toast.success('网络失败，请稍后再试~')}>成功提示</Button>
        <Button onClick={() => Toast.error('网络失败，请稍后再试~')}>失败提示</Button>
        <Button onClick={() => Toast.warning('网络失败，请稍后再试~')}>警告提示</Button>
        <Button onClick={() => Toast.loading('网络失败，请稍后再试~')}>加载提示</Button>
      </DemoItemWrapper>
    </div>
  );
}
