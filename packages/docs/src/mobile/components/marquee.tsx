import React from 'react';
import { Marquee } from '@ls-design/ui-react';

type DemoItemProps = {
  title: string;
  children: React.ReactNode;
};

const DemoItemWrapper = ({ title, children }: DemoItemProps) => {
  return (
    <div style={{ display: 'flex', flexDirection: 'column' }}>
      <h3>{title}</h3>
      <div style={{ display: 'flex', flexDirection: 'column' }}>{children}</div>
    </div>
  );
};

const text = '测试'.repeat(35);
export default function Demo() {
  return (
    <div
      style={{
        padding: 0,
        display: 'flex',
        flexDirection: 'column',
        rowGap: 20,
        minHeight: 'calc(100vh - 48px)',
      }}
    >
      <DemoItemWrapper title="基础用法">
        <Marquee title={text}></Marquee>
      </DemoItemWrapper>
      <DemoItemWrapper title="不同速度">
        <Marquee title={text} speed={25}></Marquee>
        <Marquee title={text} speed={100}></Marquee>
      </DemoItemWrapper>
      <DemoItemWrapper title="反向动画">
        <Marquee title={text} reverse></Marquee>
      </DemoItemWrapper>
    </div>
  );
}
