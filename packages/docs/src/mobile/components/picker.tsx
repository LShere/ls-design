import React, { useState, useRef, useEffect } from 'react';
import type { PickerRef } from '@ls-design/ui-react';
import { Picker } from '@ls-design/ui-react';

type DemoItemProps = {
  title: string;
  children: React.ReactNode;
};

const DemoItemWrapper = ({ title, children }: DemoItemProps) => {
  return (
    <div style={{ display: 'flex', flexDirection: 'column' }}>
      <h3>{title}</h3>
      <div style={{ display: 'flex', flexDirection: 'column' }}>{children}</div>
    </div>
  );
};
export default function Demo() {
  const [open, setOpen] = useState(false);
  const pickerRef = useRef<PickerRef>(null);

  const handleClose = () => setOpen(false);

  const handleConfirm = ({ detail }) => {
    const values = detail.value
      .map((column) => {
        return column.value;
      })
      .join('，');
    console.log(`当前选中：${values}`);
    setOpen(false);
  };

  useEffect(() => {
    // 模拟异步获取数据
    setTimeout(() => {
      const { current: pickerCurrent } = pickerRef;
      pickerCurrent.setColumns([
        {
          defaultIndex: 2,
          values: ['星期一', '星期二', '星期三', '星期四', '星期五'],
        },
        {
          defaultIndex: 0,
          values: ['上午', '下午'],
        },
      ]);
    }, 1000);
  }, []);

  const handleClick = () => setOpen(true);

  return (
    <div>
      <div onClick={handleClick}>基本使用</div>
      <Picker title="请选择时间" ref={pickerRef} open={open} onClose={handleClose} onConfirm={handleConfirm} />
    </div>
  );
}
