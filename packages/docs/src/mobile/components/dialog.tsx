import React, { useState } from 'react';
import { Dialog } from '@ls-design/ui-react';

type DemoItemProps = {
  title: string;
  children: React.ReactNode;
};

const DemoItemWrapper = ({ title, children }: DemoItemProps) => {
  return (
    <div style={{ display: 'flex', flexDirection: 'column' }}>
      <h3>{title}</h3>
      <div style={{ display: 'flex', flexDirection: 'column' }}>{children}</div>
    </div>
  );
};
export default function Demo() {
  const [open, setOpen] = useState(false);
  const showDialog = () => {
    setOpen(true);
  };

  const handleConfirm = () => {
    setOpen(false);
  };

  const handleClose = () => setOpen(false);

  return (
    <div>
      <div onClick={showDialog}>基础弹窗</div>
      <Dialog
        open={open}
        title="基础弹窗"
        onConfirm={handleConfirm}
        onClose={handleClose}
        content="代码是写出来给人看的，附带能在机器上运行"
      ></Dialog>
    </div>
  );
}
