import React from 'react';
import { TextArea, Toast } from '@ls-design/ui-react';

type DemoItemProps = {
  title: string;
  children: React.ReactNode;
};

const DemoItemWrapper = ({ title, children }: DemoItemProps) => {
  return (
    <div style={{ display: 'flex', flexDirection: 'column' }}>
      <h3>{title}</h3>
      <div style={{ display: 'flex', flexDirection: 'column' }}>{children}</div>
    </div>
  );
};
export default function Demo() {
  return (
    <div
      style={{
        padding: 0,
        display: 'flex',
        flexDirection: 'column',
        rowGap: 20,
        minHeight: 'calc(100vh - 48px)',
      }}
    >
      <DemoItemWrapper title="基本使用">
        <TextArea />
      </DemoItemWrapper>
      <DemoItemWrapper title="指定行数">
        <TextArea rows="6" />
      </DemoItemWrapper>
      <DemoItemWrapper title="根据内容自动调整高度">
        <TextArea autosize />
      </DemoItemWrapper>
      <DemoItemWrapper title="字数统计">
        <TextArea showcount />
      </DemoItemWrapper>
      <DemoItemWrapper title="字数限制">
        <TextArea showcount maxlength="50" />
      </DemoItemWrapper>
      <DemoItemWrapper title="禁用状态">
        <TextArea disabled />
      </DemoItemWrapper>
      <DemoItemWrapper title="事件触发">
        <TextArea
          onBlur={(event) => {
            Toast.text(`${event.target.value}, blur`);
          }}
          onInput={(event) => {
            Toast.text(`${event.target.value}, input`);
          }}
          onFocus={(event) => {
            Toast.text(`${event.target.value}, focus`);
          }}
          onCompositionStart={(event) => {
            Toast.text(`${event.target.value}, compositionStart`);
          }}
        />
      </DemoItemWrapper>
    </div>
  );
}
