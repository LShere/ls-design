import React from 'react';
import { Collapse } from '@ls-design/ui-react';

type DemoItemProps = {
  title: string;
  children: React.ReactNode;
};

const DemoItemWrapper = ({ title, children }: DemoItemProps) => {
  return (
    <div style={{ display: 'flex', flexDirection: 'column' }}>
      <h3>{title}</h3>
      <div style={{ display: 'flex', flexDirection: 'column' }}>{children}</div>
    </div>
  );
};
export default function Demo() {
  return (
    <div
      style={{
        padding: 0,
        display: 'flex',
        flexDirection: 'column',
        rowGap: 20,
        minHeight: 'calc(100vh - 48px)',
      }}
    >
      <DemoItemWrapper title="基础用法">
        <Collapse title="标题">生命远不止连轴转和忙到极限，人类的体验远比这辽阔、丰富得多。</Collapse>
      </DemoItemWrapper>
      <DemoItemWrapper title="打开状态">
        <Collapse title="标题" open>
          生命远不止连轴转和忙到极限，人类的体验远比这辽阔、丰富得多。
        </Collapse>
      </DemoItemWrapper>
    </div>
  );
}
