import { Navbar } from '@ls-design/ui-react';
import React from 'react';

export default function Demo() {
  return (
    <div>
      <Navbar title="页面标题"></Navbar>
      <Navbar title="左标题" closehide className="left"></Navbar>
    </div>
  );
}
