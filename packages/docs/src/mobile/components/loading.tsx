import React from 'react';
import { Loading } from '@ls-design/ui-react';

type DemoItemProps = {
  title: string;
  children: React.ReactNode;
};

const DemoItemWrapper = ({ title, children }: DemoItemProps) => {
  return (
    <div style={{ display: 'flex', flexDirection: 'column' }}>
      <h3>{title}</h3>
      <div style={{ display: 'flex', flexDirection: 'column' }}>{children}</div>
    </div>
  );
};
export default function Demo() {
  return (
    <div
      style={{
        padding: 0,
        display: 'flex',
        flexDirection: 'column',
        rowGap: 20,
        minHeight: 'calc(100vh - 48px)',
      }}
    >
      <DemoItemWrapper title="基本用法">
        <Loading></Loading>
        <Loading type="circular"></Loading>
        <Loading type="spinner"></Loading>
        <Loading size="20"></Loading>
        <Loading size="30"></Loading>
        <Loading size="40"></Loading>
        <Loading size="30">加载中...</Loading>
        <Loading size="30" vertical>
          加载中...
        </Loading>
      </DemoItemWrapper>
    </div>
  );
}
