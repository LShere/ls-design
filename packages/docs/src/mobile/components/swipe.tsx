import React from 'react';
import { Swipe, SwipeItem } from '@ls-design/ui-react';

type DemoItemProps = {
  title: string;
  children: React.ReactNode;
};

const DemoItemWrapper = ({ title, children }: DemoItemProps) => {
  return (
    <div style={{ display: 'flex', flexDirection: 'column' }}>
      <h3>{title}</h3>
      <div style={{ display: 'flex', flexDirection: 'column' }}>{children}</div>
    </div>
  );
};
export default function Demo() {
  return (
    <div
      style={{
        padding: 0,
        display: 'flex',
        flexDirection: 'column',
        rowGap: 20,
        minHeight: 'calc(100vh - 48px)',
      }}
    >
      <DemoItemWrapper title="基础用法">
        <Swipe>
          <SwipeItem>
            <div>1</div>
          </SwipeItem>
          <SwipeItem>
            <div>2</div>
          </SwipeItem>
          <SwipeItem>
            <div>3</div>
          </SwipeItem>
          <SwipeItem>
            <div>4</div>
          </SwipeItem>
        </Swipe>
      </DemoItemWrapper>
      <DemoItemWrapper title="圆形指示器">
        <Swipe type="round">
          <SwipeItem>
            <div>1</div>
          </SwipeItem>
          <SwipeItem>
            <div>2</div>
          </SwipeItem>
          <SwipeItem>
            <div>3</div>
          </SwipeItem>
          <SwipeItem>
            <div>4</div>
          </SwipeItem>
        </Swipe>
      </DemoItemWrapper>
      <DemoItemWrapper title="自动播放">
        <Swipe type="round" autoplay>
          <SwipeItem>
            <div>1</div>
          </SwipeItem>
          <SwipeItem>
            <div>2</div>
          </SwipeItem>
          <SwipeItem>
            <div>3</div>
          </SwipeItem>
          <SwipeItem>
            <div>4</div>
          </SwipeItem>
        </Swipe>
      </DemoItemWrapper>
    </div>
  );
}
