import React from 'react';
import { ActionSheet, Button } from '@ls-design/ui-react';

type DemoItemProps = {
  title: string;
  children: React.ReactNode;
};

const DemoItemWrapper = ({ title, children }: DemoItemProps) => {
  return (
    <div style={{ display: 'flex', flexDirection: 'column' }}>
      <h3>{title}</h3>
      <div style={{ display: 'flex', flexDirection: 'column' }}>{children}</div>
    </div>
  );
};
export default function Demo() {
  const showBase = () => {
    ActionSheet({
      actions: [{ name: '选项一' }, { name: '选项二' }, { name: '选项三' }],
      select: (index, action) => {
        console.log(action.name);
      },
    });
  };
  return (
    <div
      style={{
        padding: 0,
        display: 'flex',
        flexDirection: 'column',
        rowGap: 20,
        minHeight: 'calc(100vh - 48px)',
      }}
    >
      <DemoItemWrapper title="基本使用">
        <Button onClick={showBase}>基本使用</Button>
      </DemoItemWrapper>
    </div>
  );
}
