import React from 'react';
import { Grid, GridItem } from '@ls-design/ui-react';

type DemoItemProps = {
  title: string;
  children: React.ReactNode;
};

const DemoItemWrapper = ({ title, children }: DemoItemProps) => {
  return (
    <div style={{ display: 'flex', flexDirection: 'column' }}>
      <h3>{title}</h3>
      <div style={{ display: 'flex', flexDirection: 'column' }}>{children}</div>
    </div>
  );
};
export default function Demo() {
  return (
    <div
      style={{
        padding: 0,
        display: 'flex',
        flexDirection: 'column',
        rowGap: 20,
        minHeight: 'calc(100vh - 48px)',
      }}
    >
      <DemoItemWrapper title="基本使用">
        <Grid>
          <GridItem icon="图标地址" text="文字" />
          <GridItem icon="图标地址" text="文字" />
          <GridItem icon="图标地址" text="文字" />
          <GridItem icon="图标地址" text="文字" />
        </Grid>
      </DemoItemWrapper>
      <DemoItemWrapper title="正方形格子">
        <Grid square>
          <GridItem icon="图标地址" text="文字" />
          <GridItem icon="图标地址" text="文字" />
          <GridItem icon="图标地址" text="文字" />
        </Grid>
      </DemoItemWrapper>
      <DemoItemWrapper title="无边框">
        <Grid noborder>
          <GridItem icon="图标地址" text="文字" />
          <GridItem icon="图标地址" text="文字" />
          <GridItem icon="图标地址" text="文字" />
        </Grid>
      </DemoItemWrapper>
      <DemoItemWrapper title="自定义内容">
        <Grid>
          <GridItem>
            <img src="图标地址" style={{ width: 40 }} />
          </GridItem>
          <GridItem>
            <img src="图标地址" style={{ width: 40 }} />
          </GridItem>
          <GridItem>
            <img src="图标地址" style={{ width: 40 }} />
          </GridItem>
        </Grid>
      </DemoItemWrapper>
    </div>
  );
}
