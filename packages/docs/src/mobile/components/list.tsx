import React, { useState } from 'react';
import { List } from '@ls-design/ui-react';

type DemoItemProps = {
  title: string;
  children: React.ReactNode;
};

const DemoItemWrapper = ({ title, children }: DemoItemProps) => {
  return (
    <div style={{ display: 'flex', flexDirection: 'column' }}>
      <h3>{title}</h3>
      <div style={{ display: 'flex', flexDirection: 'column' }}>{children}</div>
    </div>
  );
};
export default function Demo() {
  const [loading1, setLoading1] = useState(false);
  const [finished1, setFinished1] = useState(false);
  const [list1, setList1] = useState([]);

  const onLoad1 = () => {
    if (finished1) {
      return;
    }
    setLoading1(true);
    setTimeout(() => {
      for (let i = 0; i < 10; i += 1) {
        setList1((list1) => [...list1, list1.length + 1]);
      }
      setLoading1(false);
      if (list1.length >= 30) {
        setFinished1(true);
      }
    }, 1000);
  };

  return (
    <List loading={loading1} finished={finished1} finishedtext="finished text" onLoad={onLoad1}>
      <div slot="content">
        {list1.map((item) => (
          <div className="list-list" key={item}>
            {item}
          </div>
        ))}
      </div>
    </List>
  );
}
