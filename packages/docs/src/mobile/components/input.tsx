import React, { useRef, useEffect } from 'react';
import type { InputRef } from '@ls-design/ui-react';
import { Input } from '@ls-design/ui-react';

type DemoItemProps = {
  title: string;
  children: React.ReactNode;
};

const DemoItemWrapper = ({ title, children }: DemoItemProps) => {
  return (
    <div style={{ display: 'flex', flexDirection: 'column' }}>
      <h3>{title}</h3>
      <div style={{ display: 'flex', flexDirection: 'column' }}>{children}</div>
    </div>
  );
};
export default function Demo() {
  const fieldRef = useRef<InputRef>(null);
  useEffect(() => {
    const { current } = fieldRef;
    current.setRules([
      {
        validator: (val) => {
          if (!/^1[3456789]\d{9}$/.test(val)) {
            return false;
          }
          return true;
        },
        message: '请输入正确的手机号',
      },
    ]);
  }, []);
  return (
    <div
      style={{
        padding: 0,
        display: 'flex',
        flexDirection: 'column',
        rowGap: 20,
        minHeight: 'calc(100vh - 48px)',
      }}
    >
      <DemoItemWrapper title="基本使用">
        <Input placeholder="请输入文本" label="文本" />
        <Input type="password" value="123456" label="密码" />
        <Input type="number" value="12345678901" max="11" label="数字" />
        <Input value="一二三四五" maxlength="5" label="最多5位数" />
      </DemoItemWrapper>
      <DemoItemWrapper title="自定义标题/无标题">
        <Input value="自定义标题">
          <div slot="label">
            <span>主标题</span>
            <span>这是一行副标题</span>
          </div>
        </Input>
        <Input placeholder="禁用label" value="无标题" />
      </DemoItemWrapper>
      <DemoItemWrapper title="禁用/只读">
        <Input value="我禁用了" label="禁用" disabled />
        <Input value="我是只读的" label="只读" readonly />
      </DemoItemWrapper>
      <DemoItemWrapper title="必填/自定义校验">
        <Input placeholder="请输入" label="文本" required errormsg="不能为空" />
        <Input placeholder="请输入文本" label="文本" ref={fieldRef} />
      </DemoItemWrapper>
    </div>
  );
}
