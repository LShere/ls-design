import React, { useState, useEffect, useRef } from 'react';
import type { CascadeRef } from '@ls-design/ui-react';
import { Cascade } from '@ls-design/ui-react';

type DemoItemProps = {
  title: string;
  children: React.ReactNode;
};

const DATA = [
  {
    text: '浙江',
    children: [
      {
        text: '杭州',
        children: [{ text: '西湖区' }, { text: '余杭区' }],
      },
      {
        text: '温州',
        children: [{ text: '鹿城区' }, { text: '瓯海区' }],
      },
    ],
  },
  {
    text: '福建',
    children: [
      {
        text: '福州',
        children: [{ text: '鼓楼区' }, { text: '台江区' }],
      },
      {
        text: '厦门',
        children: [{ text: '思明区' }, { text: '海沧区' }],
      },
    ],
  },
  {
    text: '北京',
    children: [
      {
        text: '',
        children: [{ text: '' }],
      },
    ],
  },
];

const DemoItemWrapper = ({ title, children }: DemoItemProps) => {
  return (
    <div style={{ display: 'flex', flexDirection: 'column' }}>
      <h3>{title}</h3>
      <div style={{ display: 'flex', flexDirection: 'column' }}>{children}</div>
    </div>
  );
};
export default function Demo() {
  const [open, setOpen] = useState(false);
  const pickerRef = useRef<CascadeRef>(null);

  const handleClose = () => setOpen(false);

  const handleConfirm = () => {
    const { current } = pickerRef;
    const values = current
      .getValues()
      .map((column) => {
        return column.value;
      })
      .join('，');
    console.log(`当前选中：${values}`);
    setOpen(false);
  };

  useEffect(() => {
    // 模拟异步获取数据
    setTimeout(() => {
      const { current: pickerCurrent } = pickerRef;
      pickerCurrent.setColumns(DATA);
    }, 1000);
  }, []);

  const handleClick = () => setOpen(true);

  return (
    <div className="demo">
      <div onClick={handleClick}></div>
      <Cascade title="请选择地区" ref={pickerRef} open={open}>
        <div slot="header" className="head-container">
          <span className="cancel" onClick={handleClose}>
            取消
          </span>
          <span className="picker-title">请选择地区</span>
          <span className="ensure" onClick={handleConfirm}>
            确定
          </span>
        </div>
      </Cascade>
    </div>
  );
}
