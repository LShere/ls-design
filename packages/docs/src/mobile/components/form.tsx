import React, { useState, useEffect, useRef, Fragment } from 'react';
import type { FormRef } from '@ls-design/ui-react';
import { Form, FormItem, Input, Button } from '@ls-design/ui-react';

type DemoItemProps = {
  title: string;
  children: React.ReactNode;
};

const DemoItemWrapper = ({ title, children }: DemoItemProps) => {
  return (
    <div style={{ display: 'flex', flexDirection: 'column' }}>
      <h3>{title}</h3>
      <div style={{ display: 'flex', flexDirection: 'column' }}>{children}</div>
    </div>
  );
};
export default function Demo() {
  const [form, setForm] = useState({
    name: '',
    password: '',
    other: {
      age: 18,
    },
  });
  const formRef = useRef<FormRef>(null);

  useEffect(() => {
    formRef.current!.setModel(form);
    formRef.current!.setRules({
      name: [{ required: true, message: '请输入姓名' }],
      password: { required: true, message: '请输入密码' },
      other: {
        age: [{ required: true, message: '请输入年龄' }],
      },
    });
  }, []);

  const submit = () => {
    formRef.current.validate((valid, res) => {
      console.log('submit', valid, res);
    });
  };

  const reset = () => {
    formRef.current.resetFields();
  };

  return (
    <Fragment>
      <Form ref={formRef}>
        <FormItem prop="name" label="姓名">
          <Input placeholder="姓名" />
        </FormItem>
        <FormItem prop="password" label="密码">
          <Input placeholder="密码" type="password" />
        </FormItem>
        <FormItem prop="other.age" label="年龄">
          <Input value={form.other.age} placeholder="年龄" />
        </FormItem>
      </Form>
      <div className="flex-box">
        <Button type="primary" size="big" onClick={submit}>
          提交
        </Button>
        <Button size="big" onClick={reset}>
          重置
        </Button>
      </div>
    </Fragment>
  );
}
