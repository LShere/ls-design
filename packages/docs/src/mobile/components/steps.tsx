import React from 'react';
import { Steps, StepItem } from '@ls-design/ui-react';

type DemoItemProps = {
  title: string;
  children: React.ReactNode;
};

const DemoItemWrapper = ({ title, children }: DemoItemProps) => {
  return (
    <div style={{ display: 'flex', flexDirection: 'column' }}>
      <h3>{title}</h3>
      <div style={{ display: 'flex', flexDirection: 'column' }}>{children}</div>
    </div>
  );
};
export default function Demo() {
  return (
    <div
      style={{
        padding: 0,
        display: 'flex',
        flexDirection: 'column',
        rowGap: 20,
        minHeight: 'calc(100vh - 48px)',
      }}
    >
      <DemoItemWrapper title="基础用法">
        <Steps>
          <StepItem status="done" title="步骤一" order="1"></StepItem>
          <StepItem status="doing" title="步骤二" order="2"></StepItem>
          <StepItem status="todo" title="步骤三" order="3"></StepItem>
        </Steps>
      </DemoItemWrapper>
      <DemoItemWrapper title="标题和描述">
        <Steps>
          <StepItem status="done" title="已完成" order="1" content="您的订单已经打包完成，商品已发出">
            1
          </StepItem>
          <StepItem status="doing" title="进行中" order="2" content="您的订单正在配送中">
            2
          </StepItem>
          <StepItem status="todo" title="未开始" order="3" content="收货地址为：杭州市益展商务大厦16F">
            3
          </StepItem>
        </Steps>
      </DemoItemWrapper>
      <DemoItemWrapper title="竖向步骤条">
        <Steps direction="vertical">
          <StepItem status="done" title="已完成" order="1" content="您的订单已经完成，商品已发出">
            1
          </StepItem>
          <StepItem status="doing" title="进行中" order="2" content="您的订单正在配送中">
            2
          </StepItem>
          <StepItem status="todo" title="未开始" order="3" content="收货地址为：杭州市益展商务大厦">
            3
          </StepItem>
        </Steps>
      </DemoItemWrapper>
    </div>
  );
}
