import React from 'react';
import { Badge } from '@ls-design/ui-react';

type DemoItemProps = {
  title: string;
  children: React.ReactNode;
};

const DemoItemWrapper = ({ title, children }: DemoItemProps) => {
  return (
    <div style={{ display: 'flex', flexDirection: 'column' }}>
      <h3>{title}</h3>
      <div style={{ display: 'flex', flexDirection: 'column' }}>{children}</div>
    </div>
  );
};

export default function Demo() {
  return (
    <div
      style={{
        padding: 0,
        display: 'flex',
        flexDirection: 'column',
        rowGap: 20,
        minHeight: 'calc(100vh - 48px)',
      }}
    >
      <DemoItemWrapper title="基本用法">
        <Badge type="dot"></Badge>
        <Badge type="round" content="9"></Badge>
        <Badge type="label" content="文字徽标"></Badge>
      </DemoItemWrapper>
      <DemoItemWrapper title="徽标大小">
        <Badge type="dot">
          <div></div>
        </Badge>
        <Badge content="9">
          <div></div>
        </Badge>
        <Badge type="dot" size="big">
          <div></div>
        </Badge>
        <Badge size="big" content="9">
          <div></div>
        </Badge>
      </DemoItemWrapper>
      <DemoItemWrapper title="支持白色边框">
        <Badge type="label" content="普通徽标">
          <div></div>
        </Badge>
        <Badge type="label" content="边框徽标" border>
          <div></div>
        </Badge>
      </DemoItemWrapper>
    </div>
  );
}
