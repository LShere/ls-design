import React, { useState } from 'react';
import { DatetimePicker, DatetimePickerRef, Toast } from '@ls-design/ui-react';

type DemoItemProps = {
  title: string;
  children: React.ReactNode;
};

const DemoItemWrapper = ({ title, children }: DemoItemProps) => {
  return (
    <div style={{ display: 'flex', flexDirection: 'column' }}>
      <h3>{title}</h3>
      <div style={{ display: 'flex', flexDirection: 'column' }}>{children}</div>
    </div>
  );
};
export default function Demo() {
  const [open, setOpen] = useState(false);

  const handleClose = () => setOpen(false);

  const handleConfirm = ({ detail }) => {
    Toast.text(`${detail.value}`);
    setOpen(false);
  };

  const handleClick = () => setOpen(true);

  return (
    <div className="demo">
      <div onClick={handleClick}></div>
      <DatetimePicker
        title="选择完整时间"
        type="datetime"
        open={open}
        onClose={handleClose}
        onConfirm={handleConfirm}
      />
    </div>
  );
}
