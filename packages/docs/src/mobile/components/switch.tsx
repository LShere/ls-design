import React, { useState } from 'react';
import { Switch } from '@ls-design/ui-react';

type DemoItemProps = {
  title: string;
  children: React.ReactNode;
};

const DemoItemWrapper = ({ title, children }: DemoItemProps) => {
  return (
    <div style={{ display: 'flex', flexDirection: 'column' }}>
      <h3>{title}</h3>
      <div style={{ display: 'flex', flexDirection: 'column' }}>{children}</div>
    </div>
  );
};
export default function Demo() {
  const [checked, setChecked] = useState(false);
  const handleChange = (e) => {
    setChecked(e.detail.value);
  };
  return (
    <div
      style={{
        padding: 0,
        display: 'flex',
        flexDirection: 'column',
        rowGap: 20,
        minHeight: 'calc(100vh - 48px)',
      }}
    >
      <DemoItemWrapper title="基础用法">
        <Switch checked></Switch>
      </DemoItemWrapper>
      <DemoItemWrapper title="禁用状态">
        <Switch disabled></Switch>
      </DemoItemWrapper>
      <DemoItemWrapper title="自定义大小">
        <Switch size="26"></Switch> <Switch style="font-size: 26px"></Switch>
      </DemoItemWrapper>
      <DemoItemWrapper title="自定义颜色">
        <Switch color="#ccc" inactivecolor="#000"></Switch>
      </DemoItemWrapper>
      <DemoItemWrapper title="change事件">
        <Switch onChange={handleChange} checked={checked}></Switch>;
      </DemoItemWrapper>
    </div>
  );
}
