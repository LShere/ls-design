import React from 'react';
import { Stepper, Toast } from '@ls-design/ui-react';

type DemoItemProps = {
  title: string;
  children: React.ReactNode;
};

const DemoItemWrapper = ({ title, children }: DemoItemProps) => {
  return (
    <div style={{ display: 'flex', flexDirection: 'column' }}>
      <h3>{title}</h3>
      <div style={{ display: 'flex', flexDirection: 'column' }}>{children}</div>
    </div>
  );
};
export default function Demo() {
  return (
    <div
      style={{
        padding: 0,
        display: 'flex',
        flexDirection: 'column',
        rowGap: 20,
        minHeight: 'calc(100vh - 48px)',
      }}
    >
      <DemoItemWrapper title="基本用法">
        <Stepper value="1"></Stepper>
      </DemoItemWrapper>
      <DemoItemWrapper title="限制范围">
        <Stepper min="5" max="12" />
      </DemoItemWrapper>
      <DemoItemWrapper title="步长设置">
        <Stepper steps="2" />
      </DemoItemWrapper>
      <DemoItemWrapper title="整数输入">
        <Stepper interger />
      </DemoItemWrapper>
      <DemoItemWrapper title="禁用状态">
        <Stepper disabled />
      </DemoItemWrapper>
      <DemoItemWrapper title="变更回调">
        <Stepper onChange={({ detail }) => Toast.text(detail)} />
      </DemoItemWrapper>
    </div>
  );
}
