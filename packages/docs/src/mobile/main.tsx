import React from 'react';
import ReactDOM from 'react-dom/client';
import { routes } from './router';
import { RouterProvider } from 'react-router-dom';

import '@/shared/utils/touchSimulator';

ReactDOM.createRoot(document.getElementById('root-mobile')!).render(<RouterProvider router={routes} />);
