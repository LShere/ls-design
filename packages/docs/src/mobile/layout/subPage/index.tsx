import React from 'react';
import { Header } from '../header';
import './index.less';

type Props = {
  title?: React.ReactNode;
  children: React.ReactNode;
};

export const SubPage = ({ title, children }: Props) => {
  return (
    <div className="mobile-sub-page">
      <Header title={title} />
      <div className="content">{children}</div>
    </div>
  );
};
