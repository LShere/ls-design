import React from 'react';
import '@ls-design/icons/dist/es/arrow-left';
import './index.less';

type Props = {
  title: React.ReactNode;
};

export const Header = ({ title }: Props) => {
  return (
    <>
      <div className="mobile-header">
        <ls-icon-arrow-left size="25" color="#000"></ls-icon-arrow-left>
        {title && <span className="title">{title}</span>}
      </div>
      <div className="mobile-header-placeholder"></div>
    </>
  );
};
