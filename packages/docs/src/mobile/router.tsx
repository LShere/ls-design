import React from 'react';
import { createBrowserRouter } from 'react-router-dom';
import { App } from '@/mobile/App';
import { getDemosRoutes } from '@/shared/utils/getComponentsRoutes';
import { SubPage } from './layout/subPage';

export const routes = createBrowserRouter(
  [
    {
      path: '/',
      element: <App />,
    },
    ...getDemosRoutes(SubPage),
  ],
  {
    basename: '/mobile',
  },
);
