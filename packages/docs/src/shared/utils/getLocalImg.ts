export const getLocalImg = (name: string) => {
  return new URL(`/${name}`, import.meta.url).href;
};
