import React, { lazy, Suspense } from 'react';
import type { RouteObject } from 'react-router-dom';

let filterCompsRoutes = null as RouteObject[] | null;
let filterMdsRoutes = null as RouteObject[] | null;

export const getDemosRoutes = (Wrapper: React.ComponentType<{ title: React.ReactNode; children: React.ReactNode }>) => {
  if (filterCompsRoutes) return filterCompsRoutes;
  const components = import.meta.globEager('../../mobile/components/*.tsx');
  console.log('===demos', components);
  const compNameRE = /components\/(.*)\.tsx$/;
  filterCompsRoutes = Object.entries(components).map(([k, v]) => {
    const name = compNameRE.exec(k)![1];
    console.log('===demo name', name);
    const Demo = (v as any).default;
    return {
      path: `${name}`,
      element: (
        <Wrapper title={name}>
          <Demo />
        </Wrapper>
      ),
    } as RouteObject;
  });

  console.log('===filterCompsRoutes', filterCompsRoutes);
  return filterCompsRoutes;
};

export const getMdsRoutes = (Wrapper: React.ComponentType<{ mdStr: string }>) => {
  if (filterMdsRoutes) return filterMdsRoutes;
  // 以字符串的方式引入,提供给react-markdown
  const mds = import.meta.globEager('../../../../ui-lib/src/components/**/zh-CN-react.md', { as: 'raw' });
  console.log('===mds', mds);
  const compNameRE = /components\/(.*)\/zh-CN-react\.md$/;

  filterMdsRoutes = Object.entries(mds).map(([k, v]) => {
    const name = compNameRE.exec(k)![1];
    console.log('===name', name);
    return {
      path: `${name}`,
      element: <Wrapper mdStr={v} />,
    } as RouteObject;
  });
  return filterMdsRoutes;
};
