import configs from '../constants/config.json';

let subMenuPaths: string[] = [];

export const cleanUrl = (url: string) => {
  return url.replace(/#(.+)/g, '').replace(/\?(.+)/g, '');
};

export const judgeCurrDocRouteExist = () => {
  if (!subMenuPaths.length) {
    subMenuPaths = configs.nav.reduce((prev, curr) => {
      return [...prev, ...curr.children.map((item) => item.path)];
    }, [] as string[]);
  }

  const docPath = cleanUrl(location.pathname).split('/').at(-1);
  if (docPath && subMenuPaths.includes(docPath)) {
    return true;
  }
  return false;
};

export const getMainMenuCategory = () => {
  const docPath = cleanUrl(location.pathname).split('/').at(-1);
  if (docPath) {
    return configs.nav.find((item) => item.children.some((subItem) => subItem.path === docPath))?.category;
  }
};

export const judgeSubMenuEqual = (path: string) => {
  const docPath = cleanUrl(location.pathname).split('/').at(-1);
  return docPath === path;
};
