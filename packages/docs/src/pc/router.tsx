import React, { lazy } from 'react';
import { createBrowserRouter } from 'react-router-dom';
import { App } from './App';
import { getMdsRoutes } from '@/shared/utils/getComponentsRoutes';
import { Doc } from './pages/doc';
import { Main } from './layout/main';

export const routes = createBrowserRouter([
  {
    path: '/',
    element: <App />,
  },
  {
    path: '/docs',
    element: <Doc />,
    children: getMdsRoutes(Main),
  },
  {
    path: '*',
    element: <App />,
  },
]);
