import React, { useLayoutEffect, useRef } from 'react';
import menuItems from '@/shared/constants/config.json';
import { useNavigate } from 'react-router-dom';
import './index.less';
import { getMainMenuCategory, judgeSubMenuEqual } from '@/shared/utils/routeUtils';

export const SideBar = () => {
  const navigate = useNavigate();
  const sidebarRef = useRef<HTMLDivElement>(null);

  useLayoutEffect(() => {
    const cb = (e) => {
      if (e.propertyName === 'opacity') {
        const currOpacity = getComputedStyle(e.target).opacity;
        if (currOpacity === '0') {
          e.target.style.visibility = 'hidden';
        } else {
          e.target.style.visibility = 'visible';
        }
      }
    };
    sidebarRef.current!.addEventListener('transitionend', cb);
    return () => {
      sidebarRef.current!.removeEventListener('transitionend', cb);
    };
  }, []);

  useLayoutEffect(() => {
    const currDocCategory = getMainMenuCategory();
    const currMenu = Array.from(sidebarRef.current?.children ?? []).find(
      (menu) => menu.getAttribute('data-category') === currDocCategory,
    );
    const checkbox = currMenu?.querySelector('input[type="checkbox"]') as HTMLInputElement;

    // 判断是否已经展开
    if (!checkbox.checked) {
      checkbox.checked = true;
    }
  }, [location.pathname]);
  return (
    <div ref={sidebarRef} className="pc-sidebar">
      {menuItems.nav.map((item, index) => (
        <div key={item.category} className="sub-menu" data-category={item.category}>
          <div className="sub-menu-title">
            <input id={`submenu-${index}`} type="checkbox" />
            <label htmlFor={`submenu-${index}`}>
              <span className="title">{item.desc}</span>
              <svg
                className="arrow"
                viewBox="0 0 1024 1024"
                version="1.1"
                xmlns="http://www.w3.org/2000/svg"
                p-id="4004"
                width="200"
                height="200"
              >
                <path
                  d="M446.293333 768a95.146667 95.146667 0 0 1-38.826666-8.533333 75.093333 75.093333 0 0 1-44.8-67.84V332.373333A75.093333 75.093333 0 0 1 407.466667 264.533333a89.6 89.6 0 0 1 94.293333 11.093334l217.6 179.626666a72.533333 72.533333 0 0 1 0 113.493334l-217.6 179.626666a87.893333 87.893333 0 0 1-55.466667 19.626667z"
                  p-id="4005"
                ></path>
              </svg>
            </label>
          </div>
          <div className="sub-menu-list">
            {item.children.map((item2) => (
              <div
                key={item2.name}
                className="menu-item"
                style={{ color: judgeSubMenuEqual(item2.path) ? 'var(--menu-active-color)' : 'var(--font-color)' }}
                onClick={() => navigate(`/docs/${item2.path}`)}
              >{`${item2.desc} ${item2.name}`}</div>
            ))}
          </div>
        </div>
      ))}
    </div>
  );
};
