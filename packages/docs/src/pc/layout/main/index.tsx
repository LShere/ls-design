import { MdPreview } from '@/pc/components/mdPreview';
import { Simulator } from '@/pc/components/simulator';
import React, { useLayoutEffect } from 'react';
import './index.less';
type Props = {
  mdStr: string;
};

export const Main = ({ mdStr }: Props) => {
  const mainMdRef = React.useRef<HTMLDivElement>(null);

  useLayoutEffect(() => {
    mainMdRef.current?.scrollTo({
      behavior: 'smooth',
      left: 0,
      top: 0,
    });
  }, [location.href]);
  return (
    <div className="pc-main">
      <div ref={mainMdRef} className="pc-main-md">
        <MdPreview mdStr={mdStr} />
      </div>
      <div className="pc-main-simulator">
        <Simulator />
      </div>
    </div>
  );
};
