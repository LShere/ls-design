import React from 'react';
import ReactDOM from 'react-dom/client';
import { routes } from './router';
import { RouterProvider } from 'react-router-dom';

ReactDOM.createRoot(document.getElementById('root-pc')!).render(<RouterProvider router={routes} />);
