import React from 'react';

import { useLocation } from 'react-router-dom';

export const Simulator = () => {
  const { pathname } = useLocation();
  return (
    <iframe
      style={{ width: '100%', height: '100%' }}
      src={`/mobile/${pathname.split('/').at(-1)}`}
      frameBorder="0"
    ></iframe>
  );
};
