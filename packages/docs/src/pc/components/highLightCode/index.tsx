import React from 'react';
import type { SyntaxHighlighterProps } from 'react-syntax-highlighter';
import { Prism as SyntaxHighlighter } from 'react-syntax-highlighter';
import { vscDarkPlus, coy } from 'react-syntax-highlighter/dist/esm/styles/prism';

type Props = {
  darkMode?: boolean;
} & SyntaxHighlighterProps;

export const HeightLightCode = ({ language, darkMode = false, ...rest }: Props) => {
  return <SyntaxHighlighter {...rest} language={language} style={darkMode ? vscDarkPlus : coy} />;
};
