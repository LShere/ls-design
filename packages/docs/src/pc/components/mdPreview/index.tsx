import type { CSSProperties } from 'react';
import React from 'react';
import Markdown from 'react-markdown';
import remarkGfm from 'remark-gfm';
import remarkBreaks from 'remark-breaks';
import { HeightLightCode } from '../highLightCode';
import './index.less';

type Props = {
  mdStr: string;
};

export const MdPreview = ({ mdStr }: Props) => {
  console.log('===', mdStr);

  // https://github.com/remarkjs/react-markdown/issues/278
  let md = mdStr.replace(/(?<=\n\n)(?![*-])\n/g, '&nbsp;\n ');
  // Support single linebreak
  md = md.replace(/(\n)/gm, '  \n');
  return (
    <div className="pc-md-preview">
      <Markdown
        children={md}
        remarkPlugins={[remarkGfm]}
        components={{
          code(props) {
            const { children, className, ...rest } = props;
            const match = /language-(\w+)/.exec(className || '');
            return match ? (
              <HeightLightCode
                {...rest}
                children={String(children).replace(/\n$/, '')}
                darkMode
                style={undefined}
                language={match[1]}
                PreTag="div"
                customStyle={{
                  borderRadius: 10,
                  background: '#000',
                }}
              />
            ) : (
              <code {...rest} className={`${className} inline-code`}>
                {children}
              </code>
            );
          },
        }}
      />
    </div>
  );
};
