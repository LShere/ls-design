import React, { useRef } from 'react';
import { useNavigate } from 'react-router-dom';
import { Header } from './layout/header';
import logo from '/logo.svg?url';
import '@ls-design/icons/dist/es/copy';
import '@ls-design/icons/dist/es/success';

import './App.less';
import { CONSTANTS_MAP } from '@/shared/constants/fields';
import { copyText } from '@/shared/utils';

let timer: NodeJS.Timeout | null = null;
export const App = () => {
  const navigate = useNavigate();
  const iconContainerRef = useRef<HTMLDivElement>(null);
  const onCopyClick = () => {
    if (timer) return;
    copyText(CONSTANTS_MAP.installDesc);
    // 动画效果触发
    iconContainerRef.current?.classList.add('icon-container-copied');
    timer = setTimeout(() => {
      iconContainerRef.current?.classList.remove('icon-container-copied');
      timer = null;
    }, 1500);
  };
  return (
    <div className="pc-home">
      <Header />
      <div className="pc-home-main">
        <div className="logo">
          <img src={logo} alt="" />
        </div>
        <div className="desc">{CONSTANTS_MAP.profile}</div>
        <div className="action-area">
          <button onClick={() => navigate('/docs/button')}>快速开始</button>
          <div onClick={onCopyClick}>
            <span>{CONSTANTS_MAP.installDesc}</span>
            <div className="icon-container-outer">
              <div ref={iconContainerRef} className="icon-container">
                <ls-icon-copy size="25" />
                <ls-icon-success size="25" />
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};
