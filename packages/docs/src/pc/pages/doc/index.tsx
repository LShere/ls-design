import React, { useLayoutEffect } from 'react';

import { Header } from '@/pc/layout/header';
import { SideBar } from '@/pc/layout/sidebar';
import { Outlet } from 'react-router-dom';

import './index.less';

export const Doc = () => {
  return (
    <div className="pc-doc">
      <Header />
      <div className="pc-doc-content">
        <SideBar />
        <Outlet />
      </div>
    </div>
  );
};
